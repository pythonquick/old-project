package impress.ps.concurrency;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.TimeUnit;

/**
 * Abstract base class for implementing a Lock.
 * A Lock class can be used to prevent simultaneous execution of a particular
 * task.
 *
 * An example would be to implement a Lock for any write-access to a repository 
 * container. If an I.Map needs to change a repository container, it first needs
 * to acquire such a Lock instance before it is allowed to update the container.
 * This prevents two I.Maps from simultaneously writing to the same container.
 * 
 * A client of the a Lock-implementing class does the following:
 * <pre><code>
 *     LockImpl lock = LockImpl.getInstance();
 *     try
 *     {
 *         if (lock.tryLock(waitTimeout))
 *         {
 *             // Do some operation while the lock is hold
 *         }
 *         else
 *         {
 *             // Oops, lock could not be aquired
 *             // Handle this case
 *         }
 *     }
 *     catch (InterruptedException e)
 *     {
 *         // We got Interrupted while trying to aquire the lock
 *         // Handle this case
 *     }
 *     finally
 *     {
 *         // Very important! Release the lock
 *         if (lock.isHeldByCurrentThread())
 *             lock.unlock()
 *     }
 * </code></pre>
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 12-Jul-2007
 *     Creation
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/Lock.java#2 $
 */
public abstract class Lock
{
    private long timeLastTouched = 0;
    private ReentrantLock reentrantLock;
    
    protected Lock()
    {
        reentrantLock = new ReentrantLock();
    }
    
    
    /**
     * Wrapper method around ReentrantLock.tryLock(int, TimeUnit)
     * Attempts to acquire the lock
     * @param timeoutMillis number of milliseconds to wait for the tryLock
     *        operation, before InterruptedException thrown
     * @throws InterruptedException if tryLock call exceeds timeoutMillis
     */
    public boolean tryLock(int timeoutMillis) throws InterruptedException
    {
        long timeNow = System.currentTimeMillis();
        if (timeLastTouched > 0 && timeNow - timeLastTouched > lockTimeout())
            reentrantLock = new ReentrantLock();
        boolean lockedIt = reentrantLock.tryLock(timeoutMillis, TimeUnit.MILLISECONDS);
        if (lockedIt)
            timeLastTouched = System.currentTimeMillis();
        return lockedIt;
    }
    
    
    /**
     * Wrapper method around ReentrantLock.isHeldByCurrentThread
     * @return true if lock held by current thread (Thread of the method caller)
     */
    public boolean isHeldByCurrentThread()
    {
        return reentrantLock.isHeldByCurrentThread();
    }
    

    /**
     * Unlock the lock. Client code should only call this if it is currently
     * holding the lock
     */
    public void unlock()
    {
        reentrantLock.unlock();
    }
    
    
    /**
     * Refreshes the timestamp of the lock instance.
     * The timeout starts running again.
     * This helps very long running locking jobs to keep the lock, 
     * otherwise the lock might expire
     */
    public void keepAlive()
    {
        timeLastTouched = System.currentTimeMillis();
    }
    
    
    /**
     * Return timeout (milliseconds). This is just precaution in case the 
     * client code does not call unlock. In such a case the tryLock will only
     * work after this amount of milliseconds.
     * NOTE: This is not the same timeout used by the tryLock method!
     *       There are two timeouts:
     *       1) timeout specified by client when it attempts to aquire lock
     *       2) this timeout, the time after which lock will expire
     *       The tryLock method uses a timeout specified by client, the number
     *       of milliseconds the client is prepared to wait to aquire a lock.
     *       The timeout returned by this method, should be the time after
     *       which the Lock (in case it was locked) shall expire.
     *       This is to prevent client program errors (lock not released) from
     *       making the Lock unusable, so after this time the Lock can be 
     *       acquired again. It is important that this timeout is greater than
     *       the duration a client might want to aquire the lock for!
     * @return timeout long value (milliseconds) for locked state
     */
    protected abstract long lockTimeout();
}
