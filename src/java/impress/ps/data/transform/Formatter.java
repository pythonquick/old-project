package impress.ps.data.transform;


/**
 * Required interface to be implemented by formatter classes
 */
public interface Formatter
{
    /**
     * @param unformatted input string
     * @return formatted output string
     */
    public String format(String unformatted);
}
