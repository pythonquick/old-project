package impress.ps.util;


import impress.ois.base.dtrans.IContainer;
import impress.ois.base.dtrans.GenericContainer;
import impress.ois.base.error.OISException;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.map.imap.rt.BR;
import impress.ois.base.map.imap.AbstractIMap;
import impress.ps.util.StringUtil;


/**
 * <pre>
 * Class with some commonly used container helper methods.
 *
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/ContUtil.java#7 $
 * </pre>
 */
public class ContUtil
{
    private final static String STD_IE_CNT_TYPE = "Standard IE";
    private final static String[] EMPTY_STR_ARRAY = new String[0];

    private ContUtil() { /* Not to be instantiated */ }


    /**
     * <pre>
     * Gets or creates the sub-container under specified key.
     * Creates a new GenericContainer if it was null, and sets this created
     * container in the specified container under the specified key
     * </pre>
     * @deprecated use getOrCreateCont
     * @param parent IContainer parent container
     * @param childKey String key of child IContainer under parent container
     * @return IContainer child IContainer if found, else newly created one
     */
    public static IContainer getOrCreateElem(IContainer parent, String childKey)
    {
        return getOrCreateCont(parent, childKey);
    }


    /**
     * <pre>
     * Gets or creates the sub-container under specified key.
     * Creates a new GenericContainer if it was null, and sets this created
     * container in the specified container under the specified key
     * </pre>
     * @deprecated use getOrCreateCont
     * @param parent IContainer parent container
     * @param childKey String key of child IContainer under parent container
     * @return IContainer child IContainer if found, else newly created one
     */
    public static IContainer createIfNull(IContainer parent, String childKey)
    {
        return getOrCreateCont(parent, childKey);
    }


    /**
     * <pre>
     * Gets or creates the sub-container under specified key.
     * Creates a new GenericContainer if it was null, and sets this created
     * container in the specified container under the specified key.
     * </pre>
     * @param parent IContainer parent container
     * @param childKey String key of child IContainer under parent container
     * @return IContainer child IContainer if found, else newly created one
     */
    public static IContainer getOrCreateCont(IContainer parent, String childKey)
    {
        IContainer child = (IContainer)parent.getElement(childKey);
        if (child == null)
        {
            child = new GenericContainer();
            parent.setElement(childKey, child);
        }
        return child;
    }


    /**
     * <pre>
     * Returns the keys (String[]) of specified container or empty array if
     * specified container is null. Conveniently saves the caller from an
     * additional if-statement for checking if the container is null.
     * </pre>
     * @param cont instance implementing IContainer, or null
     * @return String[] keys of 'cont'. String[] of length 0 if 'cont' is null
     */
    public static String[] keys(IContainer cont)
    {
        if (cont == null)
            return EMPTY_STR_ARRAY;
        else
            return cont.getKeys();
    }


    /**
     * Returns number of keys of an IContainer.
     * @param cont IContainer
     * @return number of keys in IContainer 'cont',  0 if 'cont' is null
     */
    public static int numKeys(IContainer cont)
    {
        String[] keys = keys(cont);
        return keys.length;
    }


    /**
     * <pre>
     * Adds specified Object to the next numbered row key of specified container.
     * For example, suppose container looks as follows:
     *     "2" = "AAA",
     *     "3" = "BBB"
     * then after the following call:
     * <code>
     *     ContUtil.addRow(container, rowContent);
     * </code>
     * the rowContent object will be added as a new row under key ("4"):
     *     "2" = "AAA",
     *     "3" = "BBB"
     *     "4" = <rowContent>
     * </pre>
     * @param container IContainer to add element to (in-out param: WILL CHANGE)
     * @param rowContent Object element to be added in new row of container
     */
    public static void addRow(IContainer container, Object rowContent)
    {
        if (container == null)
            throw new IllegalArgumentException("null container parameter");
        int rowNum = container.getKeys().length;
        while (container.getElement(rowNum + "") != null)
            rowNum++;
        container.setElement(rowNum + "", rowContent);
    }

    
    /**
     * <pre>
     * Adds and returns new GenericContainer ("row") to a parent container, with the next numbered key.
     * For example, suppose container looks as follows:
     *     "2" = "AAA",
     *     "3" = "BBB"
     * then after the following call:
     * <code>
     *     IContainer row = ContUtil.addRow(container);
     * </code>
     * the key "4" will contain a new, empty container:
     *     "2" = "AAA",
     *     "3" = "BBB"
     *     "4" = {empty GenericContainer}
     * and the empty container will be referenced by the 'row' variable
     * </pre>
     * @param container IContainer to add new container to (in-out param: WILL CHANGE)
     */
    public static IContainer addRow(IContainer container)
    {
        if (container == null)
            throw new IllegalArgumentException("null container parameter");
        int rowNum = container.getKeys().length;
        while (container.getElement(rowNum + "") != null)
            rowNum++;
        String rowKey = rowNum + "";
        return ContUtil.getOrCreateCont(container, rowKey);
    }
    

    /**
     * <pre>
     * Changes the container, by appending rows from another container.
     * Appended container rows are sequentially numbered (String) keys.
     * NOTES: - The container passed as first parameter will be changed
     *
     * Example:
     *     myContainer."0.FirstName"    = "George"
     *     myContainer."0.LastName"     = "Clooney"
     *     myContainer."1.FirstName"    = "Sandra"
     *     myContainer."1.LastName"     = "Bullock"
     *     rowsToAppend."0.FirstName"   = "Alan"
     *     rowsToAppend."1.FirstName"   = "Smurf"
     *     rowsToAppend."1.LastName"    = "Papa"
     *     rowsToAppend."bla.Something" = "Wicked"
     *     ContUtil.addRows(myContainer, rowsToAppend);
     *
     * Then myContainer will look as follows:
     *     myContainer."0.FirstName" = "George"
     *     myContainer."0.LastName"  = "Clooney"
     *     myContainer."1.FirstName" = "Sandra"
     *     myContainer."1.LastName"  = "Bullock"
     *     myContainer."2.FirstName" = "Alan"
     *     myContainer."3.FirstName" = "Smurf"
     *     myContainer."3.LastName"  = "Papa"
     *     myContainer."4.Something" = "Wicked"
     * </pre>
     * @param container IContainer to be changed
     * @param rowsToAppend IContainer rows to append (not changed)
     *
     */
    public static void addRows(IContainer container, IContainer rowsToAppend)
    {
        String[] toAppendKeys = keys(rowsToAppend);
        for (int i=0; i<toAppendKeys.length; i++)
        {
            Object rowToAppend = rowsToAppend.getElement(toAppendKeys[i]);
            ContUtil.addRow(container, rowToAppend);
        }
    }


    /**
     * Returns all rows (sub-containers) of specified container.
     * Each sub-container in the returned array, is the IContainer element
     * of the top-level keys of the specified container.
     * @param container IContainer with sub-containers
     * @return IContainer[] array with all sub-containers or 0-length array if container is null
     * @see #getElements
     */
    public static IContainer[] getRows(IContainer container)
    {
        String[] keys = ContUtil.keys(container);
        IContainer[] elements = new IContainer[keys.length];
        for (int rowIdx=0; rowIdx<keys.length; rowIdx++)
        {
            elements[rowIdx] = (IContainer)container.getElement(keys[rowIdx]);
        }

        return elements;
    }


    /**
     * @param cont IContainer (must not be null)
     * @return element in {cont} container, under first key: {cont}.getKeys()[0]
     * @throws EmptyContainerException if no more keys present
     * @throws IllegalArgumentException if {cont} is null
     */
    public static Object popNextElement(IContainer cont)
    {
        if (cont == null)
            throw new IllegalArgumentException("cont parameter is null");
        String[] keys = keys(cont);
        if (keys.length == 0)
            throw new EmptyContainerException();
        String firstKey = keys[0];
        Object element = cont.getElement(firstKey);
        ((GenericContainer)cont).removeElement(firstKey);
        return element;
    }


    /**
     * Returns value under container key, or a default if container/value is null.
     * @param cont IContainer parent container
     * @param key String key of the 'cont' IContainer
     * @param defaultValue Object value to return if null value under key
     * @return value of container element under key,
     *         or the supplied default if either cont or cont.(key) is null
     */
    public static Object getElement(IContainer cont, String key, Object defaultValue)
    {
        if (cont == null)
            return defaultValue;
        Object elemValue = cont.getElement(key);
        if (elemValue == null)
            return defaultValue;
        else
            return elemValue;
    }


    /**
     * Returns Object-array of elements of container. Elements are the contents
     * of direct keys of specified container.
     * @param cont IContainer parent container
     * @return Object[] of all elements or length-0 array if container is empty or null.
     * @see #keys
     * @see #getRows
     */
    public static Object[] getElements(IContainer cont)
    {
        String[] keys = ContUtil.keys(cont);
        Object[] elements = new Object[keys.length];
        for (int rowIdx=0; rowIdx<keys.length; rowIdx++)
        {
            elements[rowIdx] = cont.getElement(keys[rowIdx]);
        }

        return elements;
    }


    /**
     * <pre>
     * Updates (merges) specified IContainer with contents of another IContainer.
     * NOTES: - This changes 'toBeMerged' (first parameter IContainer)
     *        - Will not change 'data' (second parameter IContainer)
     *
     * Example:
     *     IContainer toBeMerged = new GenericContainer();
     *     toBeMerged."Msg"      = "one";
     *     toBeMerged."0.Msg"    = "two";
     *     toBeMerged."0.0.Name" = "A";
     *     toBeMerged."0.1.Name" = "B";
     *     toBeMerged."0.1.Age"  = "2";
     *     toBeMerged."1.Msg"    = "three";
     *     toBeMerged."1.1.Age"  = "3";
     *
     *     IContainer data = new GenericContainer();
     *     data."Msg"      = "FOUR";
     *     data."0.Msg"    = "FIVE";
     *     data."0.0.Age"  = "111";
     *     data."0.1.Age"  = "222";
     *     data."1.1.Age"  = "333";
     *     data."2.0.Msg"  = "SIX";
     *
     *     ContUtil.merge(toBeMerged, data);
     *
     * Then container will look as follows:
     *     toBeMerged."Msg" = "FOUR"
     *     toBeMerged."0.Msg" = "FIVE"
     *     toBeMerged."0.0.Name" = "A"
     *     toBeMerged."0.0.Age" = "111"
     *     toBeMerged."0.1.Name" = "B"
     *     toBeMerged."0.1.Age" = "222"
     *     toBeMerged."1.Msg" = "three"
     *     toBeMerged."1.1.Age" = "333"
     *     toBeMerged."2.0.Msg" = "SIX"
     *
     * If the same IContainer key existed in both 'toBeMerged' and 'data'
     * then referencing 'toBeMerged' under that key references the
     * IContainer under 'toBeMerged'.
     * For example:
     *     toBeMerged."0.Msg" = "clive";
     * will not change the value of data."0.Msg"
     *
     * However, an IContainer key that exists in 'data', but did not exist
     * originally in 'toBeMerged', will reference the same IContainer.
     * For example:
     *     toBeMerged."2.0.Msg" = "BlaBla";
     * will then also update "data."2.0.Msg" to the value "BlaBla".
     *
     * </pre>
     * @param toBeMerged IContainer to be updated (merged) with 'mergeData'
     * @param data IContainer used to update 'toBeMerged' container
     */
    public static void merge(IContainer toBeMerged, IContainer data)
    {
        mergeSubContainer(toBeMerged, "", data);
    }


    /**
     * <pre>
     * Returns new IContainer with rows (sub-containers) combined from two specified IContainers.
     * NOTES: - Containers 'first' and 'second' are assumed to have index
     *          keys (e.g. "0", "1", "2"), not having special meaning.
     *        - This does not create a deep copy of 'first' or 'second'.
     *          The return IContainer will reference the same rows (sub-containers)
     *        - Will not change specified containers 'first' or 'second'.
     *
     * Example:
     *     first."0.FirstName"    = "George"
     *     first."0.LastName"     = "Clooney"
     *     first."1.FirstName"    = "Sandra"
     *     first."1.LastName"     = "Bullock"
     *     second."0.FirstName"   = "Alan"
     *     second."1.FirstName"   = "Smurf"
     *     second."bla.Something" = "Wicked"
     *     IContainer merged = ContUtil.combineRows(first, second);
     *
     * Then merged will look as follows:
     *     merged."0.FirstName" = "George"
     *     merged."0.LastName"  = "Clooney"
     *     merged."1.FirstName" = "Sandra"
     *     merged."1.LastName"  = "Bullock"
     *     merged."2.FirstName" = "Alan"
     *     merged."3.FirstName" = "Smurf"
     *     merged."4.Something" = "Wicked"
     * </pre>
     * @param first IContainer
     * @param second IContainer
     * @return IContainer consisting of rows (subcontainers) from {first} and {second}
     *         Top-level keys will be in sequence "0", "1", "2", etc. and will
     *         not necessarily correspond to keys in 'first' or 'second'.
     */
    public static IContainer combineRows(IContainer first, IContainer second)
    {
        IContainer merged = new GenericContainer();

        addRows(merged, first);
        addRows(merged, second);

        return merged;
    }


    /**
     * Update 'toBeMerged' container, with specified 'dataChildCnt'.
     * 'dataChildCnt' is a sub-container of a parent data container and the
     * container key under the data container is specified in parameter
     * 'dataChildKey'
     * @see #merge
     * @param toBeMerged container to be merged
     * @param dataChildKey key from a parent data container, under which
     *                     'dataChildCnt' resides
     * @param dataChildCnt container under a data container's 'dataChildKey'
     *                     container key
     */
    private static void mergeSubContainer(
        IContainer toBeMerged,
        String dataChildKey,
        IContainer dataChildCnt)
    {
        String nextLevelKeyPrefix = dataChildKey;
        if (!"".equals(dataChildKey))
            nextLevelKeyPrefix += ".";

        String[] childCntKeys = keys(dataChildCnt);
        for (int i=0; i<childCntKeys.length; i++)
        {
            String childCntKey = childCntKeys[i];
            Object childCntValue = dataChildCnt.getElement(childCntKey);

            String nextLevelKey = nextLevelKeyPrefix + childCntKey;

            if (toBeMerged.getElement(nextLevelKey) == null)
            {
                // container key does not exist in the 'toBeMerged' container.
                // so set this key to reference existing 'childCntValue':
                toBeMerged.setElement(nextLevelKey, childCntValue);
                continue;
            }

            if (childCntValue instanceof IContainer)
            {
                IContainer subContainer = (IContainer)childCntValue;
                mergeSubContainer(toBeMerged, nextLevelKey, subContainer);
            }
            else
            {
                toBeMerged.setElement(nextLevelKey, childCntValue);
            }
        }
    }


    /**
     * Returns true if container parameter is null or has no keys.
     * @param container any IContainer
     * @return boolean true if container is null or has no keys
     */
    public static boolean isNullOrEmpty(IContainer container)
    {
        if (container == null)
            return true;
        return container.getKeys().length == 0;
    }


    /**
     * Returns true if container non-null and has at least one key
     * @param container any IContainer
     * @return boolean true if container is not null and has at least one key
     * @see #isNullOrEmpty
     */
    public static boolean isFilled(IContainer container)
    {
        return !isNullOrEmpty(container);
    }


    /**
     * Returns true if value under specified container key is null or empty.
     * empty means null String (see StringUtil.isNullOrEmpty),
     * or empty container (see ContUtil.isNullOrEmpty overloaded method)
     * @param container any IContainer
     * @param key any String key
     * @return boolean true if container value under specified key is null or
     *         is an empty container or empty (zero-length) string
     */
    public static boolean isNullOrEmpty(IContainer container, String key)
    {
        if (container == null)
            throw new IllegalArgumentException("container is null");

        Object value = container.getElement(key);
        if (value == null)
            return true;
        else if (value instanceof String)
            return StringUtil.isNullOrEmpty((String)value);
        else if (value instanceof IContainer)
        {
            return isNullOrEmpty((IContainer)value);
        }
        else
            return false;
    }


    /**
     * Returns true if value under specified container key is non-null and
     * is not empty
     * @param container any IContainer
     * @param key any String key
     * @return boolean true if container value under specified key is non-null
     *         and is non-empty container or non-empty String
     * @see #isNullOrEmpty
     */
    public static boolean isFilled(IContainer container, String key)
    {
        return !isNullOrEmpty(container, key);
    }


    /**
     * <pre>
     * Splits value-separated String into container with rows.
     * Splits the specified String (values separated by specified separator),
     * into container of format {i} = <splitted String>.
     * For example, ContUtil.split("abc;def;yy", ";") will return the
     * following container:
     *
     * ---IContainer START---
     * 0 = "abc"
     * 1 = "def"
     * 2 = "yy"
     * ---IContainer END---
     * </pre>
     * @param sourceStr Original string containing tokens and separators
     * @param separator separator string e.g. "|"
     * @return IContainer with tokens in sequencial keys "0", "1", "2", etc.
     */
    public static IContainer split(String sourceStr, String separator)
    {
        if (sourceStr == null)
            throw new IllegalArgumentException("null sourceStr");
        if (separator == null)
            throw new IllegalArgumentException("null separator");

        String[] splittedStrings = StringUtil.split(sourceStr, separator);

        IContainer splittedCnt = new GenericContainer();
        for (int i=0; i<splittedStrings.length; i++)
        {
            ContUtil.addRow(splittedCnt, splittedStrings[i]);
        }

        return splittedCnt;
    }


    /**
     * <pre>
     * Combines elements of a container into a String with specified separator.
     * Values of the container elements must not be sub-containers, otherwise
     * IllegalArgument shall be thrown.
     *
     * The returned String uses the 'toString()' representation of each element.
     * For example, if the IContainer 'myCont' looks as follows:
     *     ---IContainer START---
     *     0 = "abc"
     *     1 = "def"
     *     2 = "yy"
     *     ---IContainer END---
     * then ContUtil.concatElems(myCont, "|") shall return the following String:
     *     "abc|def|yy"
     * </pre>
     * @param valueCnt container with simple values (no sub-containers)
     * @param separator separator string e.g. "|"
     * @return String with elements of the container, joined by specified separator
     *         or empty "" string if no elements in the container
     * @see #concatKeys
     */
    public static String concatElems(IContainer valueCnt, String separator)
    {
        if (valueCnt == null)
            throw new IllegalArgumentException("null valueCnt");
        if (separator == null)
            throw new IllegalArgumentException("null separator");

        StringBuilder unsplitted = new StringBuilder();
        String[] keys = keys(valueCnt);
        for (int i=0; i<keys.length; i++)
        {
            Object element = valueCnt.getElement(keys[i]);
            if (element instanceof IContainer)
            {
                String msg = "elem under key " + keys[i] + " is a container";
                throw new IllegalArgumentException(msg);
            }
            if (i > 0)
            {
                unsplitted.append(separator);
            }
            unsplitted.append(element);
        }

        return unsplitted.toString();
    }


    /**
     * <pre>
     * Combines elements of a container into a String with specified separator.
     * Values of the container elements must not be sub-containers, otherwise
     * IllegalArgument shall be thrown.
     *
     * The returned String uses the 'toString()' representation of specified
     * element of each row.
     * For example, if the IContainer 'statuses' looks as follows:
     *     ---IContainer START---
     *     0.SYSTEM_STATUS = PRC
     *     1.SYSTEM_STATUS = PREL
     *     2.SYSTEM_STATUS = ACAS
     *     3.SYSTEM_STATUS = SSAP
     *     4.SYSTEM_STATUS = MANC
     *     5.SYSTEM_STATUS = NMAT
     *     ---IContainer END---
     * then the following call:
     * <code>
     *     ContUtil.concatElems(statuses, "SYSTEM_STATUS", " ")
     * </code>
     * shall return the following String:
     *     "PRC PREL ACAS SSAP MANC NMAT"
     * </pre>
     * @param tableCont container with rows (sub-containers)
     * @param key on each row, where to find the elements to be concatenated
     * @param separator separator string e.g. "|"
     * @return String with specified elements each row, joined by specified separator
     *         or empty "" string if no elements in the container
     * @see #concatKeys
     */
    public static String concatElems(IContainer tableCont, String key, String separator)
    {
        if (tableCont == null)
            throw new IllegalArgumentException("null tableCont");
        if (key == null)
            throw new IllegalArgumentException("null key");
        if (separator == null)
            throw new IllegalArgumentException("null separator");

        StringBuilder unsplitted = new StringBuilder();
        String[] keys = keys(tableCont);
        for (int i=0; i<keys.length; i++)
        {
            Object row = tableCont.getElement(keys[i]);
            if (! (row instanceof IContainer))
            {
                String msg = "elem under key " + keys[i] + " is not a container";
                throw new IllegalArgumentException(msg);
            }
            if (i > 0)
            {
                unsplitted.append(separator);
            }
            Object elemToConcat = ((IContainer)row).getElement(key);
            unsplitted.append(elemToConcat);
        }

        return unsplitted.toString();
    }


    /**
     * <pre>
     * Combines keys of a container into a String with specified separator.
     * Similar to the unsplit method, except this method works with keys,
     * not the elements (values)
     *
     * For example, if the IContainer 'myCont' looks as follows:
     *     ---IContainer START---
     *     "DSEX" = "X"
     *     "PCNF" = null
     *     "REL" = "dummy"
     *     ---IContainer END---
     * then ContUtil.concatKeys(myCont, " ") shall return the following String:
     *     "DSEX PCNF REL"
     * </pre>
     * @param anyCnt container with simple keys to concatenate
     * @param separator separator string e.g. "|"
     * @return String with keys of the container, joined by specified separator
     *         or empty "" string if no keys in the container
     * @see #concatElems
     */
    public static String concatKeys(IContainer anyCnt, String separator)
    {
        if (anyCnt == null)
            throw new IllegalArgumentException("null anyCnt");
        if (separator == null)
            throw new IllegalArgumentException("null separator");

        StringBuilder concatenated = new StringBuilder();
        String[] keys = keys(anyCnt);
        for (int i=0; i<keys.length; i++)
        {
            if (i > 0)
            {
                concatenated.append(separator);
            }
            concatenated.append(keys[i]);
        }

        return concatenated.toString();
    }


    /**
     *
     * <pre>
     * Copies a set of container fields from source container to target container.
     * For example we have two containers:
     *     sourceCnt."A" = "a";
     *     sourceCnt."B" = "b";
     *     sourceCnt."C" = "c";
     *     targetCnt."A" = "aaaaa";
     *     targetCnt."Y" = "yyyyy";
     *     targetCnt."Z" = "zzzzz";
     *
     * Then the following statement:
     *     ContUtil.copyFields(sourceCnt, targetCnt, new String[] {"A", "B", "C"});
     *
     * will result in targetCnt looking as follows:
     *
     *     targetCnt."A" = "a";
     *     targetCnt."Y" = "yyyyy";
     *     targetCnt."Z" = "zzzzz";
     *     targetCnt."B" = "b";
     *     targetCnt."C" = "c";
     * </pre>
     * @param sourceCnt source IContainer
     * @param targetCnt target IContainer
     * @param fieldKeysToCopy String[] of String keys, of all fields to copy
     */
    public static void copyFields(
        IContainer sourceCnt,
        IContainer targetCnt,
        String[] fieldKeysToCopy)
    {
        for (int i=0; i<fieldKeysToCopy.length; i++)
        {
            String key = fieldKeysToCopy[i];
            targetCnt.setElement(key, sourceCnt.getElement(key));
        }
    }


    /**
     * Returns true if there's a container element container.{key} that is equal
     * to specifieid element value, where {key} is any direct key under container.
     * @param container in which to find element
     * @param elemValueToFind Object to look for under container's direct key's.
     * @return true if there's some key {i} where container.{i} equals elemValue
     */
    public static boolean containsElement(IContainer container, Object elemValueToFind)
    {
        if (elemValueToFind == null)
            return false;

        String[] keys = ContUtil.keys(container);
        for (int i=0; i<keys.length; i++)
        {
            Object nextElement = container.getElement(keys[i]);
            if (elemValueToFind.equals(nextElement))
                return true;
        }
        return false;
    }


    /**
     * Creates container key from five specified parts.
     * Returns same part1 + '.' + part2 + '.' + part3 + '.' + part4 + '.' + part5
     * string, but is roughly 15% faster (ImpressServer 5.4 with JDK 1.5.0)
     * @param part1
     * @param part2
     * @param part3
     * @param part4
     * @param part5
     * @return concatenation of specified parts, separated by '.'
     */
    public static String createKey(Object part1, Object part2, Object part3, Object part4, Object part5)
    {
        StringBuilder concatenation = new StringBuilder(60);
        concatenation.append(part1).append('.');
        concatenation.append(part2).append('.');
        concatenation.append(part3).append('.');
        concatenation.append(part4).append('.');
        concatenation.append(part5);
        return concatenation.toString();
    }


    /**
     * Creates container key from four specified parts.
     * Returns same part1 + '.' + part2 + '.' + part3 + '.' + part4
     * string, but is roughly 15% faster (ImpressServer 5.4 with JDK 1.5.0)
     * @param part1
     * @param part2
     * @param part3
     * @param part4
     * @return concatenation of specified parts, separated by '.'
     */
    public static String createKey(Object part1, Object part2, Object part3, Object part4)
    {
        StringBuilder concatenation = new StringBuilder(50);
        concatenation.append(part1).append('.');
        concatenation.append(part2).append('.');
        concatenation.append(part3).append('.');
        concatenation.append(part4);
        return concatenation.toString();
    }


    /**
     * Creates container key from three specified parts.
     * Returns same part1 + '.' + part2 + '.' + part3
     * string, but is roughly 15% faster (ImpressServer 5.4 with JDK 1.5.0)
     * @param part1
     * @param part2
     * @param part3
     * @return concatenation of specified parts, separated by '.'
     */
    public static String createKey(Object part1, Object part2, Object part3)
    {
        StringBuilder concatenation = new StringBuilder(40);
        concatenation.append(part1).append('.');
        concatenation.append(part2).append('.');
        concatenation.append(part3);
        return concatenation.toString();
    }


    /**
     * Creates container key from two specified parts.
     * Returns same part1 + '.' + part2
     * string, but is roughly 15% faster (ImpressServer 5.4 with JDK 1.5.0)
     * @param part1
     * @param part2
     * @return concatenation of specified parts, separated by '.'
     */
    public static String createKey(Object part1, Object part2)
    {
        StringBuilder concatenation = new StringBuilder(30);
        concatenation.append(part1).append('.').append(part2);
        return concatenation.toString();
    }


    /**
     * <pre>
     * Returns true if specified type "Standard IE" container exists in repository.
     * </pre>
     * @param ie IESystem instance
     * @param cntName of container to check for existence in Repository
     * @return boolean true if exists
     * @throws OISException if I.Map call failed
     */
    public static boolean existsInRepos(IESystem ie, String cntName)
    {
        final String IMAP_NAME = "ieDtrSearchContainer";

        BR br = ie.newCall(IMAP_NAME);
        br.setElement("In.TypeName", STD_IE_CNT_TYPE);
        br.setElement("In.NamePattern", cntName);
        br.process();

        if (br.wasSuccessful())
        {
            IContainer searchResultList = (IContainer)br.getElement("Out.Container");
            String[] searchResultKeys = keys(searchResultList);
            return searchResultKeys.length > 0;
        }
        else
        {
            IContainer excCnt = (IContainer)br.getElement("Out.Exception");
            OISException oisExc = OISException.convertIContainerToException(excCnt);
            throw oisExc;
        }
    }


    /**
     * Load a container from the ImpressServer repository (type "Standard IE").
     * @param ie IESystem instance
     * @param name of container to load from ImpressServer repository
     * @return IContainer
     * @throws NotFoundException if container not found
     * @throws OISException if I.Map call failed
     */
    public static IContainer loadContainer(IESystem ie, String name) throws NotFoundException
    {
        final String IMAP_NAME = "ieDtrDbLoadContainer";

        BR br = ie.newCall(IMAP_NAME);
        br.setElement("In.TypeName", STD_IE_CNT_TYPE);
        br.setElement("In.Name", name);
        br.process();
        if (br.wasSuccessful())
            return (IContainer)br.getElement("Out.Container.Data.TestContainer");

        if ((new Integer(7201)).equals(br.getElement("Out.Exception.Code")))
        {
            throw new NotFoundException(name);
        }
        else
        {
            IContainer excCnt = (IContainer)br.getElement("Out.Exception");
            OISException oisExc = OISException.convertIContainerToException(excCnt);
            throw oisExc;
        }
    }


    /**
     * Save container to repository (type "Standard IE"), in format "ASCII"
     * @param ie IESystem instance
     * @param container IContainer to save to ImpressServer repository
     * @param name repository name of container
     * @throws OISException if I.Map call failed
     */
    public static void saveContainer(IESystem ie, IContainer container, String name)
    {
        final String IMAP_NAME = "iePrjEngSaveContainer";
        BR br = ie.newCall(IMAP_NAME);
        br.setElement("In.Container.TypeName", STD_IE_CNT_TYPE);
        br.setElement("In.Container.Name", name);
        br.setElement("In.Container.Data.TestContainer", container);
        br.setElement("In.Format", "ASCII");
        br.process();
        if (!br.wasSuccessful())
        {
            IContainer excCnt = (IContainer)br.getElement("Out.Exception");
            OISException oisExc = OISException.convertIContainerToException(excCnt);
            throw oisExc;
        }
    }


    /**
     * Checked exception, for when a container could not be loaded from repository.
     */
    public static class NotFoundException extends Exception
    {
        private final String containerName;


        public NotFoundException(String containerName)
        {
            super(containerName + " not found");
            this.containerName = containerName;
        }


        /**
         * @return name of container that could not be found
         */
        public String getContainerName()
        {
            return containerName;
        }
    }


    /**
     * Unchecked exception, for when container expected to have keys, but has none
     */
    public static class EmptyContainerException extends RuntimeException
    {
        public EmptyContainerException()
        {
            super("Container has no keys/values. Expected to to be non-empty");
        }
    }
}
