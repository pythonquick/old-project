package impress.ps.util;


import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


/**
 * <pre>
 * Class with some commonly used String helper methods.
 *
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/StringUtil.java#5 $
 * </pre>
 */
public final class StringUtil
{
    private StringUtil() { /* not to be instantiated */ }


    /**
     * Returns String representation of given object, or default "" if it is null.
     * @param anObject
     * @return anObject.toString() or "" if anObject is null
     */
    public static String toString(Object anObject)
    {
        if (anObject == null)
            return "";
        else if (anObject instanceof String)
            return (String)anObject;
        else
            return anObject.toString();
    }


    /**
     * Determines whether the specified String is one of the
     * Strings in the specified String array.
     * NOTE: this method does case sensitive matching!
     * @param searchStr String to be searched for in String array
     * @param strArr String values to search from
     * @return true if searchString is one Strings in stringArray
     *         Will return false if searchString or stringArray null
     */
    public static boolean isOneOf(String searchStr, String[] strArr)
    {
        return isOneOf(searchStr, strArr, false);
    }


    /**
     * Determines whether the specified String is one of the
     * Strings in the specified String array.
     * @param searchStr String to be searched for in String array
     * @param strArr String values to search from
     * @param ignoreCase true if case (capitalization) should not matter
     * @return true if searchString is one Strings in stringArray
     *         Will return false if searchString or stringArray null
     */
    public static boolean isOneOf(String searchStr, String[] strArr, boolean ignoreCase)
    {
        if (strArr == null || searchStr == null)
            return false;
        for (int i=0; i<strArr.length; i++)
        {
            if (ignoreCase)
            {
                if (searchStr.equalsIgnoreCase(strArr[i]))
                    return true;
            }
            else
            {
                if (searchStr.equals(strArr[i]))
                    return true;
            }
        }
        return false;
    }


    /**
     * Determines whether the specified String contains one of the
     * Strings in the specified String array.
     * @param searchString String to be searched for in String array
     * @param stringArray
     * @return true if any one of the Strings in stringArray (case sensitive)
     *         is contained in searchString
     *         Will return false if searchString or stringArray null
     */
    public static boolean containsOneOf(String searchString, String[] stringArray)
    {
        if (stringArray == null || searchString == null)
            return false;
        for (int i=0; i<stringArray.length; i++)
        {
            if (searchString.indexOf(stringArray[i]) > -1)
                return true;
        }
        return false;
    }


    /**
     * Determines whether the specified String contains all of the
     * Strings in the specified String array.
     * @param searchString String to be searched for in String array
     * @param stringArray
     * @return true if any all of the Strings in stringArray (case sensitive)
     *         is contained in searchString
     *         Will return false if searchString or stringArray null
     */
    public static boolean containsAll(String searchString, String[] stringArray)
    {
        if (stringArray == null || searchString == null)
            return false;
        for (int i=0; i<stringArray.length; i++)
        {
            if (searchString.indexOf(stringArray[i]) == -1)
                return false;
        }
        return true;
    }


    /**
     * Removes leading chars - usually used for removing leading zeroes from SAP
     * For example, removeLeading("0000000845561",'0') converts "0000000845561" to "845561"
     * @param str String to remove leading chars from
     * @param c char the char to be removed
     * @return String without the leading char c, null if {str} was null
     */
    public static String removeLeading(String str, char c)
    {
        if (str == null) return null;
        if (str == "") return "";

        char[] temp = str.toCharArray();
        int i=0;

        for(i=0; i<temp.length && temp[i]==c; i++){;}
        return str.substring(i,str.length());
    }


    /**
     * Removes trailing characters.
     * @param str String to remove trailing chars from
     * @param c char the char to be removed
     * @return String without the trailing char c
     */
    public static String removeTrailing(String str, char c)
    {
        if (str == null)
            throw new IllegalArgumentException("null str");

        char[] temp = str.toCharArray();

        // find starting index of the specified char, from the repeating
        // sequence of this char at the end of the specified string
        int charIdx = temp.length;
        while (charIdx > 0 && temp[charIdx-1] == c)
        {
            // Do nothing, just count down the index
            charIdx--;
        }

        return str.substring(0, charIdx);
    }


    /**
     * Removes leading and trailing characters.
     * @param str String to remove trailing chars from
     * @param c char the char to be removed
     * @return String without leading/tailing char c
     */
    public static String removeLeadingAndTrailing(String str, char c)
    {
        return removeLeading(removeTrailing(str, c), c);
    }


    /**
     * @param anyObject any Object or null
     * @param charToAdd char to add to beginning ob String to be returned
     * @param totalLen total length of returned String
     * @return String representation based on specified 'anyObject', filled up
     *         from the left with specified char to the specified total length
     */
    public static String addLeading(Object anyObject, char charToAdd, int totalLen)
    {
        String strRepr = anyObject != null ? anyObject.toString() : "";
        int missingChars = totalLen - strRepr.length();
        if (missingChars < 0)
        {
            String msg = "String representation " + strRepr + " exceeds totalLength " + totalLen;
            throw new IllegalArgumentException(msg);
        }

        StringBuffer padding = new StringBuffer(Character.toString(charToAdd));
        while (padding.length() < missingChars)
            padding.append(padding);
        padding.append(strRepr);
        return padding.substring(padding.length() - totalLen);
    }


    /**
     * @param anyObject any Object or null
     * @param charToAdd char to add to end ob String to be returned
     * @param totalLen total length of returned String
     * @return String representation based on specified 'anyObject', filled up
     *         to the right with specified char to the specified total length
     */
    public static String addTrailing(Object anyObject, char charToAdd, int totalLen)
    {
        String strRepr = anyObject != null ? anyObject.toString() : "";
        int missingChars = totalLen - strRepr.length();
        if (missingChars < 0)
        {
            String msg = "String representation " + strRepr + " exceeds totalLength " + totalLen;
            throw new IllegalArgumentException(msg);
        }

        StringBuffer padding = new StringBuffer(Character.toString(charToAdd));
        while (padding.length() < missingChars)
            padding.append(padding);
        padding.append(strRepr);

        return (strRepr + padding).substring(0, totalLen);
    }


    /**
     * @param aString
     * @param aChar
     * @return int - number of times the specified char occurs in the string
     */
    public static int countChar(String aString, char aChar)
    {
        int count = 0;
        for (int i=0; i<aString.length(); i++)
            count += aString.charAt(i) == aChar ? 1 : 0;
        return count;
    }


    /**
     * Returns new String with the specified char removed
     * @param input String to remove chars from
     * @param charToRemove char the char to be removed
     * @return String {input} without char {charToRemove}
     */
    public static String removeChar(String input, char charToRemove)
    {
        StringBuilder strBuilder = new StringBuilder(input);
        for (int charPos=strBuilder.length()-1; charPos>=0; charPos--)
        {
            if (strBuilder.charAt(charPos) == charToRemove)
            {
                strBuilder.deleteCharAt(charPos);
            }
        }

        return strBuilder.toString();
    }


    /**
     * @param aString
     * @return true if aString is either null or has length 0
     */
    public static boolean isNullOrEmpty(String aString)
    {
        return aString == null || aString.length() == 0;
    }


    /**
     * Returns true if string is non-null and has length greater than 0
     * @param aString
     * @return boolean true if string is non-null and length greater than 0
     * @see #isNullOrEmpty
     */
    public static boolean isFilled(String aString)
    {
        return !isNullOrEmpty(aString);
    }


    /**
     * @param aString
     * @return true if aString is either null or all its chars are whitespace
     */
    public static boolean isNullOrWhitespace(String aString)
    {
        return aString == null || aString.trim().length() == 0;
    }


    /**
     * Escapes regular expression meta-characters such as '|', '*', '.', etc.
     * if these should be treated as normal literals in a regular expression
     * string
     * @param origString original string that might contain meta-characters
     * @return String based on 'origString' but with regex meta-characters
     *         escaped. This string can then be used for regex matching
     */
    public static String escapeRegexMetaChars(String origString)
    {
        String escapedMetaChars = origString;

        // replace all backslashes with double backslashes:
        escapedMetaChars = escapedMetaChars.replaceAll("\\\\", "\\\\\\\\");
        // Replace all "." (period) with "[.]":
        escapedMetaChars = escapedMetaChars.replaceAll("[.]", "[.]");
        // Replace all "?" with ".":
        escapedMetaChars = escapedMetaChars.replaceAll("\\?", ".");
        // Replace all "*" (asterix) with ".*":
        escapedMetaChars = escapedMetaChars.replaceAll("\\*", ".*");
        // Escape all "$" (dollar) symbols:
        escapedMetaChars = escapedMetaChars.replaceAll("\\$", "\\\\\\$");
        // Escape all "|" (pipe) symbols:
        escapedMetaChars = escapedMetaChars.replaceAll("\\|", "\\\\\\|");
        // Escape all "{" and "}" symbols (curly braces):
        escapedMetaChars = escapedMetaChars.replaceAll("\\{", "\\\\\\{");
        escapedMetaChars = escapedMetaChars.replaceAll("\\}", "\\\\\\}");

        return escapedMetaChars;
    }


    /**
     * Returns whether given String matches the given DOS-type wild card string.
     * For example, "my pretty house" will match the wildcard pattern "*hou?e".
     * @param aString
     * @param wildcardPattern DOS type wild card pattern, for example:
     * @return boolean true if 'aString' matches 'wildcardPattern'
     */
    public static boolean matchWildcardPattern(String aString, String wildcardPattern)
    {
        String forRegex = escapeRegexMetaChars(wildcardPattern);

        // Add ^ to beginning of Regex pattern to mark start of line
        if (!forRegex.startsWith("^"))
            forRegex = "^" + forRegex;
        // Add $ to end of Regex pattern to mark end of line
        if (!forRegex.endsWith("$"))
            forRegex += "$";

        Pattern pattern = Pattern.compile(forRegex);
        Matcher matcher = pattern.matcher(aString);
        boolean matchesWildcardPattern = matcher.find();
        return matchesWildcardPattern;
    }


    /**
     * @param part1 nullable String
     * @param part2 nullable String
     * @param joinString non-null String to join part1 and part2.
     * @return {part1}{joinString}{part2} if part1 and part2 non-null
     *         {part1} if only part1 is non-null
     *         {part2} if only part2 is non-null
     *         "" if both part1 and part2 are null
     */
    public static String join(String part1, String part2, String joinString)
    {
        if (joinString == null)
            throw new IllegalArgumentException("null joinString");
        StringBuilder strBuilder = new StringBuilder(20);
        if (part1 != null)
        {
            strBuilder.append(part1);
        }
        if (part2 != null)
        {
            if (strBuilder.length() > 0)
            {
                strBuilder.append(joinString);
            }
            strBuilder.append(part2);
        }

        return strBuilder.toString();
    }


    /**
     * @param items List of objects to be joined
     * @param joinString non-null String to join elements of {items} list
     * @return String consisting of sequence {StringUtil.toString(elem)}
     *         for each {elem} in {list} separated by {joinString}
     */
    public static String join(List items, String joinString)
    {
        StringBuilder strBuilder = new StringBuilder(20);
        for (int i=0; i<items.size(); i++)
        {
            if (strBuilder.length() > 0)
            {
                strBuilder.append(joinString);
            }
            String elem = StringUtil.toString(items.get(i));
            strBuilder.append(elem);
        }

        return strBuilder.toString();
    }


    /**
     * @param items Strings to be joined
     * @param joinString non-null String to join elements of {items} list
     * @return String consisting of sequence {StringUtil.toString(elem)}
     *         for each {elem} in {list} separated by {joinString}
     */
    public static String join(String[] items, String joinString)
    {
        StringBuilder strBuilder = new StringBuilder(20);
        for (int i=0; i<items.length; i++)
        {
            if (strBuilder.length() > 0)
            {
                strBuilder.append(joinString);
            }
            String elem = StringUtil.toString(items[i]);
            strBuilder.append(elem);
        }

        return strBuilder.toString();
    }


    /**
     * Wrapper for String.split(..) but takes regular String as separator,
     * instead of regular expression. Separator string may contain regex
     * meta characters (these are escaped first)
     * @param sourceStr Original string containing tokens and separators
     * @param separator separator string e.g. "|"
     * @return String[] of tokens
     */
    public static String[] split(String sourceStr, String separator)
    {
        if (sourceStr == null)
            throw new IllegalArgumentException("null sourceStr");
        if (separator == null)
            throw new IllegalArgumentException("null separator");

        String sepRegex = escapeRegexMetaChars(separator);
        return sourceStr.split(sepRegex);
    }


    /**
     * Returns token at given position when specified 'sourceStr' is split up
     * by the specified 'separator'.
     * For some reason the String method 'split' does not always work.
     * @param sourceStr Original string containing tokens and separators
     * @param separator separator string e.g. "|"
     * @param tokenIdx index of token to be returned, first token at index 0
     * @return token at requested index position.
     * @throws IllegalArgumentException if not found
     */
    public static String getToken(String sourceStr, String separator, int tokenIdx)
    {
        String[] tokens = split(sourceStr, separator);
        int numTokens = tokens.length;
        if (tokenIdx >= numTokens)
        {
            String msg = "idx=" + tokenIdx + ", but only " + numTokens + " available";
            throw new IllegalArgumentException(msg);
        }

        return tokens[tokenIdx];
    }


    /**
     * Returns number of tokens for specified string.
     * @param sourceStr Original string containing separators
     * @param separator separator string e.g. "|"
     * @return number of tokens
     * @throws IllegalArgumentException if not found
     */
    public static int getTokenCount(String sourceStr, String separator)
    {
        if (sourceStr == null)
            throw new IllegalArgumentException("null sourceStr");
        if (separator == null)
            throw new IllegalArgumentException("null separator");

        String[] tokens = split(sourceStr, separator);
        return tokens.length;
    }


    /**
     * Non-Regex wrapper for String.replaceAll.
     * @param origString
     * @param strToReplace
     * @param replacement
     * @return String based on {origString} but with {replacement} for each {strToReplace}
     */
    public static String replaceAll(String origString, String strToReplace, String replacement)
    {
        String escReplacement = escapeRegexMetaChars(replacement);
        return origString.replaceAll(escReplacement, strToReplace);
    }


    /**
     * Non-Regex wrapper for String.replaceFirst.
     * @param origString
     * @param strToReplace
     * @param replacement
     * @return String based on {origString} but with {replacement} for each {strToReplace}
     */
    public static String replaceFirst(String origString, String strToReplace, String replacement)
    {
        String escReplacement = escapeRegexMetaChars(replacement);
        return origString.replaceFirst(escReplacement, strToReplace);
    }


    /**
     * To escape '.' characters, so String can be used as container key.
     * @param origString String that might include '.' characters
     * @return String based on origString with '.' replaced by ASCII code 1
     * @see #unescapeDots
     */
    public static String escapeDots(String origString)
    {
        return origString.replace('.', (char)1);
    }


    /**
     * To unescape the String, restore the '.' characters
     * @param origString String that might include '.' characters
     * @return String based on origString with ASCII code 1 replaced by '.'
     * @see #escapeDots
     */
    public static String unescapeDots(String origString)
    {
        return origString.replace((char)1, '.');
    }


    /**
     * Returns string based on original string, with whitespace trimmed on left.
     * @param origString String with words.
     * @return origString with leading white-space characters stripped away.
     */
    public static String trimLeft(String origString)
    {
        StringBuffer ltrimmed = new StringBuffer(origString);
        while (ltrimmed.length() > 0 && Character.isWhitespace(ltrimmed.charAt(0)))
        {
            ltrimmed.deleteCharAt(0);
        }

        return ltrimmed.toString();
    }
    
    
    /**
     * @param origString String that might exceed length limit
     * @param lengthLimit int max number of characters for returned string
     * @return origString instance itself OR a substring of the first {lengthLimit} 
     *         chars if origString exceeds the length specified in lengthLimit.
     */
    public static String limit(String origString, int lengthLimit)
    {
        if (origString.length() <= lengthLimit)
            return origString;
        else
            return origString.substring(0, lengthLimit);
    }


    /**
     * Returns first word from specified string.
     * A "Word" here, means any sequence of non-space characters.
     * It is assumed that words are separated by a space character (' '),
     * or a sequence of space characters. Currently does not support other
     * whitespace characters.
     * @param origString
     * @return String of first word (no leading/trailing whitespace)
     *         or "" if there's no word at left of origString
     */
    public static String firstWord(String origString)
    {
        String result = trimLeft(origString); // cut leading whitespace
        int spaceIdx = result.indexOf(" ");
        if (spaceIdx == -1)
        {
            // after trimming whitespace on left, there's no " " left in string.
            // So result of trimming is either the only word, or empty string
            return result;
        }
        else
            return origString.substring(0, spaceIdx);
    }


    /**
     * Returns string with first word stripped away from original string.
     * Here, it is assumed that words are separated by a space character (' '),
     * or a sequence of space characters. Currently does not support other
     * whitespace characters.
     * @param origString String with words.
     * @return origString with first word stripped away.
     */
    public static String removeFirstWord(String origString)
    {
        String result = trimLeft(origString);
        int spaceIdx = result.indexOf(" ");
        if (spaceIdx == -1)
            return "";
        else
        {
            result = result.substring(spaceIdx);
            return trimLeft(result);
        }
    }
}
