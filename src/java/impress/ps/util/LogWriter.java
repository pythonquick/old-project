package impress.ps.util;

import java.io.*;
import java.util.Date;

/**
 *  Class to create Log file entries. Useful during debugging 
 *  See main(..) method for sample usage
 *
 * @author     GHA
 * @created    Sep 9, 2005
 */
public class LogWriter
{
    private String pathToFile;
    private boolean append;
    
    /** private constructor
     * @param pathToFile full path to log file (e.g. "c:\\logs\\myfile.log")
     * @param append true to append to file if it exists, false creates new file
     */
    private LogWriter (String pathToFile, boolean append) 
    {
        this.pathToFile = pathToFile;
        this.append = append;
    }
    
    /** private constructor, LogWriter will append to file if it exists
     * @param pathToFile full path to log file (e.g. "c:\\logs\\myfile.log")
     */
    private LogWriter(String pathToFile)
    {
        this(pathToFile, true); // by default append = true
    }
    
    /** Factory method - creates LogWriter
     * @param pathToFile full path to log file (e.g. "c:\\logs\\myfile.log")
     * @param append true to append to file if it exists, false creates new file
     */
    public static LogWriter create(String pathToFile, boolean append) 
    {
        return new LogWriter(pathToFile, append);
    }
    
    /** Factory method - creates LogWriter that appends to file if it exists
     * @param pathToFile full path to log file (e.g. "c:\\logs\\myfile.log")
     */
    public static LogWriter create(String pathToFile)
    {
        return new LogWriter(pathToFile);
    }
    
    /** Log message to text file on a new line. 
     * @param message any object to log (will use toString() if object not null)
     */
    public void log(Object message)
    {
        Date time = new Date();
        PrintWriter pw = createPW(this.pathToFile, this.append);
        pw.println(time + " | " + message);
        pw.close();
    }
    
    private PrintWriter createPW(String pathToFile, boolean append)
    {
        PrintWriter pw = null;
        try 
        {
            boolean autoFlush = true;
            pw = new PrintWriter(
                    new FileOutputStream(pathToFile, append), autoFlush
                );
        }
        catch (IOException e)
        {
            throw new RuntimeException("IOException while creating PrintWriter", e);
        }
        return pw;
    }
    
    public static void main(String[] args)
    {
        LogWriter lw = new LogWriter("c:\\temp\\msg.txt", true);
        lw.log("One");
        lw.log("Two");
        lw.log("Three");
        lw.log("Four");
        lw.log("Five");
    }
}
