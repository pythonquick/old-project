package impress.ps.util;

import impress.ois.base.dtrans.IContainer;
import impress.ps.util.ContUtil;
import impress.ps.util.StringUtil;

import impress.ois.base.map.imap.rt.XA;
import impress.ois.base.map.imap.rt.BR;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.error.OISException;

import impress.ps.err.domain.General;

/**
 *  Class with RFC processing utility methods
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 21-Jun-2007
 *     Creation
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/RFCUtil.java#4 $
 *
 * @author     GHA
 */
public class RFCUtil
{
    public static final String[] ERROR_TYPES = {"X", "A", "E"};
    
    
    /**
     * Returns whether the specified SAP RETURN type table contains an error.
     * Supports both flat structure, or table.     
     * Returns true if field "TYPE" is "E", "X" or "A" in the  structure, or 
     * in case of a table if at least one row has "TYPE" field value E, X or A.
     * @param sapCnt
     * @return boolean true if returnCnt contains error
     */
    public static boolean isSAPReturnError(IContainer sapCnt)
    {
        return containsSAPError(sapCnt, "TYPE");
    }

    
    /**
     * Returns whether the specified SAP MESSAGE type table contains an error.
     * Supports both flat structure, or table.     
     * Returns true if field "MESSAGE_TYPE" is "E", "X" or "A" in the structure,
     * or in case of a table if at least one row has "TYPE_TYPE" field 
     * value E, X or A.
     * @param sapCnt
     * @return boolean true if returnCnt contains error
     */
    public static boolean isSAPMessageError(IContainer sapCnt)
    {
        return containsSAPError(sapCnt, "MESSAGE_TYPE");
    }
    
    
    /**
     * Returns the error text from specified RETURN structure/table.
     * Multiple errors found are appended with a string in following format:
     * <error message1> + ". " + <error message2> + ". " ... (etc.)
     * @param sapCnt SAP return container (flat structure or table)
     * @return error text collected from "MESSAGE" fields on error (E,A,X) rows
     */
    public static String collectReturnErrorText(IContainer sapCnt)
    {
        return collectSAPErrorText(sapCnt, "TYPE", "MESSAGE");
    }
    
    
    /**
     * Returns the error text from specified MESSAGE structure/table.
     * <error message1> + ". " + <error message2> + ". " ... (etc.)
     * @param sapCnt SAP return container (flat structure or table)
     * @return error text collected from "MESSAGE_TEXT" fields on error (E,A,X) rows
     */
    public static String collectMessageErrorText(IContainer sapCnt)
    {
        return collectSAPErrorText(sapCnt, "MESSAGE_TYPE", "MESSAGE_TEXT");
    }
    
    
    /**
     * Convenience method to call RFC. 
     * Note: Caller must take care of commit/rollback of the XA instance
     * @throws General.SAPCallFailed if RFC call fails
     * @throws General.SAPReturnedErrorOrWorse if RFC returned errors
     * @param xa XA instance
     * @param rfcName
     * @param params the parameter name/values (BR."In" container), for example:
     *        params."I_STATUS.0" = "REL"
     *        params."I_STATUS.1" = "CNF"
     *        params."I_PROJ" = "X-AL-123"
     * @param returnStructName name of RFC's return structure e.g. "RETURN"
     * @param messageStructName name of RFC's message structure e.g. "MESSAGES"
     * @return BR instance from which output of the call an be retrieved
     */
    public static BR callRFC(
        XA xa, 
        String rfcName, 
        IContainer params, 
        String returnStructName, 
        String messageStructName)
    {
        BR br = xa.newBR(rfcName);
        callRFC(br, rfcName, params, returnStructName, messageStructName);
        return br;
    }
    
    
    /**
     * Convenience method to call RFC 
     * BR created from IESystem - no transaction.
     * @throws General.SAPCallFailed if RFC call fails
     * @throws General.SAPReturnedErrorOrWorse if RFC returned errors
     * @param ies IESystem instance
     * @param sapBE name of SAP backend system
     * @param rfcName
     * @param params the parameter name/values (BR."In" container), for example:
     *        params."I_STATUS.0" = "REL"
     *        params."I_STATUS.1" = "CNF"
     *        params."I_PROJ" = "X-AL-123"
     * @param returnStructName name of RFC's return structure e.g. "RETURN"
     * @param messageStructName name of RFC's message structure e.g. "MESSAGES"
     * @return BR instance from which output of the call an be retrieved
     */
    public static BR callRFC(
        IESystem ies, 
        String sapBE, 
        String rfcName, 
        IContainer params, 
        String returnStructName, 
        String messageStructName)
    {
        BR br = ies.newBR(sapBE, rfcName, true); //untouched = true (new connection)
        callRFC(br, rfcName, params, returnStructName, messageStructName);
        return br;
    }
    
    
    /**
     * Convenience method to call RFC. 
     * Note: Caller must take care of commit/rollback of the XA (if applicable)
     * @throws General.SAPCallFailed if RFC call fails
     * @throws General.SAPReturnedErrorOrWorse if RFC returned errors
     * @param br BR instance
     * @param rfcName
     * @param params the parameter name/values (BR."In" container), for example:
     *        params."I_STATUS.0" = "REL"
     *        params."I_STATUS.1" = "CNF"
     *        params."I_PROJ" = "X-AL-123"
     * @param returnStructName name of RFC's return structure e.g. "RETURN"
     * @param messageStructName name of RFC's message structure e.g. "MESSAGES"
     */
    private static void callRFC(
        BR br, 
        String rfcName, 
        IContainer params, 
        String returnStructName, 
        String messageStructName)
    {
        br.setElement("In", params);
        br.process();
        if (!br.wasSuccessful())
        {
            IContainer errCnt = (IContainer)br.getElement("Exception");
            OISException exc = OISException.convertIContainerToException(errCnt);
            throw new General.SAPCallFailed(rfcName, exc);
        }
        
        if (returnStructName != null)
        {
            IContainer returnStructCnt = (IContainer)br.getElement("Out." + returnStructName);
            if (isSAPReturnError(returnStructCnt))
            {
                String sapErrorText = collectReturnErrorText(returnStructCnt);
                if (messageStructName != null)
                {
                    IContainer messageStructCnt = (IContainer)br.getElement("Out." + messageStructName);
                    sapErrorText += collectMessageErrorText(messageStructCnt);
                    throw new General.SAPReturnedErrorOrWorse(rfcName, sapErrorText, messageStructCnt);
                }
                else
                {
                    throw new General.SAPReturnedErrorOrWorse(rfcName, sapErrorText, returnStructCnt);
                }
            }
        }
        else if(messageStructName != null)
        {
            IContainer messageStructCnt = (IContainer)br.getElement("Out." + messageStructName);
            if (isSAPMessageError(messageStructCnt))
            {
                String sapErrorText = collectMessageErrorText(messageStructCnt);
                throw new General.SAPReturnedErrorOrWorse(rfcName, sapErrorText, messageStructCnt);
            }
        }
    }
    
    
    /**
     * Returns the error text from specified RETURN or MESSAGE structure/table.
     * @param sapCnt SAP return/message container (flat structure or table)
     * @param typeField name of field with error level e.g. TYPE or MESSAGE_TYPE
     * @param msgField name of field with error text, e.g. MESSAGE or MESSAGE_TEXT
     * @return error text collected on all message fields on error (E,A,X) rows
     */
    private static String collectSAPErrorText(
        IContainer sapCnt, 
        String typeField, 
        String msgField)
    {
        StringBuffer errorText = new StringBuffer("");
        
        if (sapCnt == null)
            throw new IllegalArgumentException("null sapCnt");
        
        String[] keys = ContUtil.keys(sapCnt);
        for (int i=0; i<keys.length; i++)
        {
            String key = keys[i];
            Object value = sapCnt.getElement(key);
            
            if (typeField.equals(key) && value instanceof String)
            {
                String typeValue = (String)value;
                if (StringUtil.isOneOf(typeValue, ERROR_TYPES))
                {
                    String message = (String)sapCnt.getElement(msgField);
                    if (message != null)
                    {
                        errorText.append(message).append(". ");
                    }
                    break;
                }
            }
            
            if (value instanceof IContainer)
            {
                String errorTextRow = collectSAPErrorText((IContainer)value, typeField, msgField);
                errorText.append(errorTextRow);
            }
        }
        
        return errorText.toString();
    }
    
    
    /**
     * Returns whether the specified RETURN or MESSAGE container contains 
     * an error. Supports both flat structure, or table.
     * Use parameter typeField to specify name of the type field, for example:
     * type field "TYPE" is used in RFC RETURN tables, whereas "MESSAGE_TYPE"
     * is used in RFC MESSAGE tables.
     * Returns true if field value of the type field is E, X or A in the 
     * structure, or in case of a table if at least one row has type E, X or A.
     * @param sapCnt
     * @param typeField name of field with error level e.g. TYPE or MESSAGE_TYPE
     * @return boolean true if returnCnt contains error
     */
    private static boolean containsSAPError(IContainer sapCnt, String typeField)
    {
        if (sapCnt == null)
            throw new IllegalArgumentException("null sapCnt");
        if (typeField == null)
            throw new IllegalArgumentException("null typeField");
        
        String[] keys = ContUtil.keys(sapCnt);
        for (int i=0; i<keys.length; i++)
        {
            String key = keys[i];
            Object value = sapCnt.getElement(key);
            
            if (typeField.equals(key) && value instanceof String)
            {
                String typeValue = (String)value;
                if (StringUtil.isOneOf(typeValue, ERROR_TYPES))
                    return true;
            }
            
            if (value instanceof IContainer)
            {
                boolean errorFound = containsSAPError((IContainer)value, typeField);
                if (errorFound)
                    return true;
            }
        }
        
        return false;
    }

}
