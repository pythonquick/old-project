package impress.ps.util;

import java.util.ResourceBundle;
import java.util.PropertyResourceBundle;
import java.util.MissingResourceException;
import java.util.Enumeration;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Config //implements IContainer
{
    private ResourceBundle resBundle;
    
    private Config(ResourceBundle bundle)
    {
        this.resBundle = bundle;
    }
    
    
    /**
     * @param absPathToConfigFile absolute path to config file
     * @return Config instance from specified config file
     */
    public static Config createFromFile(String absPathToConfigFile)
    {
        try 
        {
            ResourceBundle bundle = new PropertyResourceBundle(
                new FileInputStream(absPathToConfigFile)
            );
            
            return new Config(bundle);
        }
        catch (FileNotFoundException e)
        {
            String msg = "Not found: " + absPathToConfigFile;
            throw new MissingResourceException(msg, "", "");
        }
        catch (IOException e)
        {
            String msg = "IO error while load config file " + absPathToConfigFile;
            throw new RuntimeException(msg, e);
        }
    }
    
    
    /**
     * @param property name of property (on the left of "=" sign in config file)
     * @return value of property (on the right of "=" sign in config file)
     */
    public String getString(String property)
    {
        return resBundle.getString(property);
    }
    
    
    /**
     * Returns array of Strings of full property names, that start with given
     * string, for example, if 'propertyStartStr' is "ProjCode*", then this
     * method might return the following properties if they exist in config:
     * {"ProjCode.SAPCompCode", "ProjCode.TemplateID", "ProjCode.IntCalcProf"}
     *
     * @param wildcardPattern String to match start of property name,
     *        can contain DOS type wildcard characters * or ?
     * @return String[] of property names in config starting with given string
     */
    public String[] getProperties(String wildcardPattern)
    {
        java.util.List propertyNames = new java.util.ArrayList();
        for (Enumeration keys = resBundle.getKeys(); keys.hasMoreElements(); )
        {
            String propertyName = (String)keys.nextElement();
            if (StringUtil.matchWildcardPattern(propertyName, wildcardPattern))
                propertyNames.add(propertyName);
        }
        String[] propertyArr = new String[propertyNames.size()];
        for (int i=0; i<propertyNames.size(); i++)
            propertyArr[i] = (String)propertyNames.get(i);
        return propertyArr;
    }
}
