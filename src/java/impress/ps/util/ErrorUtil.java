/**
 * Class with utility methods for dealing with Exceptions
 *
 * <pre>
 * -----------------------------------------------------------------------------
 * Change history:
 *  1. GHA, 29-Apr-206
 *     Created
 * -----------------------------------------------------------------------------
 * </pre>
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/ErrorUtil.java#2 $
 *
 * @author     GHA
 */

package impress.ps.util;
import impress.ois.base.error.OISException;

public class ErrorUtil
{
    /**
     * Returns an abbreviated stacktrace of the specified Throwable instance
     * The stack trace string that is returned, consists of stack trace elements
     * from the source problem, until the specified method is reached.
     *
     * Example String of a stack trace that should end at method "process":
     *
     * java.lang.ArithmeticException: BigInteger divide by zero
     * Caused by: java.math.MutableBigInteger.divide(Unknown), java.math.BigInte
     * ger.divideAndRemainder(Unknown), java.math.BigDecimal.divide(Unknown), .d
     * ivide(Unknown), Stack.e(74), .d(67), .c(62), .b(57), .a(52), .process(47)
     *
     * @param e Throwable instance
     * @param untilMethod method name where stacktrace should end E.g. "process"
     *                    use null value for untilMethod to process entire trace
     * @return String with compact stack trace
     */
    public static String compactStackTrace(Throwable e, String untilMethod)
    {
        if (e == null)
            throw new IllegalArgumentException("Parameter e is null");
        
        StackTraceElement[] stackTrace = e.getStackTrace();
        StringBuffer sbuf = new StringBuffer();
        sbuf.append(e.getClass().getName());
        sbuf.append(" ");
        sbuf.append(e.getMessage());
        sbuf.append("\nat: ");
        String lastClassName = null;
        for (int i=0; i<stackTrace.length; i++)
        {
            StackTraceElement elem = stackTrace[i];
            String className = elem.getClassName();
            String fileName = elem.getFileName();
            int lineNum = elem.getLineNumber();
            String method = elem.getMethodName();
            
            if (!className.equals(lastClassName))
            {
                lastClassName = className;
                sbuf.append(lastClassName);
            }
            
            sbuf.append(".").append(method);
            String location = lineNum == -1 ? "Unknown" : lineNum + "";
            sbuf.append("(").append(location).append("), ");
            
            if (untilMethod != null && untilMethod.equals(method))
            {
                int furtherElements = stackTrace.length - i - 1;
                sbuf.append("... " + furtherElements + " more");
                break;
            }
        }
        Throwable cause;
        if (e instanceof OISException) 
           cause = ((OISException)e).getCausingException();
        else cause = e.getCause();
        if (cause != null)
        {
            sbuf.append("\n\nCaused by:\n");
            sbuf.append(compactStackTrace(cause, untilMethod));
        }
        return sbuf.toString();
    }
    
    
    /**
     * Returns an abbreviated stacktrace of the specified Throwable instance
     *
     * Same as for the other overloaded version of this method, except that this
     * method does not stop the stack trace at any specified method - the entire
     * stack trace chain is displayed. Calling this method is equivalent to
     * calling the overloaded version of this method and specifying a 'null'
     * untilMethod parameter.
     *
     * @param e Throwable instance
     * @return String with compact stack trace
     */
    public static String compactStackTrace(Throwable e)
    {
        return compactStackTrace(e, null);
    }
    
    
    /**
     * Returns Exception message including code (if code was set)
     * and also adds the cause instance's message (if cause is set and known)
     *
     * Sample cause-message String, if detailed==true:
     * "MyErrClass: Update failed on activity 'A9999' [Code: 91001, Level: 3]\n"
     * +"Caused by: MyErrClass2: Could not read ZWM_ED [Code: 90010, Level: 3]\n"
     * +"Caused by: IllegalStateException: Mandatory ID entry is missing!\n"
     *
     * Sample cause-message String, if detailed==false:
     * "Update failed on activity 'A9999'\n"
     * "Caused by: Could not read ZWM_ED_PV_ACT\n"
     * "Caused by: Mandatory ID entry is missing!\n"
     *
     * @param throwable a Throwable (Exception or Error)
     * @param detailed true if error message should be detailed, which means:
     *        - message includes name of each Throwable class
     *        - message includes code and level of OISException instances
     * @return String chain of causing OISExceptions
     */
    public static String getCausedMsgChain(Throwable throwable, boolean detailed)
    {
        String message = throwable.getMessage();
        String className = throwable.getClass().getName();
        int dotIdx = className.lastIndexOf(".");
        if (dotIdx > -1)
            className = className.substring(dotIdx+1);
        
        String msgChainText;
        
        if (detailed)
        {
            msgChainText = className + ": " + message;
            
            if (throwable instanceof OISException)
            {
                OISException oisexc = (OISException)throwable;
                Integer code = oisexc.getCode();
                Integer level = oisexc.getLevel();
                msgChainText += " [Code: " + code + ", Level: " + level + "]";
            }
        }
        else
        {
            if (message != null)
                msgChainText = message;
            else
                msgChainText = className;
        }
        
        Throwable cause = throwable.getCause();
        if (cause == null && throwable instanceof OISException)
            cause = ((OISException)throwable).getCausingException();
        
        if (cause != null)
        {
            String causeMsg = getCausedMsgChain(cause, detailed);
            msgChainText += "\nCaused by: " + causeMsg;
        }
        else
        {
            msgChainText += "\n";
        }
        
        return msgChainText;
    }
}
