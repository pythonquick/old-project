package impress.ps.util;

import impress.ois.base.map.imap.AbstractIMap;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.map.imap.rt.BR;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.error.OISException;

import impress.ps.util.ContUtil;
import impress.ps.err.domain.General;

/**
 *  Utility class to assist with controlling and calling I.Maps
 *
 * @author    GHA
 * Created    Feb 28, 2006
 */
public class IMapUtil
{
    
    /**
     * Calls the specified method with the specified arguments of specified IMap
     * @param theIMap the IMap on which to call the method
     * @param methName name of the method to call
     * @param inArgs IContainer of arguments, format is <i> = <value of arg i>
     * @return Object whatever the IMap method returns, null if method was void
     */
    public static Object callMethod(AbstractIMap theIMap, 
                                            String methName, IContainer inArgs)
    {
        String[] argKeys = ContUtil.keys(inArgs);
        
        Object[] callArgs = new Object[argKeys.length];
        for (int i=0; i<argKeys.length; i++)
            callArgs[i] = inArgs.getElement(i + ""); // "0", then "1", etc
        
        Class theIMapClass = theIMap.getClass();
        java.lang.reflect.Method[] methods = theIMapClass.getMethods();
        java.lang.reflect.Method toCall = null;
        
        // Find the method to call:
        for (int i=0; i<methods.length; i++)
        {
            java.lang.reflect.Method method = methods[i];
            if (methName.equals(method.getName()))
            {
                toCall = method;
                break;
            }
        }
        if (toCall == null)
        {
            String msg = "Method " + methName + " does not exist";
            throw new IllegalArgumentException(msg);
        }
        
        Object result = null;
        try
        {
            result = toCall.invoke(theIMap, callArgs); // call the method
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalArgumentException e)
        {
            throw new RuntimeException(e);
        }
        catch (java.lang.reflect.InvocationTargetException e)
        {
            Throwable cause = e.getCause();
            throw new RuntimeException(cause); // Throw the cause instead
        }
        return result;
    }
    
    
    /**
     * Convenience method to call IMap 
     * BR created from IESystem - no transaction.
     * @throws General.IMapCallFailed if IMap call fails
     * @param ies IESystem instance
     * @param imapName
     * @param imapParams I.Map "In" container
     * @return BR instance from which output of the call an be retrieved
     */
    public static BR callIMap(
        IESystem ies, 
        String imapName, 
        IContainer imapParams)
    {
        BR br = ies.newCall(imapName);
        br.setElement("In", imapParams);
        
        br.process();
        if (!br.wasSuccessful())
        {
            throw new General.IMapCallFailed(imapName, br);
        }
        
        return br;
    }
}
