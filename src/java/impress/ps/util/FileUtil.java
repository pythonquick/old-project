package impress.ps.util;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.dtrans.GenericContainer;
import impress.ois.base.map.imap.rt.BR;
import impress.ois.base.map.imap.rt.IESystem;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 *  Class with some commonly used file methods.
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 26-Mar-2006
 *     Creation
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/FileUtil.java#3 $
 *
 * @author     GHA
 */
public final class FileUtil
{
    /**
     * @param directory String path representing a filesystem directory
     * @return true if specified directoryPath is in fact a directory
     */
    public static boolean dirExists(String directory)
    {
        File f = new File(directory);
        return f.isDirectory();
    }
    
    
    /**
     * @param directory String path representing a filesystem directory
     * @return String directoryPath ending with path separator ('\' on Windows)
     */
    public static String addPathSepIfMissing(String directory)
    {
        if (directory.endsWith(File.separator))
            return directory;
        else
            return directory + File.separator;        
    }
    
    
    /** 
     * @param directory String
     * @param fileName
     * @return full path to the file, including directoryPath and fileName
     *         with file separator inserted between directory and filename
     * Example:
     *     String myPath2 = FileUtil.pathToFile("c:\\temp\\", "myfile.txt");
     *     String myPath1 = FileUtil.pathToFile("c:\\temp", "myfile.txt");
     *     // now both myPath1 and myPath2 contain "c:\\temp\\myfile.txt"
     *
     */
    public static String pathToFile(String directory, String fileName)
    {
        return addPathSepIfMissing(directory) + fileName;
    }
    
    
    /**
     * Copies file at source path, to a new file created at destination path
     * @param srcPath absolute path to file that should be copied
     * @param dstPath absolute path of where file should be copied to
     */
    public static void copyFile(String srcPath, String dstPath) throws IOException 
    {
        InputStream in = new FileInputStream(srcPath);
        OutputStream out = new FileOutputStream(dstPath);
        
        try
        {
            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        }
        finally
        {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }
    }    
}
