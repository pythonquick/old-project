package impress.ps.util;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 *  Class with some commonly used Date helper methods.
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 9-Aug-2007
 *     Creation
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/DateUtil.java#2 $
 *
 * @author     GHA
 */
public class DateUtil
{
    /**
     * Returns a java.util.Date instance for a given year, month, day, hour, min
     * @param yyyy Year in format YYYY, e.g. "2007"
     * @param mm Month in format MM, e.g. "12" (December)
     * @param dd Day in format DD, e.g. "31"
     * @param hhmm time in format hhmm (hour minute), e.g. "2359"
     * @return Date instance of specified year, month, day, hour and minutes
     */
    public static Date makeDate(String yyyy, String mm, String dd, String hhmm)
    {
        int year;
        int month;
        int day;
        int hour;
        int minutes;
        try
        {
            year = Integer.parseInt(yyyy);
            month = Integer.parseInt(mm);
            day = Integer.parseInt(dd);
            int hourmin = Integer.parseInt(hhmm);
            hour = hourmin / 100;
            minutes = hourmin % 100;
        }
        catch (NumberFormatException e)
        {
            String msg = "Invalid date fields: " + yyyy + ", " + mm + ", " + dd;
            throw new IllegalArgumentException(msg, e);
        }
        
        return makeDate(year, month, day, hour, minutes);
    }
    
    
    /**
     * Returns a java.util.Date instance for a given year, month, day
     * with time 00:00
     * @param yyyy Year in format YYYY, e.g. "2007"
     * @param mm Month in format MM, e.g. "12" (December)
     * @param dd Day in format DD, e.g. "31"
     * @return Date instance of specified year, month, day, and time 00:00
     */
    public static Date makeDate(String yyyy, String mm, String dd)
    {
        return makeDate(yyyy, mm, dd, "0000");
    }
    
    
    /**
     * Returns a java.util.Date instance for a given year, month, day, hour, min
     * @param year e.g. 2006 the year of Football World Cup in Germany
     * @param month month in year e.g. 12 (December)
     * @param day Day in month e.g. 31
     * @param hour the hour of day, e.g. 23 (11pm)
     * @param minute number of minutes after the hour, e.g. 59
     * @return Date instance of specified year, month, day, hour and minutes
     */
    public static Date makeDate(int year, int month, int day, int hour, int minute)
    {
        GregorianCalendar gcal = new GregorianCalendar(year, month-1, day, hour, minute);
        return gcal.getTime();
    }
    
    
    /**
     * Returns a java.util.Date instance for a given year, month, day
     * with time 00:00
     * @param year Year e.g. 2006 the year of Football World Cup in Germany
     * @param month month in year e.g. 12 (December)
     * @param day Day in month e.g. 31
     * @return Date instance of specified year, month, day, and time 00:00
     */
    public static Date makeDate(int year, int month, int day)
    {
        return makeDate(year, month, day, 00, 00);
    }
}
