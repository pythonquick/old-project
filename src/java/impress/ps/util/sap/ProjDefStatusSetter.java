package impress.ps.util.sap;

import impress.ois.base.dtrans.*;
import impress.ps.util.ContUtil;
import impress.ps.util.StringUtil;
import impress.ps.util.RFCUtil;

import impress.ois.base.map.imap.rt.XA;
import impress.ois.base.map.imap.rt.BR;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.error.OISException;

import impress.ps.err.domain.General;

/**
 * <pre>
 * Class for setting/unsetting user statuses on a PS project definition.
 *
 * For example, if user wants to set user status "PLBD" and unset status "PROJ"
 * then the following code can be used:
 *
 *     <code>
 *     String sapBE = "SAP"
 *     String projDef = "X-ABC-ONE";
 *     boolean isSimulate = true;
 *     ProjDefStatusSetter statusSetter = new ProjDefStatusSetter(ie, sapBE, projDef, isSimulate);
 *     statusSetter.userStatusToSet("PLBD").userStatusToUnset("PROJ").execute();
 *     </code>
 *
 * At least one status must be specified:
 * - either a status to be set (method userStatusToSet)
 * - or a status to be unset (method userStatusToUnset)
 * - or both a status to be set and a status to be unset
 *
 * </pre>
 */
public class ProjDefStatusSetter
{
    private static final String
        BAPI_PS_INITIALIZATION = "BAPI_PS_INITIALIZATION",
        BAPI_PS_PRECOMMIT = "BAPI_PS_PRECOMMIT",
        BAPI_BUS2001_SET_STATUS = "BAPI_BUS2001_SET_STATUS",
        BAPI_TRANSACTION_ROLLBACK = "BAPI_TRANSACTION_ROLLBACK",
        BAPI_TRANSACTION_COMMIT = "BAPI_TRANSACTION_COMMIT";
    
    
    public ProjDefStatusSetter(IESystem ies, String sapBE, String projDef, boolean isSimulate)
    {
        this.ies = ies;
        this.sapBE = sapBE;
        this.projDef = projDef;
        this.isSimulate = isSimulate;
        this.userStatusToSet = null;
        this.userStatusToUnset = null;
    }
    
    
    /**
     * Set status to be set. Note: this does not call SAP yet (call execute()).
     * @param user status to be set.
     */
    public ProjDefStatusSetter userStatusToSet(String userStatusToSet)
    {
        this.userStatusToSet = userStatusToSet;
        return this;
    }
    
    
    /**
     * Set status to be unset. Note: this does not call SAP yet (call execute()).
     * @param user status to be unset.
     */
    public ProjDefStatusSetter userStatusToUnset(String userStatusToUnset)
    {
        this.userStatusToUnset = userStatusToUnset;
        return this;
    }
    
    
    /**
     * Makes the sequence of BAPI calls to set and unset the specified statuses
     * @throws ProjDefStatusSetException if status could not be set (e.g. BAPI call failed)
     */
    public void execute() 
    {
        IContainer setStatusParamCnt = new GenericContainer();
        XA xa = ies.newXA(sapBE);
        try
        {
            // Prepare BAPI call:
            setStatusParamCnt.setElement("PROJECT_DEFINITION", projDef);
            if (StringUtil.isFilled(userStatusToSet))
            {
                setStatusParamCnt.setElement("SET_USER_STATUS", userStatusToSet);
            }
            if (StringUtil.isFilled(userStatusToUnset))
            {
                setStatusParamCnt.setElement("UNDO_USER_STATUS", userStatusToUnset);
            }
            
            if (ContUtil.numKeys(setStatusParamCnt) == 1)
            {
                throw new IllegalStateException("No statuses specified to set or unset");
            }
            
            // Call status-setting BAPI and pre-commit:
            RFCUtil.callRFC(xa, BAPI_PS_INITIALIZATION, null, null, null);
            try
            {
                RFCUtil.callRFC(xa, BAPI_BUS2001_SET_STATUS, setStatusParamCnt, "RETURN", "E_RESULT");
                RFCUtil.callRFC(xa, BAPI_PS_PRECOMMIT, null, "ET_RETURN", null);
            }
            catch (Exception e)
            {
                RFCUtil.callRFC(xa, BAPI_TRANSACTION_ROLLBACK, null, null, null);
                String msg = "BAPI call failed. Transaction rolled back";
                throw new ProjDefStatusSetException(msg, e);
            }
            
            // No problems so far. Finish transaction: Commit or Rollback
            String finalBAPI = isSimulate ? BAPI_TRANSACTION_ROLLBACK : BAPI_TRANSACTION_COMMIT;
            RFCUtil.callRFC(xa, finalBAPI, null, null, null);
        }
        catch (Exception e)
        {
            String msg = "Error while setting/unsetting status on project " + projDef
                + ". The following BAPI parameters were used: " + setStatusParamCnt;
            throw new ProjDefStatusSetException(msg, e);
        }
        finally
        {
            if (xa != null)
            {
                xa.rollback();
            }
        }
    }
    

    public static class ProjDefStatusSetException extends RuntimeException
    {
        public ProjDefStatusSetException(String msg, Exception cause)
        {
            super (msg, cause);
        }
    }
    
    
    private IESystem ies;
    private String sapBE;
    private String projDef;
    private boolean isSimulate;
    private String userStatusToSet;
    private String userStatusToUnset;
}
