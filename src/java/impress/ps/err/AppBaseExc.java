package impress.ps.err;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.HashSet;
import impress.ois.base.error.OISException;
import java.text.MessageFormat;
import java.text.Format;
import java.lang.reflect.Constructor;

/**
 * Abstract base class for application error classes.
 * Application error classes can subclass this error class and define two static
 * final fields: 
 * <ol>
 * <li>CODE (unique int error code), and
 * <li>MSG (String error message)
 * </ol>
 *
 * Typically the error classes are contained as static inner classes within an
 * error domain class that groups together error classes belonging to an 
 * error domain.
 * <br>
 * Here is an example of a simple error class:
 *
 * <pre><code>
 * public class MyErrorDomain
 * {
 *     public static class NoSourceID extends AppBaseExc
 *     {
 *         static final int CODE = 81301;
 *         static final String MSG = "No source ID found on entity. ";
 *     }
 * 
 *     // ... More static AppBaseExc inner classes here ...
 * }
 * </code></pre>
 * 
 * The above error class has a default constructor and can be used as follows:
 *
 * <pre><code>
 *     throw new MyErrorDomain.NoSourceID();
 * </code></pre>
 *
 * Error classes can take parameters to make error messages more descriptive:
 * 
 * <pre><code>
 *     public static class SaveCIRFCError extends AppBaseExc
 *     {
 *         static final int CODE = 81702;
 *         static final String MSG = "RFC {0} returned an error message: {1}";
 *         
 *         public SaveCIRFCError(String rfcName, String rfcMessage) {
 *             super (new Object[] {rfcName, rfcMessage});
 *         }
 *     }
 * </code></pre>
 *
 * The above error class has one constructor that takes two parameters
 * 
 * <pre><code>
 *     throw new MyErrorDomain.SaveCIRFCError("Z_MY_RFC", errorMessage);
 * </code></pre>
 *
 * <pre>
 * -----------------------------------------------------------------------------
 * Change history:
 *  1. GHA, 13-Apr-2006
 *     Created
 * -----------------------------------------------------------------------------
 * </pre>
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/AppBaseExc.java#1 $
 *
 * @author     GHA
 */
public abstract class AppBaseExc extends OISException
{
    private static final int NO_ERR_CODE = -1;
    private static final Integer 
        INFO_LEVEL        = new Integer(OISException.INFORMATION),
        WARNING_LEVEL     = new Integer(OISException.WARNING),
        ERROR_LEVEL       = new Integer(OISException.ERROR),
        FATAL_ERROR_LEVEL = new Integer(OISException.FATAL_ERROR);
    /**
     * Simple AppBaseExc default constructor.
     * Subclass MUST declare static final fields CODE (int) and MSG (String)
     */
    public AppBaseExc()
    {
        Integer code = getCODE();
        setCode(code);
        String msg = getMSG();
        setText(msg);
    }
    
    
    /**
     * @param cause Throwable causing exception to chain
     * Subclass MUST declare static final fields CODE (int) and MSG (String)
     */
    public AppBaseExc(Throwable cause)
    {
        this();
        if (cause != null)
            setCausingException(cause);
    }
    
    
    /**
     * @param attribs Object[] array with parameters to substitute into message
     * Subclass MUST declare static final fields CODE (int) and MSG (String)
     */
    protected AppBaseExc(Object[] attribs)
    {
        this(attribs, null);
    }
    
    
    /**
     * @param attribs Object[] array with parameters to substitute into message
     * @param cause Throwable causing exception to chain
     * Subclass MUST declare static final fields CODE (int) and MSG (String)
     */
    protected AppBaseExc(Object[] attribs, Throwable cause)
    {
        Integer code = getCODE();
        setCode(code);
        
        String msgPattern = getMSG();
        MessageFormat msgFormat = new MessageFormat(msgPattern);
        
        // Verify that the number attributes fits the message pattern:
        Format[] formats = msgFormat.getFormatsByArgumentIndex();
        if (formats.length != attribs.length)
        {
            String errMsg = "Wrong number of attributes used for error class "
                + getClass().getName();
            System.out.println(errMsg);
            throw new IllegalArgumentException(errMsg);
        }
        
        String formatted = msgFormat.format(attribs);
        setText(formatted);
        
        if (cause != null)
            setCausingException(cause);
    }
    
    
    /**
     * Returns message including error code (if error code set)
     * @return String message including error code (if error code was set)
     */
    public String getMessage()
    {
        String msg = super.getMessage();
        Integer errCode = getCode();
        if (errCode.intValue() == NO_ERR_CODE)
            return msg;
        else return msg;
    }
    
    
    /**
     * Set error level to Information
     */
    public void setInformation()
    {
        setLevel(INFO_LEVEL);
    }
    
    
    /**
     * Set error level to Warning
     */
    public void setWarning()
    {
        setLevel(WARNING_LEVEL);
    }

    
    /**
     * Set error level to Error
     */
    public void setError()
    {
        setLevel(ERROR_LEVEL);
    }

    
    /**
     * Set error level to Fatal
     */
    public void setFatalError()
    {
        setLevel(FATAL_ERROR_LEVEL);
    }

    
    /**
     * Verifies all of the specified error domain classes for validity.
     * Each of the error domain classes must contain inner error classes
     * that are subclasses of this class (AppBaseExc). These inner classes
     * must adhere to several rules, which are checked. 
     * <ul>
     *   <li>Inner error class must declare unique static final int CODE value
     *   <li>Inner error class must declare unique static final String MSG value
     *   <li>If the inner error class MSG pattern contains place holders for 
     *       parameters, then after constructing the instance, all parameters
     *       must be substituted. For example, the error message pattern
     *       "The RFC {0} returned an error message: {1}" expects two objects.
     *       These two objects must be passed to this class's constructor as
     *       an Object[] (array or Objects).
     * </ul>
     * 
     * It is recommended to call this method from a JUnit test suite that gets
     * called from within the ant build script. That way, the application JAR 
     * file gets built only when all the error classes are valid.
     *
     * @param errDomainClasses String[] of error domain classes (incl. package)
     */
    public static void verifyErrorDomainClasses(String[] errDomainClasses)
    {
        Set uniqueCodes = new HashSet();
        for (int i=0; i<errDomainClasses.length; i++)
        {
            String errDomainClass = errDomainClasses[i];
            verifyInnerClasses(errDomainClass, uniqueCodes);
        }
    }
    
    
    /**
     * Verifies the inner classes of the specified domain class. See method 
     * verifyErrorDomainClasses.
     *
     * It collects all error codes as Integer instances in the specified Set
     *
     * @param domainClass String value of the error domain class (incl. package)
     * @param uniqueCodes Set of Integer values of error codes (param changes!!)
     */
    private static void verifyInnerClasses(String domainClass, Set uniqueCodes)
    {
        try
        {
            Class theClass = Class.forName(domainClass);
            Class[] classes = theClass.getClasses();
            for (int i=0; i<classes.length; i++)
            {
                // Load the class:
                String className = classes[i].getName();
                Class errorClass = Class.forName(className);
                
                // Verify that the inner class is subclass of AppBaseExc:
                Class superclass = errorClass.getSuperclass();
                String superclassName = superclass.getName();
                if (!"impress.ps.err.AppBaseExc".equals(superclassName))
                {
                    String errMsg = "Error class " + errorClass + 
                        " must be a subclass of AppBaseExc";
                    System.out.println(errMsg);
                    throw new IllegalStateException(errMsg);
                }
                
                ////////////////////////////////////////////////////////////////
                // Verify field CODE
                ////////////////////////////////////////////////////////////////
                Integer code;
                try
                {
                    Field codeField = errorClass.getDeclaredField("CODE");
                    if ("int".equals(codeField.getType().getName()))
                        code = new Integer(codeField.getInt(errorClass));
                    else {
                        String errMsg = "field CODE of error class " 
                            + className + " must be an int";
                        System.out.println(errMsg);
                        throw new IllegalStateException(errMsg);
                    }
                } 
                catch (NoSuchFieldException e)
                {
                    String errMsg = "No CODE field in error class " + className;
                    System.out.println(errMsg);
                    throw new RuntimeException(errMsg, e);
                }
                if (uniqueCodes.contains(code))
                {
                    String errMsg = className + ":Duplicate error code " + code;
                    System.out.println(errMsg);
                    throw new RuntimeException(errMsg);
                }
                uniqueCodes.add(code);
                
                ////////////////////////////////////////////////////////////////
                // Verify field MSG
                ////////////////////////////////////////////////////////////////
                Object messageValue = null;
                try
                {
                    Field messageField = errorClass.getDeclaredField("MSG");
                    String messageFieldType = messageField.getType().getName();
                    if ("java.lang.String".equals(messageFieldType))
                        messageValue = messageField.get(errorClass);
                    else {
                        String errMsg = "field MSG of error class " 
                            + className + " must be a java.lang.String";
                        System.out.println(errMsg);
                        throw new IllegalStateException(errMsg);
                    }
                } catch (NoSuchFieldException e) 
                {
                    String errMsg = "No MSG field in error class " + className;
                    System.out.println(errMsg);
                    throw new RuntimeException(errMsg, e);
                }
                if (messageValue == null)
                {
                    String errMsg = "null MSG in error class " + className;
                    System.out.println(errMsg);
                    throw new RuntimeException(errMsg);
                }
                
                // error class contains CODE and MSG fields, so far so good
                // Next, call each constructor and check if all parameters in
                // the MSG string are formatted
                verifyConstructors(errorClass);
            }
        }
        catch (Exception e)
        {
            String errMsg = "Verification of " + domainClass + " failed.";
            System.out.println(errMsg);
            throw new RuntimeException (errMsg, e);
        }
    }
    
    
    /**
     * Verifies that the error message's attributes (if any) are all substituted
     * after calling each of the error class's constructors.
     * @param errorClass Class - the error class (subclass of AppBaseExc)
     */
    private static void verifyConstructors(Class errorClass) throws Exception
    {
        String className = errorClass.getName();
        Constructor[] constructors = errorClass.getConstructors();
        if (constructors.length == 0)
        {
            String errMsg = className + " has no callable/public constructors";
            System.out.println(errMsg);
            throw new RuntimeException(errMsg);
        }
        for (int constr=0; constr<constructors.length; constr++) {
            Constructor constructor = constructors[constr];
            Class[] paramTypes = constructor.getParameterTypes();
            Object[] constrParams = new Object[paramTypes.length];
            
            for (int p=0; p<paramTypes.length; p++) {
                Class param = paramTypes[p];
                Object paramValue = makeDefaultInstance(param);
                constrParams[p] = paramValue;
            }
            
            AppBaseExc inst = (AppBaseExc)constructor.newInstance(constrParams);
            
            try
            {
                if (!allParamsConverted(inst))
                {
                    String errMsg = "A constructor of error class " 
                        + className + " did not convert all MSG parameters!";
                    System.out.println(errMsg);
                    throw new IllegalStateException(errMsg);
                }
            }
            catch (Exception e)
            {
                String msg = "allParamsConverted() failed for " + className;
                throw new RuntimeException(msg, e);
            }
        }
    }
    
    
    /**
     * Helper method for verifying error classes. Creates a default instance
     * of the specified Class
     * @param cl Class
     * @return Object a default instance of the specified Class or null if
     *         the Class is not handled specifically by this method
     */
    private static Object makeDefaultInstance(Class cl)
    {
        String className = cl.getName();
        if ("java.lang.String".equals(className))
            return "<Test-String>";
        else if ("java.lang.Integer".equals(className))
            return new Integer(12345);
        else if ("java.lang.Boolean".equals(className))
            return Boolean.TRUE;
        else if ("java.lang.Character".equals(className))
            return new Character('x');
        else if ("java.lang.Long".equals(className))
            return new Long(12345);
        else if ("java.lang.Float".equals(className))
            return new Float(12345);
        else if ("java.lang.Double".equals(className))
            return new Double(12345);
        else if ("java.math.BigDecimal".equals(className))
            return java.math.BigDecimal.valueOf(12345);
        else return null;
    }
    
    
    /**
     * Helper method for verifying error classes. Returs whether the specified
     * AppBaseExc instance's message (method getMessage()) contains no 
     * '{' or '}' characters in which case all parameters (if there were any) 
     * were converted.
     * @param errInstance AppBaseExc instance
     * @return boolean whether there exists no '{' or '}' chars in the message
     */
    private static boolean allParamsConverted(AppBaseExc errInstance)
    {
        String message = errInstance.getMessage();
        if (message == null)
        {
            String msg = "errInstance: " + errInstance + ": null message";
            throw new IllegalArgumentException(msg);
        }
        return message.indexOf("{") == -1 && message.indexOf("}") == -1;
    }
    
    
    /**
     * Gets the instance's CODE field which must be declared as int
     * @return Integer representing the int CODE field declared in instance
     */
    private Integer getCODE()
    {
        Class theClass = getClass();
        int codeValue;
        try
        {
            Field codeField = theClass.getDeclaredField("CODE");
            codeValue = codeField.getInt(theClass);
        } catch (Exception e) 
        {
            String errMsg = "Could not read field CODE";
            System.out.println(errMsg);
            throw new RuntimeException(errMsg, e);
        }
        return new Integer(codeValue);
    }
    
    
    /**
     * Gets the instance's MSG field which must be declared as String
     * @return String representing the String MSG field declared in instance
     */
    private String getMSG()
    {
        Class myClass = getClass();
        Object msgValue = null;
        try
        {
            Field msgField = myClass.getDeclaredField("MSG");
            msgValue = msgField.get(myClass);
        } catch (Exception e) 
        {
            String errMsg = "Could not read field MSG";
            System.out.println(errMsg);
            throw new RuntimeException(errMsg, e);
        }
        return (String)msgValue;
    }
}
