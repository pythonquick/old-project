package impress.ps.i4epm.logging;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.dtrans.GenericContainer;
import impress.ois.base.map.imap.AbstractIMap;
import impress.ps.util.ContUtil;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.map.imap.rt.BR;

/**
 * Synch log utility class for writing entries to the I4EPM (3.x) synch log
 * Basically, the static methods of this class make entries to the 
 * output container element "PassThrough.SynchLog.<log name>.<n>"
 * where <log name> and some other parameters are specified by the method caller
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 20-Jul-2006
 *     Creation
 * 2.  GHA, 14-Jul-2008
 *     ContUtil.createIfNull replaced with ContUtil.getOrCreateCont
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/SynchLog.java#3 $
 *
 * @author     GHA
 */
public final class SynchLog
{
    private static final Integer NOERRCODE = new Integer(-1);
    private static final Integer ZERO = new Integer(0);
    private static final Integer LEVEL_INFO = new Integer(1);
    private static final Integer LEVEL_WARNING = new Integer(2);
    private static final Integer LEVEL_ERROR = new Integer(3);
    private static final Integer LEVEL_FATAL = new Integer(4);
     
    /**
     * Adds entry to synch log, with specified log entry
     * @param imap AbstractIMap instance
     * @param logName String name of synch log, e.g. "BHP User messages"
     * @param logEntry LogEntry implementing instance with log entry content
     */
    public static void add(AbstractIMap imap, String logName, LogEntry logEntry)
    {
        String prettyFormat = logEntry.prettyFormat();
        String plainFormat = logEntry.plainFormat();
        Integer errCode = logEntry.getErrorCode();
        Integer errLevel = logEntry.getErrorLevel();
        
        String synchLogKey = "PassThrough.SynchLog." + logName;
        addLogEntry(imap, synchLogKey, errLevel, errCode, prettyFormat, plainFormat, null); 
        // (pass null container for now - maybe implement this in future)
    }
    
    
    /**
     * Saves any SynchLog entries in PassThrough container to the repository
     * @param ie magical IESystem
     * @param imapIn IContainer the in-container of the I.Map containing the 
     *        PassThrough container with the SynchLog entries to be flushed
     */
    public static void flushToRepository(IESystem ie, IContainer imapIn)
    {
        GenericContainer synchLog = (GenericContainer)imapIn.getElement("PassThrough.SynchLog");
        if (synchLog == null) 
            return; //nothing to log
        
        Long synchRunId = (Long)imapIn.getElement("SynchRunId");
        String beSchema = (String)imapIn.getElement("TargetSelectionParameters.BESchema");
        String beSchemaView = (String)imapIn.getElement("TargetSelectionParameters.BESchemaView");
        
        String[] logNames = synchLog.getKeys();
        for (int i=0; i<logNames.length; i++)
        {
            String logName = logNames[i];
            IContainer namedSynchLog = (IContainer)synchLog.getElement(logName);
            saveLogEntry(ie, logName, namedSynchLog, synchRunId, beSchema, beSchemaView);
            synchLog.removeElement(logName); // Remove named synch log afterwards!
        }
    }
    
    
    /**
     * Adds entry to synch log, with specified text (no container)
     * @param imap AbstractIMap instance
     * @param synchLogKey String key where to place/add synch log entry
     * @param level Integer level for severity (1=Info, 2=Warn, 3=Err, 4=Fatal)
     * @param code Integer error code / warning code / information code
     * @param msg String message to be written to log
     * @param container IContainer to be written to log (additional info)
     */
    private static void addLogEntry(
        AbstractIMap imap, 
        String synchLogKey, 
        Integer level, 
        Integer code, 
        String prettyFormat, 
        String plainFormat,
        IContainer container)
    {
        IContainer synchLog = ContUtil.getOrCreateCont(imap.out, synchLogKey);
        
        String imapName = nameFor(imap);
        IContainer entry = new GenericContainer();
        String text = "IMap " + imapName + ": ";
        if (code == null || ZERO.equals(code))
            text += prettyFormat;
        else
        {
            if (LEVEL_INFO.equals(level))
                text += " Information code " + code + "\n" + prettyFormat;
            if (LEVEL_WARNING.equals(level))
                text += " Warning code " + code + "\n" + prettyFormat;
            else 
                text += " Error code " + code + "\n" + prettyFormat;
        }

        if (container != null)
            entry.setElement("Container", container);
        entry.setElement("PlainFormat", plainFormat);
        entry.setElement("Text", prettyFormat);
        entry.setElement("Level", level);
        if (!ZERO.equals(code))
            entry.setElement("ErrorCode", code);
        entry.setElement("IMapName", imapName);
        entry.setElement("TimeStamp", new java.util.Date());
        ContUtil.addRow(synchLog, entry);
    }
    
    
    /**
     * Call iePrjEngSaveSynchLogEntry to save the synchLog to the repository
     * @param ie magical ImpressEngine instance
     * @param logName String display name of the synch log line
     * @param synchLog IContainer the actual synch log entries container
     * @param synchRunId
     * @param beSchema
     * @param beSchemaView
     */
    private static void saveLogEntry(IESystem ie, String logName, 
        IContainer synchLog, Long synchRunId, String beSchema, String beSchemaView)
    {
        BR br = ie.newCall("iePrjEngSaveSynchLogEntry");
        br.setElement("In.Name", logName);
        br.setElement("In.System.PassThrough.SynchLog", synchLog); 
        br.setElement("In.System.SynchRunId", synchRunId);
        br.setElement("In.BackendView.BackendSchema.Name", beSchema);
        br.setElement("In.BackendView.Name", beSchemaView);
        
        br.process();        
    }
    
    
    /**
     * @param imap AbstractIMap instance
     * @return name of the specified IMap
     */
    private static String nameFor(AbstractIMap imap)
    {
        String fullClassName = imap.getClass().getName();
        
        // Strip package if there is any:
        String withoutPackage;
        int lastPeriod = fullClassName.lastIndexOf(".");
        if (lastPeriod > -1)
            withoutPackage = fullClassName.substring(lastPeriod+1);
        else withoutPackage = fullClassName;
        
        // Get the inner class name if I.Map is an inner class:
        String plainClassName;
        int lastDollar = withoutPackage.lastIndexOf("$");
        if (lastDollar > -1)
        {
            plainClassName = withoutPackage.substring(lastDollar + 1);
        }
        else 
        {
            plainClassName = withoutPackage;
        }
        return plainClassName;
    }
}
