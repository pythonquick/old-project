package impress.ps.i4epm.logging;
import impress.ois.base.error.OISException;
import impress.ps.util.ErrorUtil;

/**
 * This class implements LogEntry interface. 
 * It represents a warning log entry - suitable for user error message
 * It does not contain details such as the exception stack trace
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 20-Jul-2006
 *     Creation
 * 2.  GHA, 28-Mar-2007
 *     Work with OISException instead of AppBaseExc
 *     Use ErrorUtil.getCausedMsgChain instead of AppBaseExc instance's
 *     getCausedMsgChain method
 * 3.  GHA, 02-Jul-2007
 *     Added boolean parameter to ErrorUtil.getCausedMsgChain
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/FatalError.java#3 $
 *
 * @author     GHA
 */
public class FatalError extends LogEntry
{
    protected OISException exc;
    
    public FatalError(OISException exc)
    {
        this.exc = exc;
    }
    
    
    /**
     * @return String with pretty formatted error report including stack trace
     */
    public String prettyFormat()
    {
        return formatError(true);
    }
    
    
    /**
     * @return String with plain text report including stack trace
     */
    public String plainFormat()
    {
        return formatError(false).trim();
    }
    
    
    /**
     * @param pretty boolean true if should pretty format, false for plain format
     * @return error text report including stack trace, either pretty or plain
     */
    private String formatError(boolean pretty)
    {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("Fatal error report:\n");
        if (pretty)
            sbuf.append(excMsgChain(exc, HORIZONTAL_LINE, HORIZONTAL_LINE, true));
        else
            sbuf.append(excMsgChain(exc, "", "", true));
        sbuf.append("Stack trace:\n");
        sbuf.append(ErrorUtil.compactStackTrace(exc, "process"));
        if (pretty)
            sbuf.append("\n\n");
        return sbuf.toString();
    }
    
    
    /**
     * @return Integer the AppBaseExc exception code
     */
    public Integer getErrorCode()
    {
        return exc.getCode();
    }
    
    
    /**
     * @return corresponding OISException Integer value for FATAL ERROR
     */
    public Integer getErrorLevel()
    {
        return exc.getLevel();
    }
}
