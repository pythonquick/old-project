package impress.ps.i4epm.logging;

import impress.ois.base.error.IExceptionConstants;
import impress.ois.base.dtrans.IContainer;
 
/**
 * Represents a simple text entry
 * -----------------------------------------------------------------------------
 * Change history:
 *
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/TextEntry.java#2 $
 *
 * @author     VVO
 *
 */
public final class TextEntry extends LogEntry
{
    private String msg;
    
    public TextEntry(String msg)
    {
        this.msg = msg;
    }
    
    
    public String prettyFormat()
    {
        StringBuffer sbuf = new StringBuffer("\n");
        if(msg != null && msg.length() > 0)
        {
            sbuf.append(msg);
            sbuf.append("\n");
        }
        return sbuf.toString();
    }
    
    
    public String plainFormat()
    {
        return msg;
    }
    
    
    public Integer getErrorCode()
    {
        return new Integer(IExceptionConstants.CODE_OK);
    }
    
    
    public Integer getErrorLevel()
    {
        return new Integer(IExceptionConstants.CODE_OK);
    }
}
