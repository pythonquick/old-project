package impress.ps.i4epm.logging;

import impress.ois.base.error.IExceptionConstants;
import impress.ois.base.dtrans.IContainer;
 
/**
 * This class implements LogEntry interface. It represents a list of entries
 * -----------------------------------------------------------------------------
 * Change history:
 *
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/ListEntry.java#2 $
 *
 * @author     VVO
 *
 */

import java.util.List;
import java.util.Iterator;

public final class ListEntry extends LogEntry
{
    private List list;
    private String title;
    private Integer level;
    
    public ListEntry(List list, String title, Integer level)
    {
        this.list = list;
        this.title = title;
        this.level = level;
    }

    public String prettyFormat()
    {
        StringBuffer sbuf = new StringBuffer();
        if(title != null) sbuf.append(title);
        sbuf.append("\n");
        if(list != null && !list.isEmpty())
        {
            Iterator iterator = list.iterator();
            while(iterator.hasNext())
            {
                sbuf.append(iterator.next());
                sbuf.append("\n");
            }
        }
        return sbuf.toString();
    }
    
    
    public String plainFormat()
    {
        return prettyFormat().trim();
    }
    
    
    public Integer getErrorCode()
    {
        return new Integer(IExceptionConstants.CODE_OK);
    }
    
    
    public Integer getErrorLevel()
    {
        if(level != null) return level;
        else return new Integer(IExceptionConstants.CODE_OK);
    }
}
