package impress.ps.i4epm.logging;

import impress.ois.base.error.OISException;
import impress.ps.util.ErrorUtil;
/**
 * This abstract class represents a log entry.
 * Implementing classes must (among other methods) implement the prettyFormat 
 * method that will  generate a String representation of the text as it should
 * appear in the synch log. 
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 20-Jul-2006
 *     Creation
 * 2.  GHA, 28-Mar-2007
 *     Use OISException instead of AppBaseExc.
 *     Use ErrorHandler's getCausedMsgChain method instead of AppBaseExc's
 *     method getCausedMsgChain
 * 3.  GHA, 07-Jul-2007
 *     Method excMsgChain now with boolean parameter 'detailed'
 * 4.  GHA, 03-Aug-2007
 *     Added parameters header and footer for excMsgChain
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/LogEntry.java#2 $
 *
 * @author     GHA
 */
public abstract class LogEntry
{
    public static final String HORIZONTAL_LINE 
        = "=========================================================\n";
    
    /**
     * @param throwable e.g. Exception or Error
     * @param header string to place at start of the exception chain
     * @param footer string to place at end of exception chain
     * @param detailed boolean whether to include Exception class names, codes
     * @return String the messages from the exception chain, 
     */
    protected String excMsgChain(Throwable exc, String header, String footer, boolean detailed)
    {
        StringBuffer sbuf = new StringBuffer();
        //sbuf.append("=========================================================\n");
        sbuf.append(header);
        sbuf.append(ErrorUtil.getCausedMsgChain(exc, detailed));
        sbuf.append(footer);
        return sbuf.toString();
    }
    
    public abstract String plainFormat();
    public abstract String prettyFormat();
    public abstract Integer getErrorLevel();
    public abstract Integer getErrorCode();
}
