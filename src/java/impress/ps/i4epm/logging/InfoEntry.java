package impress.ps.i4epm.logging;

import impress.ois.base.error.OISException;
import impress.ois.base.dtrans.IContainer;
import impress.ps.util.ErrorUtil;
/**
 * This class implements LogEntry interface. It represents an Information entry
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 20-Jul-2006
 *     Creation
 * 2.  GHA, 18-Aug-2006
 *     Added constructor that takes AppBaseExc
 * 3.  GHA, 28-Mar-2007
 *     Work with OISException instead of AppBaseExc
 *     Use ErrorUtil.getCausedMsgChain instead of AppBaseExc instance's
 *     getCausedMsgChain method
 * 4.  GHA, 02-Jul-2007
 *     Extra boolean param to ErrorUtil.getCausedMsgChain
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/InfoEntry.java#2 $
 *
 * @author     GHA
 */
public final class InfoEntry extends LogEntry
{
    private String msg;
    private IContainer detail;
    private OISException exc;
    
    public InfoEntry(String msg)
    {
        this.msg = msg;
        this.detail = null;
        this.exc = null;
    }
    
    
    public InfoEntry(String msg, IContainer detail)
    {
        this.msg = msg;
        this.detail = detail;
        this.exc = null;
    }

    
    /**
     * Constructs an InfoEntry with the message text built from the message
     * chains of the specified AppBaseExc instance
     * @param applExc AppBaseExc instance that contains a message (-chain)
     */
    public InfoEntry(OISException oisExc)
    {
        this.exc = oisExc;
        this.msg = null;
        this.detail = null;
    }
    
    
    public String prettyFormat()
    {
        return formatInfo(true);
    }
    
    
    public String plainFormat()
    {
        return formatInfo(false).trim();
    }
    
    
    /**
     * @param pretty boolean true if should pretty format, false for plain format
     * @return info text, either pretty or plain
     */
    private String formatInfo(boolean pretty)
    {
        StringBuffer sbuf = new StringBuffer();
        if (msg != null)
            sbuf.append(msg);
        if (exc != null)
        {
            if (pretty)
            {
                sbuf.append("Info entry:\n");
                sbuf.append(excMsgChain(exc, HORIZONTAL_LINE, HORIZONTAL_LINE, false));
            }
            else
                sbuf.append(excMsgChain(exc, "", "", false));
        }
        if (detail != null)
        {
            sbuf.append("\nContainer with additional information:\n");
            sbuf.append(detail);
        }
        if (pretty)
            sbuf.append("\n");
        return sbuf.toString();
    }
    
    
    public Integer getErrorCode()
    {
        return new Integer(0);
    }
    
    
    public Integer getErrorLevel()
    {
        return new Integer(OISException.INFORMATION);
    }
}
