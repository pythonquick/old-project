package impress.ps.i4epm.logging;
import impress.ois.base.error.OISException;
import impress.ps.util.ErrorUtil;

/**
 * This class implements LogEntry interface. 
 * It represents a low level Error log entry - suitable for developer error
 * messages. Method prettyFormat returns a String description that includes
 * the exception stack trace.
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 20-Jul-2006
 *     Creation
 * 2.  GHA, 28-Mar-2007
 *     Work with OISException instead of AppBaseExc
 * 3.  GHA, 02-Jul-2007
 *     Extra boolean param to ErrorUtil.getCausedMsgChain
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/LowlevelErr.java#3 $
 *
 * @author     GHA
 */
public final class LowlevelErr extends HighlevelErr
{
    public LowlevelErr(OISException exc)
    {
        super(exc);
    }
    
    
    /**
     * @return pretty formatted error report, including the stack trace
     */
    public String prettyFormat()
    {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("Low-level error report:\n");
        sbuf.append(excMsgChain(exc, HORIZONTAL_LINE, HORIZONTAL_LINE, true));
        sbuf.append("Stack trace:\n");
        sbuf.append(ErrorUtil.compactStackTrace(exc, "process"));
        sbuf.append("\n\n");
        return sbuf.toString();
    }
    
    
    /**
     * @return plain formatted error report, including the stack trace
     */
    public String plainFormat()
    {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("Low-level error report:\n");
        sbuf.append(excMsgChain(exc, "", "", true));
        sbuf.append("Stack trace:\n");
        sbuf.append(ErrorUtil.compactStackTrace(exc, "process").trim());
        return sbuf.toString();
    }
}
