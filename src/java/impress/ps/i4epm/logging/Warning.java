package impress.ps.i4epm.logging;
import impress.ois.base.error.OISException;

/**
 * This class implements LogEntry interface. 
 * It represents a warning log entry - suitable for user error message
 * It does not contain details such as the exception stack trace
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 20-Jul-2006
 *     Creation
 * 2.  GHA, 28-Mar-2007
 *     Use OISException instead of AppBaseExc
 * 3.  GHA, 02-Jul-2007
 *     Extra boolean param to ErrorUtil.getCausedMsgChain
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/Warning.java#2 $
 *
 * @author     GHA
 */
public class Warning extends LogEntry
{
    protected OISException exc;
    
    public Warning(OISException exc)
    {
        this.exc = exc;
    }
    
    
    public String prettyFormat()
    {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("Warning report:\n");
        sbuf.append(excMsgChain(exc, HORIZONTAL_LINE, HORIZONTAL_LINE, false));
        sbuf.append("\n");
        return sbuf.toString();
    }
    
    
    public String plainFormat()
    {
        return excMsgChain(exc, "", "", false).trim();
    }
    
    
    public Integer getErrorCode()
    {
        return exc.getCode();
    }
    
    
    public Integer getErrorLevel()
    {
        return exc.getLevel();
    }
}
