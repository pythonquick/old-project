package impress.ps.anonomized;

import impress.ps.concurrency.Lock;

/**
 * Class for locking a SAP project scan job.
 * Such a job includes the following:
 * - scanning for new eligible "REDY" projects in SAP
 * - creating workspaces for such projects
 * - creating P3e projects for such workspaces
 * - starting an initial Project mapping rule for such workspaces
 *
 * At the start of a project scan job, this ProjectScanJobLock singleton's lock
 * will be "acquired". No other scan job can then start until the current one 
 * "release"s the lock. When acquiring a lock, the caller specifies a timeout
 * (number of seconds) when the lock will expire
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 13-Jun-2007
 *     Creation
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/ProjectScanJobLock.java#2 $
 *
 */
public class ProjectScanJobLock extends Lock
{
    public static Lock instance;
    
    private ProjectScanJobLock() {}
    
    
    protected long lockTimeout()
    {
        return 1000 * 60 * 5; // 5 minute;
    }
    

    public static Lock getInstance()
    {
        if (instance == null)
            instance = new ProjectScanJobLock();
        return instance;
    }

}
