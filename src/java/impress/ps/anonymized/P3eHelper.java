package impress.ps.anonomized;

import impress.ois.p3e.intf.common.value.ObjectId;
import impress.ois.p3e.intf.common.value.EndDate;
import impress.ois.p3e.intf.common.value.spread.ResourceAssignmentSpread;

import impress.ois.p3e.intf.integration.client.GlobalObjectManager;
import impress.ois.p3e.intf.integration.client.Session;
import impress.ois.p3e.intf.integration.client.bo.BusinessObjectException;
import impress.ois.p3e.intf.integration.client.bo.helper.BOHelperMap;
import impress.ois.p3e.intf.integration.client.bo.BOHelper;
import impress.ois.p3e.intf.integration.client.bo.BOIterator;
import impress.ois.p3e.intf.integration.client.bo.BusinessObject;
import impress.ois.p3e.intf.integration.client.bo.object.Activity;
import impress.ois.p3e.intf.integration.client.bo.object.ActivityCode;
import impress.ois.p3e.intf.integration.client.bo.object.ActivityCodeAssignment;
import impress.ois.p3e.intf.integration.client.bo.object.ActivityCodeType;
import impress.ois.p3e.intf.integration.client.bo.object.CostAccount;
import impress.ois.p3e.intf.integration.client.bo.object.EPS;
import impress.ois.p3e.intf.integration.client.bo.object.NotebookTopic;
import impress.ois.p3e.intf.integration.client.bo.object.Project;
import impress.ois.p3e.intf.integration.client.bo.object.ProjectNote;
import impress.ois.p3e.intf.integration.client.bo.object.ResourceAssignment;
import impress.ois.p3e.intf.integration.client.bo.object.Resource;
import impress.ois.p3e.intf.integration.client.bo.object.ResourceRate;
import impress.ois.p3e.intf.integration.client.bo.object.ProjectCode;
import impress.ois.p3e.intf.integration.client.bo.object.ProjectCodeAssignment;
import impress.ois.p3e.intf.integration.client.bo.object.ProjectCodeType;
import impress.ois.p3e.intf.integration.client.bo.object.UDFType;
import impress.ois.p3e.intf.integration.client.bo.object.UDFValue;
import impress.ois.p3e.intf.integration.client.bo.enm.ActivityCodeTypeScope;
import impress.ois.p3e.intf.integration.client.bo.enm.SpreadPeriodType;
import impress.ois.p3e.intf.integration.client.bo.enm.UDFDataType;
import impress.ois.p3e.intf.integration.client.bo.enm.UDFSubjectArea;
import impress.ois.p3e.intf.PrimaveraException;
import impress.ois.p3e.intf.ServerException;
import impress.ois.p3e.stat.Interface;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.map.imap.rt.XA;
import impress.ois.base.map.imap.rt.BR;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.dtrans.GenericContainer;
import impress.ps.util.StringUtil;
import impress.ps.util.ContUtil;
import impress.ps.err.domain.General;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ArrayList;


import impress.ps.anonomized.err.domain.P3e;

/**
 * Class for Primavera API helper methods
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 18-Jun-2007
 *     Creation
 * 2.  GHA, 05-Jun-2008
 *     Overloaded method p3eProjectExists
 * 3.  GHA, 30-Jun-2008
 *     Method setNotebookTopicText. Various text for 'throw' statements updated
 * 4.  GHA, 01-Jul-2008
 *     New method createResource
 * 5.  GHA, 02-Jul-2008
 *     Renamed getActivityCodeTypeObjectId to getActivityCodeType
 *     Renamed exception ProjectUDFNotFound to UDFNotFound
 *     New methods: loadActivities, getActivityCodeType, getActivityCodeValue, 
 *     getActivityUDFType, setActivityUDFText
 * 6.  GHA, 03-Jul-2008
 *     Method loadActiveResourceRate
 * 7.  GHA, 09-Sep-2008
 *     Method getProjectUDFType's exception message corrected 
 *     ("Project" instead of "Activity").
 *     New method getActivityUDFValue
 * 8.  GHA, 17-Sep-2008
 *     New methods:
 *     setActivityCodeValue, getActivityCode
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/P3eHelper.java#10 $
 *
 */
public final class P3eHelper 
{
    static final String[] NO_FIELDS = new String[] { };
    static final SimpleDateFormat YYYY_MM_DD_FORMATTER = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    
    
    /**
     * Creates a P3e project as a copy of the specified template P3e project
     * @param ie IESystem interface
     * @param p3eProjName name for new P3e project
     * @param p3eBE name of P3e backend schema
     * @param p3eTemplProj name of P3e project to copy
     * @param p3eEpsPathOrNode EPS node/path where to place newly created P3e project
     *        path is a "." separated node string
     */
    public static void createP3eProjectIfNotExist
    (
        IESystem ie,
        String p3eProjName,
        String p3eBE,
        String p3eTemplProj,
        String p3eEpsPathOrNode
    )
    throws 
        P3e.TemplateProjNotFound,
        P3e.ProjectAlreadyExists,
        P3e.EPSNotFound 
    {
        XA xa = ie.newXA(p3eBE);
        try
        {
            BR br = xa.newBR("Get");
            br.process();
            Session sess = (Session)br.getElement("Out.Session");
            Interface stat = (Interface)br.getElement("Out.Static");

            if (!p3eProjectExists(sess, stat, p3eProjName)) 
            {
                GlobalObjectManager gom = sess.getGlobalObjectManager();
                String whereClause = "Id = '" + p3eTemplProj + "'";
                BOIterator projIterator = gom.loadProjects(Project.getAllFields(stat), whereClause, null);

                if (!projIterator.hasNext()) 
                {
                    throw new P3e.TemplateProjNotFound(p3eTemplProj);
                }

                // get the leaf (last) node from the EPS path of nodes:
                String[] epsNodes = StringUtil.split(p3eEpsPathOrNode, ".");
                String finalEpsNode = epsNodes[epsNodes.length - 1];

                // Get Object ID of last EPS node of the EPS path:
                ObjectId epsObjId = getEpsObjectId(sess, stat, finalEpsNode);

                // Create a new P3e project as a copy of the template:
                Project srcProj = Project.wrap$Project(projIterator.next(), stat);
                ObjectId trgObjId = srcProj.createCopy(epsObjId, null, null, null);

                // Update the name ("Id") of the project
                String[] fieldsToLoad = {"Id"};
                Project trgProj = Project.load(sess, fieldsToLoad, trgObjId, stat);
                trgProj.setId(p3eProjName);
                trgProj.update();
            }
            else
                throw new P3e.ProjectAlreadyExists(p3eProjName);
        }
        finally
        {
            if (xa != null)
                xa.commit();
        }
    }
    
    
    /**
     * @param ie IESystem interface
     * @param p3eBE name of P3e backend schema
     * @param p3eProjName name for new P3e project
     */
    public static boolean p3eProjectExists(IESystem ie, String p3eBE, String p3eProjName)
    {
        XA xa = ie.newXA(p3eBE);
        try
        {
            BR br = xa.newBR("Get");
            br.process();
            Session sess = (Session)br.getElement("Out.Session");
            Interface stat = (Interface)br.getElement("Out.Static");

            return p3eProjectExists(sess, stat, p3eProjName);
        }
        finally
        {
            if (xa != null)
                xa.commit();
        }        
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param p3eProjName name of P3e project name
     * @return true if specified P3e project already exists, else false
     */
    public static boolean p3eProjectExists
    (
        Session sess,
        Interface stat,
        String p3eProjName) 
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String whereClause = "Id = '" + p3eProjName + "'";
        BOIterator boi = gom.loadProjects(Project.getAllFields(stat), whereClause ,null);
        return boi.hasNext();
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param epsNodeName name of P3e EPS node
     * @return ObjectId for requested EPS
     */
    public static ObjectId getEpsObjectId(
        Session sess,
        Interface stat,
        String epsNodeName
    ) 
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String whereClause = "Id = '" + epsNodeName + "'";
        String[] fieldsToLoad = {"Id", "ObjectId"};
        BOIterator boi = gom.loadEPS(fieldsToLoad, whereClause ,null);
        if (boi.hasNext())
            return EPS.wrap$EPS(boi.next(), stat).getObjectId();
        else {
            throw new P3e.EPSNotFound(epsNodeName);
        }
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param epsObjectId ObjectId for an EPS node
     * @param fieldsToLoad EPS field names to load
     * @return EPS instance for the specified EPS ObjectId
     * @throws General.NotFound if no EPS for the ObjectId found
     */
    public static EPS loadEPS(
        Session sess,
        Interface stat,
        ObjectId epsObjectId,
        String[] fieldsToLoad
    )   throws General.NotFound
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String whereClause = "ObjectId = '" + epsObjectId + "'";
        BOIterator boi = gom.loadEPS(fieldsToLoad, whereClause ,null);
        if (boi.hasNext())
            return EPS.wrap$EPS(boi.next(), stat);
        else {
            throw new General.NotFound("EPS for objectId: " + epsObjectId);
        }
    }
    
    
    /**
     * Creates new P3e Activity Code type (not the code value) with length 20
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj P3e Project instance
     * @param activityCodeName name of activity code type
     */
    public static ActivityCodeType createP3eActivityCodeType
    (
        Session sess,
        Interface stat,
        Project proj,
        String activityCodeName
    )
    {
        ActivityCodeType actCodeType = ActivityCodeType.new$ActivityCodeType(sess,stat);
        actCodeType.setProjectObjectId(proj.getObjectId());
        actCodeType.setLength(20);
        actCodeType.setScope(ActivityCodeTypeScope.getPROJECT(stat));
        actCodeType.setName(activityCodeName);
        actCodeType.setObjectId(actCodeType.create());
        return actCodeType;
    }
    
    
    /**
     * Create Activity Code values (incl. description per value)
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param actCodeType
     * @param actCodesCnt container with activity code value/description pairs:
     *        {i}."Value" = {Value of activity code}
     *        {i}."Descr" = {Description of activity code value}
     */
    public static void createP3eActCodeValues(
        Session sess,
        Interface stat,
        ActivityCodeType actCodeType,
        IContainer actCodesCnt
    )
    {
        String[] keys = ContUtil.keys(actCodesCnt);
        for (int i=0; i<keys.length; i++) 
        {
            IContainer actCodeRow = (IContainer)actCodesCnt.getElement(keys[i]);
            String value = (String)actCodeRow.getElement("Value");
            String descr = (String)actCodeRow.getElement("Descr");
            
            ActivityCode actCode = ActivityCode.new$ActivityCode(sess,stat);
            actCode.setCodeTypeObjectId(actCodeType.getObjectId());
            actCode.setCodeValue(value);
            actCode.setDescription(descr);
            try
            {
                actCode.setObjectId(actCode.create());
            }
            catch (ServerException e)
            {
                // Can ignore exception, when setting duplicate value
            }
        }
    }
    
    
    /**
     * Create Resource with specified ID and name
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param resourceId ID of resource
     * @param resourceName Name of resource
     */
    public static Resource createResource(
        Session sess,
        Interface stat,
        String resourceId,
        String resourceName
    )
    {
        Resource newResource = Resource.new$Resource(sess, stat);
        newResource.setId(resourceId);
        newResource.setName(resourceName);
        newResource.setObjectId(newResource.create());
        return newResource;
    }
    
    
    /**
     * Load the active ResourceRate of specified Resource. "Active" means, the
     * ResourceRate entry with the latest effective date that either matches
     * or precedes the current system date.
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param resource
     * @param fieldsToLoad fields to load on ResourceRate instance
     */
    public static ResourceRate loadActiveResourceRate(
        Session sess,
        Interface stat,
        Resource resource,
        String[] fieldsToLoad
    )
    {
        String dateToday = YYYY_MM_DD_FORMATTER.format(new Date());
        String whereClause = "EffectiveDate <= '" + dateToday + "'";
        String orderBy = "EffectiveDate desc";
        BOIterator boi = resource.loadResourceRates(fieldsToLoad, whereClause, orderBy);
        if (boi.hasNext())
            return ResourceRate.wrap$ResourceRate(boi.next(), stat);
        else
        {
            throw new P3e.NoActiveResourceRateFound(dateToday);
        }
    }

    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param projId ID (name) of P3e project
     * @param fieldsToLoad fields to load on Project instance
     * @return Project instance for specified P3e project ID (project name)
     * @throws P3e.ProjectNotFound
     */
    public static Project loadProject
    (
        Session sess,
        Interface stat,
        String projectName,
        String[] fieldsToLoad
    ) 
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String whereClause = "Id = '" + projectName + "'";
        
        BOIterator boi = gom.loadProjects(fieldsToLoad, whereClause ,null);
        if (boi.hasNext())
            return Project.wrap$Project(boi.next(), stat);
        else 
        {
            throw new P3e.ProjectNotFound("Project: " + projectName);
        }
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj
     * @param activityId
     * @param fieldsToLoad fields to load on Activity instance
     * @return Activity instance
     * @throws P3e.ActivityNotFound
     */
    public static Activity loadActivity(
        Session sess,
        Interface stat,
        Project proj,
        String activityId,
        String[] fieldsToLoad
    )
    {
        String whereClause = "Id = '" + activityId + "'";
        BOIterator boi = proj.loadAllActivities(fieldsToLoad, whereClause, null);
        if (boi.hasNext())
            return Activity.wrap$Activity(boi.next(), stat);
        else
        {
            String projName;
            try
            {
                projName = proj.getName();
            }
            catch (Exception e)
            {
                projName = "(unknown)";
            }
            throw new P3e.ActivityNotFound(projName, activityId);
        }
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj
     * @param fieldsToLoad fields to load on Activity instance
     * @return List of all Activity instances of given project
     */
    public static List loadActivities(
        Session sess,
        Interface stat,
        Project proj,
        String[] fieldsToLoad
    )
    {
        String whereClause = null;
        BOIterator boi = proj.loadAllActivities(fieldsToLoad, whereClause, null);
        List activities = new ArrayList();
        while (boi.hasNext())
        {
            activities.add(Activity.wrap$Activity(boi.next(), stat));
        }
        
        return activities;
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param resourceId
     * @param fieldsToLoad fields to load on Resource instance
     * @return Resource
     * @throws P3e.ResourceNotFound
     */
    public static Resource loadResource(
        Session sess,
        Interface stat,
        String resourceId, 
        String[] fieldsToLoad
    )
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String whereClause = "Id = '" + resourceId + "'";
        BOIterator boi = gom.loadResources(fieldsToLoad, whereClause, null);
        if (boi.hasNext())
            return Resource.wrap$Resource(boi.next(), stat);
        else
        {
            throw new P3e.ResourceNotFound(resourceId);
        }
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param costAccountId
     * @param fieldsToLoad fields to load on CostAccount instance
     * @return CostAccount
     */
    public static CostAccount loadCostAccount(
        Session sess,
        Interface stat,
        String costAccountId,
        String[] fieldsToLoad
    )
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String whereClause = "Id = '" + costAccountId + "'";
        BOIterator boi = gom.loadCostAccounts(fieldsToLoad, whereClause, null);
        if (boi.hasNext())
            return CostAccount.wrap$CostAccount(boi.next(), stat);
        else
        {
            throw new P3e.CostAccountNotFound(costAccountId);
        }
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param activity Activity instance 
     * @param resourceId
     * @param costAccountId
     * @param fieldsToLoad fields to load on ResourceAssignment instance
     * @return ResourceAssignment
     */
    public static ResourceAssignment loadResourceAssignment(
        Session sess,
        Interface stat,
        Activity activity,
        String resourceId,
        String costAccountId,
        String[] fieldsToLoad
    )
    {
        String whereClause = "ResourceId = '" + resourceId + "'"
            + " and CostAccountId = '" + costAccountId + "'";
        BOIterator boi = activity.loadResourceAssignments(fieldsToLoad, whereClause, null);
        if (boi.hasNext())
            return ResourceAssignment.wrap$ResourceAssignment(boi.next(), stat);
        else
            throw new P3e.RANotFound(activity.getId(), resourceId);
    }
    
    
    /**
     * Return a ResourceAssignmentSpread instance, that can be used to determine
     * how the cost or unit values are spread for a certain period.
     */
    public static ResourceAssignmentSpread loadRASpread(
        Session sess,
        Interface stat,
        int raObjectId,
        String[] spreadFieldsToLoad,
        Date startDate,
        Date endDate
    )
    {
        ResourceAssignment raForSpread = ResourceAssignment.loadWithLiveSpread(
            sess,
            spreadFieldsToLoad,
            ObjectId.new$ObjectId(raObjectId, stat),
            spreadFieldsToLoad,
            SpreadPeriodType.getMONTH(stat),
            startDate,
            endDate,
            true,
            stat
        );

        ResourceAssignmentSpread raSpread = raForSpread.getResourceAssignmentSpread();    
        return raSpread;
    }
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param projId ID (name) of P3e project
     */
    public static void deleteProject
    (
        Session sess,
        Interface stat,
        String projId
    ) 
    {
        Project proj = loadProject(sess, stat, projId, new String[]{"ObjectId"});
        proj.delete();
    }
    
    
    /**
     * Get ProjectCode with specified project code name
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj P3e Project instance
     * @param projCodeName name of project code field
     * @param fieldsToLoad fields to load on returned ProjectCodeType instance
     * @return ProjectCodeType instance
     */
    public static ProjectCodeType getProjectCodeType(
        Session sess,
        Interface stat,
        String projCodeName,
        String[] fieldsToLoad
    ) 
    {
        String whereClause = "Name = '" + projCodeName + "'";
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        BOIterator boi = gom.loadProjectCodeTypes(fieldsToLoad, whereClause ,null);
        if (boi.hasNext())
            return ProjectCodeType.wrap$ProjectCodeType(boi.next(), stat);
        else {
            throw new P3e.CodeTypeNotFound(projCodeName);
        }
    }
    
    
    /**
     * Get ProjectCode with specified project code name
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param projCodeType ProjectCodeType instance
     * @param projCodeValue value of project code field
     * @param fieldsToLoad fields to load on returned ProjectCode instance
     * @return ProjectCode instance having specified name and value
     */
    public static ProjectCode getProjectCode(
        Session sess,
        Interface stat,
        ProjectCodeType projCodeType,
        String projCodeValue,
        String[] fieldsToLoad
    ) 
    {
        String projCodeWhereClause = "CodeValue = '" + projCodeValue + "'";
        BOIterator boi = projCodeType.loadAllProjectCodes(fieldsToLoad, projCodeWhereClause, null);
        
        if (!boi.hasNext())
        {
            String codeTypeName = projCodeType.getName();
            throw new P3e.CodeNotFound(codeTypeName, projCodeValue);
        }
        ProjectCode projCode = ProjectCode.wrap$ProjectCode(boi.next(), stat);
        return projCode;
    }
    
    
    /**
     * Get value of ProjectCode assigned to specified project
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj Project instance
     * @param projCodeTypeName name of project code
     * @return String value of specified project's project code
     * @throws General.NotFond if project code value not assigned to value
     */
    public static String getProjectCodeValue(
        Session sess,
        Interface stat,
        Project proj,
        String projCodeTypeName
    ) 
    {
        String whereClause = "ProjectObjectId = '" + proj.getObjectId() + "'"
            + " and ProjectCodeTypeName = '" + projCodeTypeName + "'";
        BOIterator boi = proj.loadProjectCodeAssignments(new String[] {"ProjectCodeValue"}, whereClause, null);
        if (!boi.hasNext())
            throw new General.NotFound("Code value for " + projCodeTypeName + " on project");

        ProjectCodeAssignment projCodeAssgn = ProjectCodeAssignment.wrap$ProjectCodeAssignment(boi.next(), stat);
        return  projCodeAssgn.getProjectCodeValue();
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param projCodeType ProjectCodeType instance
     * @param projCodeValue value to set for Project Code
     */
    public static void setProjectCode(
        Project proj, 
        ProjectCodeType projCodeType, 
        ProjectCode projCode
    )
    {
        proj.setCodeValueObjectId(projCodeType.getObjectId(), projCode.getObjectId());
        proj.update();
    }    
    
    
    /**
     * Create the specified ProjectCode field
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj P3e Project instance
     * @param projCodeName name of project code field
     * @return ProjectCodeType instance
     */
    public static ProjectCodeType createProjectCodeType(
        Session sess,
        Interface stat,
        Project proj,
        String projCodeName
    ) 
    {
        ProjectCodeType projCodeType = ProjectCodeType.new$ProjectCodeType(sess, stat);
        projCodeType.setObjectId(proj.getObjectId());
        projCodeType.setLength(20);
        projCodeType.setName(projCodeName);
        projCodeType.setObjectId(projCodeType.create());
        
        return projCodeType;
    }
    
    
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param actCodeTypeName name of activity code type
     * @return ActivityCodeType (with only field ObjectId loaded)
     * @throws P3e.ActCodeTypeNotFound
     */
    public static ActivityCodeType getActivityCodeType(
        Interface stat,
        Project proj,
        String actCodeTypeName
    ) 
    {
        String whereClause = "Name = '" + actCodeTypeName + "'";
        final String[] fieldsToLoad = new String[]{"ObjectId", "Name"};
        BOIterator boi = proj.loadActivityCodeTypes(fieldsToLoad, whereClause ,null);
        if (boi.hasNext())
            return ActivityCodeType.wrap$ActivityCodeType(boi.next(), stat);
        else 
            throw new P3e.ActCodeTypeNotFound(actCodeTypeName);
    }
    
    
    /**
     * Get value of ActivityCode assigned to specified activity
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param activity Activity instance
     * @param actCodeTypeName name of activity code
     * @return String value of specified activity's activity code
     * @throws General.NotFond if activity code value not assigned to value
     */
    public static String getActivityCodeValue(
        Session sess,
        Interface stat,
        Project proj,
        Activity activity,
        String actCodeTypeName
    ) 
    {
        String whereClause = "ActivityObjectId = '" + activity.getObjectId() + "'"
            + " and ActivityCodeTypeName = '" + actCodeTypeName + "'";
        BOIterator boi = proj.loadAllActivityCodeAssignments(new String[] {"ActivityCodeValue"}, whereClause, null);
        if (!boi.hasNext())
            throw new General.NotFound("Code value for " + actCodeTypeName + " on activity");

        ActivityCodeAssignment actCodeAssgn = ActivityCodeAssignment.wrap$ActivityCodeAssignment(boi.next(), stat);
        return  actCodeAssgn.getActivityCodeValue();
    }
    
    
    /**
     * Get ActivityCode with specified activity code name, 
     * loading only specified ActivityCode fields.
     * @param stat Impress P3e API Interface
     * @param actCodeType ProjectCodeType instance
     * @param actCodeValue value of project code field
     * @param fieldsToLoad fields to load on returned ProjectCode instance
     * @return ActivityCode instance having specified name and value
     */
    public static ActivityCode getActivityCode(
        Interface stat,
        ActivityCodeType actCodeType,
        String actCodeValue,
        String[] fieldsToLoad
    ) 
    {
        String actCodeWhereClause = "CodeValue = '" + actCodeValue + "'";
        BOIterator boi = actCodeType.loadAllActivityCodes(fieldsToLoad, actCodeWhereClause, null);
        
        if (!boi.hasNext())
        {
            String codeTypeName = actCodeType.getName();
            throw new P3e.CodeNotFound(codeTypeName, actCodeValue);
        }
        ActivityCode actCode = ActivityCode.wrap$ActivityCode(boi.next(), stat);
        return actCode;
    }
    
    
    /**
     * @param stat Impress P3e API Interface
     * @param proj Project instance
     * @param activity Activity instance
     * @param actCodeName name of activity code type
     * @param actCodeValue activity code value to assign to the activity
     * @throws P3e.CodeNotFound if there's no Activity code with value {actCodeValue}
     * @throws P3e.ActCodeTypeNotFound if activity code type does not exist
     */
    public static void setActivityCodeValue(
        Interface stat,
        Project proj,
        Activity activity, 
        String actCodeName, 
        String actCodeValue
    )
    {
        ActivityCodeType actCodeType = P3eHelper.getActivityCodeType(stat, proj, actCodeName);
        final String[] fieldsToLoad = {"ObjectId"};
        ActivityCode actCode = P3eHelper.getActivityCode(
            stat,
            actCodeType,
            actCodeValue,
            fieldsToLoad
        );
        activity.setCodeValueObjectId(actCodeType.getObjectId(), actCode.getObjectId());
        activity.update();
    }      
    
    
    /**
     * Get value of Activity UDF assigned to specified activity.
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param project Project instance to which activity belongs
     * @param activity Activity instance
     * @param actUDFName name of activity UDF
     * @return UDFValue instanceof specified activity's UDF
     *         This instance then needs to be queried depending on its type, e.g.
     *             String text = udfValue.getText(); // if type is Text
     *         or: Number cost = udfValue.getCost(); // if type is Cost
     *         or: Date date = udfValue.getStartDate(); // if type is Start-Date
     * @throws General.NotFound if no activity UDF value found (not set)
     * @throws P3e.UDFNotFound if UDF does not exist
     */
    public static UDFValue getActivityUDFValue(
        Session sess,
        Interface stat,
        Project project,
        Activity activity,
        String actUDFName
    ) 
    {
        UDFType activityUDFType = getActivityUDFType(sess, stat, actUDFName);
        String whereClause = "ProjectObjectId = '" + project.getObjectId() + "'"
            + " and ForeignObjectId = '" + activity.getObjectId() + "'"
            + " and UDFTypeObjectId = '" + activityUDFType.getObjectId() + "'";
        String[] udfFields = {"CodeValue", "Cost", "Double", "FinishDate", "Integer", "StartDate", "Text"};
        BOIterator boi = activity.loadUDFValues(udfFields, whereClause, null);
        if (!boi.hasNext())
            throw new General.NotFound("UDF value for " + actUDFName+ " on activity");

        UDFValue udfValue = UDFValue.wrap$UDFValue(boi.next(), stat);
        return  udfValue;
    }
    

    /**
     * Get NotebookTopic with specified name
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param notebookTopicName name of NotebookTopic
     * @param fieldsToLoad fields to load on returned NotebookTopic instance
     * @return NotebookTopic instance
     * @throws General.NotFound
     */
    public static NotebookTopic loadNotebookTopic(
        Session sess,
        Interface stat,
        String notebookTopicName,
        String[] fieldsToLoad
    ) 
    {
        String whereClause = "Name = '" + notebookTopicName + "'";
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        BOIterator boi = gom.loadNotebookTopics(fieldsToLoad, whereClause ,null);
        if (boi.hasNext())
            return NotebookTopic.wrap$NotebookTopic(boi.next(), stat);
        else {
            throw new General.NotFound("Notebook Topic: " + notebookTopicName);
        }
    }
    
    
    /**
     * Get ProjectNote instance for specified NotebookTopic on 
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj P3e Project instance
     * @param nbTopic NotebookTopic instance
     * @param fieldsToLoad fields on returned ProjectNote instance
     * @return ProjectNote
     * @throws General.NotFound
     */
    public static ProjectNote loadProjectNote(
        Session sess,
        Interface stat,
        Project proj,
        NotebookTopic nbTopic,
        String[] fieldsToLoad
    ) 
    {
        String projNoteWhereClause = "ProjectObjectId = '" + proj.getObjectId() + "'";
        BOIterator boi = nbTopic.loadProjectNotes(fieldsToLoad, projNoteWhereClause, null);
        
        if (!boi.hasNext())
        {
            String nbTopicName = nbTopic.getName();
            throw new General.NotFound("Notebook topic " + nbTopicName + " on project");
        }
        ProjectNote projNote = ProjectNote.wrap$ProjectNote(boi.next(), stat);
        return projNote;
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj P3e Project instance
     * @param nbTopic NotebookTopic instance
     * @param text content to set on the project's notebook topic note
     */
    public static void setNotebookTopicText(
        Session sess,
        Interface stat,
        Project proj,
        NotebookTopic nbTopic,
        String text
    )
    {
        ProjectNote projNote;
        try
        {
            // Load ProjectNote, in case it exists already on the Project:
            String[] NB_FIELDS_TO_LOAD = {"ObjectId", "Note"};
            projNote = loadProjectNote(sess, stat, proj, nbTopic, NB_FIELDS_TO_LOAD);
        }
        catch (General.NotFound e)
        {
            // Or create it, if it didn't already exist on the Project
            projNote = ProjectNote.new$ProjectNote(sess, stat);
            projNote.setProjectObjectId(proj.getObjectId());
            projNote.setNotebookTopicObjectId(nbTopic.getObjectId());
            projNote.setObjectId(projNote.create());
        }
        
        projNote.setNote(text);
        projNote.update();
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param udfFieldName
     * @return UDFType matching udfFieldName
     * @throws P3e.ProjectUDFNotFound
     */
    public static UDFType getProjectUDFType(
        Session sess,
        Interface stat,
        String projUDFField
    ) throws P3e.UDFNotFound
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String projSubjArea = stat.getIntegrationClientBoEnmUDFSubjectArea$PROJECT().getValue();
        String whereClause = "SubjectArea='"+ stat.sIntegrationUtilWhereClauseHelper$escapeSqlString(projSubjArea)+"' "
                            + "and Title='" + projUDFField + "'";
        BOIterator boiUDFTypes = gom.loadUDFTypes( stat.sIntegrationClientBoObjectUDFType$getAllFields(), whereClause,null);

        if (boiUDFTypes != null && boiUDFTypes.hasNext())
        {
            return stat.wIntegrationClientBoObjectUDFType(boiUDFTypes.next());
        }
        else
            throw new P3e.UDFNotFound("Project UDF: " + projUDFField);
    }
    
    
    
    
    
    /**
     * Translates specified udf data type String, into UDFDataType instance
     * @param stat Impress P3e API Interface
     * @param udfDataType, one of the following values:
     *        "Text", "Start Date", "Finish Date", "Cost", "Double", "Integer"
     *        "Indicator", "NULL"
     * @return UDFDataType corresponding to the specified String udfDataType
     */
    public static UDFDataType translateUDFDataType(Interface stat, String udfDataType)
    {
        if ("Text".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$TEXT();
        else if("Start Date".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$START_DATE();
        else if("Finish Date".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$FINISH_DATE();
        else if("Cost".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$COST();
        else if("Double".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$DOUBLE();
        else if("Integer".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$INTEGER();
        else if("Indicator".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$INDICATOR();
        else if("NULL".equals(udfDataType))
            return stat.getIntegrationClientBoEnmUDFDataType$NULL();
        else
            throw new IllegalArgumentException("udfDataType: " + udfDataType);
    }    
    
    
    /**
     * Translates UDF subject area String, into UDFSubjectArea instance
     * @param stat Impress P3e API Interface
     * @param subjectAreaString, one of the following values:
     *        "Project", "Activity"
     * @return UDFSubjectArea corresponding to specified String subjectAreaString
     */
    public static UDFSubjectArea translateSubjectArea(Interface stat, String subjectAreaString)
    {
        if ("Project".equals(subjectAreaString))
            return stat.getIntegrationClientBoEnmUDFSubjectArea$PROJECT();
        else if("Activity".equals(subjectAreaString))
            return stat.getIntegrationClientBoEnmUDFSubjectArea$ACTIVITY();
        else
            throw new IllegalArgumentException("subjectAreaString: " + subjectAreaString);
    }    
    
    
    /**
     * Create a UDF type
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param title name of UDF field
     * @param dataType (see method translateUDFDataType for possible values)
     * @param subjectArea UDFSubjectArea instance for project, activity, etc.
     * @return UDFType instance
     */
    public static UDFType createUDFType(
        Session sess,
        Interface stat,
        String title, 
        String dataType, 
        String subjectArea)
    {
        UDFDataType udfDataType = translateUDFDataType(stat, dataType);
        UDFSubjectArea udfSubjectArea = translateSubjectArea(stat, subjectArea);
        
        UDFType udfType = stat.cIntegrationClientBoObjectUDFType(sess);
        udfType.setTitle(title);
        udfType.setSubjectArea(udfSubjectArea);
        udfType.setDataType(udfDataType);
        udfType.setObjectId(udfType.create());
        
        return udfType;
    }
    
    
    /**
     * Set value for specified project UDF date field
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj Project instance
     * @param udfType UDFType for the field to update
     * @param finishDate Date "Finish date" value to insert into UDF
     */
    public static void setProjectUDFFinishDate(
        Session sess,
        Interface stat,
        Project proj,
        UDFType udfType,
        Date finishDate)
    {
        UDFValue udfValue = UDFValue.new$UDFValue(sess, udfType.getObjectId() , proj.getObjectId(), stat);
        udfValue.setFinishDate(EndDate.new$EndDate(finishDate, stat));
        
        try
        {
            udfValue.update();
        }
        catch (PrimaveraException e)
        {
            udfValue.setObjectId(udfValue.create());
        }
    }
    
    
    /**
     * Set value for specified project UDF text field
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param proj Project instance
     * @param udfType UDFType for the field to update
     * @param text the text value to insert into the project's UDF
     */
    public static void setProjectUDFText(
        Session sess,
        Interface stat,
        Project proj,
        UDFType udfType,
        String text)
    {
        UDFValue udfValue = UDFValue.new$UDFValue(sess, udfType.getObjectId() , proj.getObjectId(), stat);
        udfValue.setText(text);
        
        try
        {
            udfValue.update();
        }
        catch (PrimaveraException e)
        {
            udfValue.setObjectId(udfValue.create());
        }
    }
    
    
    /**
     * Adds a UDF container entry to the 'udfCnt' UDF container
     * @param udfCnt container with UDF rows - will add row to this container
     * @param udfTitle name of UDF field
     * @param udfType type of UDF, e.g. "Text", "Finish Date"
     * @param udfValue value of UDF
     */
    public static void setUDF(
        IContainer udfCnt, 
        String udfTitle, 
        String udfType,
        Object udfValue)
    {
        IContainer udfRow = new GenericContainer();
        udfRow.setElement("Title", udfTitle);
        udfRow.setElement("Type", udfType);
        udfRow.setElement("Value", udfValue);
        
        ContUtil.addRow(udfCnt, udfRow);
    }
    
    
    /**
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param actUserDefinedField
     * @return UDFType matching {actUserDefinedField}
     * @throws P3e.UDFNotFound
     */
    public static UDFType getActivityUDFType(
        Session sess,
        Interface stat,
        String actUserDefinedField
    ) throws P3e.UDFNotFound
    {
        GlobalObjectManager gom = sess.getGlobalObjectManager();
        String projSubjArea = stat.getIntegrationClientBoEnmUDFSubjectArea$ACTIVITY().getValue();
        String whereClause = "SubjectArea='"+ stat.sIntegrationUtilWhereClauseHelper$escapeSqlString(projSubjArea)+"' "
                            + "and Title='" + actUserDefinedField + "'";
        BOIterator boiUDFTypes = gom.loadUDFTypes( stat.sIntegrationClientBoObjectUDFType$getAllFields(), whereClause,null);

        if (boiUDFTypes != null && boiUDFTypes.hasNext())
        {
            return stat.wIntegrationClientBoObjectUDFType(boiUDFTypes.next());
        }
        else
            throw new P3e.UDFNotFound("Activity UDF: " + actUserDefinedField);
    }
    
    
    /**
     * Set value for specified activity UDF text field
     * @param sess P3e communication object
     * @param stat Impress P3e API Interface
     * @param activity Activity instance
     * @param udfType UDFType for the field to update
     * @param text the text value to insert into the activity's UDF
     */
    public static void setActivityUDFText(
        Session sess,
        Interface stat,
        Activity activity,
        UDFType udfType,
        String text)
    {
        UDFValue udfValue = UDFValue.new$UDFValue(sess, udfType.getObjectId() , activity.getObjectId(), stat);
        udfValue.setText(text);
        
        try
        {
            udfValue.update();
        }
        catch (PrimaveraException e)
        {
            udfValue.setObjectId(udfValue.create());
        }
    }
    
    
    /**
     * Sets a Project Code to specified 'projCodeCnt' container
     * @param projCodeCnt project code container - will add row to this container
     * @param projCodeName name of project code
     * @param projCodeValue value of project code
     * @param projCodeLen max length of code field
     * @param projCodeDesc description of project code
     */    
    public static void setProjectCode(
        IContainer projCodeCnt,
        String projCodeName,
        Object projCodeValue,
        Integer projCodeLen,
        String projCodeDesc)
    {
        IContainer projCodeRow = new GenericContainer();
        
        if (projCodeValue == null || "".equals(projCodeValue))
            throw new IllegalArgumentException("empty code value for " + projCodeName);
        
        projCodeRow.setElement("CodeTypeName", projCodeName);
        projCodeRow.setElement("Length", projCodeLen);
        projCodeRow.setElement("CodeValue", projCodeValue);
        projCodeRow.setElement("Description", projCodeDesc);
        
        ContUtil.addRow(projCodeCnt, projCodeRow);
    }
    
    
    /**
     * Sets a Project Code to specified 'projCodeCnt' container
     * @param codeCnt code container rows (e.g. activity codes), format:
     *        {i} = {Code row (container)}
     *        e.g. "Data.Project.0.System.ProjectCodes" container
     * @param codeName name of code field
     * @return String value of code field
     * @throws General.NotFound if code field not found
     */    
    public static String lookupCode(IContainer codeCnt, String codeName) throws General.NotFound
    {
        if (codeName == null)
            throw new IllegalArgumentException("null codeName");
        
        String[] keys = ContUtil.keys(codeCnt);
        for (int i=0; i<keys.length; i++)
        {
            IContainer codeRow = (IContainer)codeCnt.getElement(keys[i]);
            if (codeName.equals(codeRow.getElement("CodeTypeName")))
                return (String)codeRow.getElement("CodeValue");
        }
        
        throw new General.NotFound("Code field " + codeName);
    }
    
    
    /**
     * @param udfCnt UDF container rows, format:
     *        {i} = {UDF row (container)}
     *        (e.g. the "Data.Project.0.System.UDF" container)
     * @param udfName name of UDF field
     * @return Object value of UDF
     * @throws General.NotFound if UDF field value not found
     */
    public static Object lookupUDF(IContainer udfCnt, String udfName) throws General.NotFound
    {
        if (udfName == null)
            throw new IllegalArgumentException("null udfName");
        
        String[] keys = ContUtil.keys(udfCnt);
        for (int i=0; i<keys.length; i++)
        {
            IContainer udfRow = (IContainer)udfCnt.getElement(keys[i]);
            if (udfName.equals(udfRow.getElement("Title")))
                return udfRow.getElement("Value");
        }
        throw new General.NotFound("UDF field " + udfName);
    }
}
