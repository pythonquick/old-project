package impress.ps.anonomized;


import impress.ps.anonomized.ContFieldAccessor;
import impress.ps.util.ContUtil;


/**
 * Class for PassThrough container key constants specific to the 
 * Anonomized solution.
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 21-Jun-2008
 *     Creation
 * 2.  GHA, 02-Jul-2008
 *     Added WBS_STATUSES
 * 3.  GHA, 30-Oct-2008
 *     Changed following constants from type ContFieldAccessors to String:
 *     - GROUPED_BF_DATA
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/PassThrough.java#3 $
 */
public final class PassThrough
{
    public static final ContFieldAccessor
        //FCST_SMRY = forField(ProjConst.FCST_SMRY_CONT_KEY),
        ACT_INFO = forField("ActInfo"),
        PROJ_INFO = forField("ProjInfo");
    
    
    public static final String
        WBS_STATUSES = passThroughField("WbsStatuses"),
        GROUPED_BF_DATA = passThroughField("GroupedBFData");
    
    
    private static String passThroughField(String keyInPassThrough)
    {
        return ContUtil.createKey("PassThrough", keyInPassThrough);
    }
    
    
    private static ContFieldAccessor forField(String contKey)
    {
        return ContFieldAccessor.createFromKey("PassThrough." + contKey);
    }
}
