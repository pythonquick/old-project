package impress.ps.anonomized;

import impress.ps.err.domain.General;
import impress.ois.base.rep.ConfigFileProperties;
import java.io.File;

/**
 * Class for field constants specific to the Anonomized solution.
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 13-Jun-2007
 *     Creation
 * 2.  GHA, 05-Jun-2008
 *     Added BACKFIT_PROJ_MAPPING_RULE
 * 3.  GHA, 12-Jun-2008
 *     Added UPL_FCST_MAPPING_RULE
 * 3.  GHA, 25-Jun-2008
 *     Added P3E_NONLABOR_RES_TYPE, WS_CONT_FCST_SMRY_KEY, WS_CONT_PREFIX,
 *           UPL_BUDGET_MAPPING_RULE, P3E_RES_NAME, FINISH_DATE,
 *           FCST_SMRY_CONT_KEY, FCST_SMRY_RES, FCST_SMRY_SAP_PROJ_DEF,
 *           FCST_SMRY_P3E_PROJ_NAME, FCST_SMRY_GEN_DATE
 * 4.  GHA, 01-Jul-2008
 *     Renamed BACKFIT_PROJ_MAPPING_RULE to UPDATE_PROJ_MAPPING_RULE
 *     Renamed PROJECT_CREATION_PROJ_MAPPING_RULE to CREATE_PROJ_MAPPING_RULE
 * 5.  GHA, 14-Jul-2008
 *     Added BUDGET_DWNL_TRIGGER_VALUE, BUDGET_DWNL_STATUS_SUCCESS_VALUE,
 *     BUDGET_DWNL_STATUS_PARTIAL_SUCCESS_VALUE, SAP_BUDGET_ZTABLE, 
 *     BUDGET_DWNL_STATUS_NO_BUDGET_VALUE, SAP_ACTUALS_ZTABLE,
 * 6.  GHA, 01-Aug-2008
 *     Changed constant FORECAST_UPLOAD_SAP_STATUS's value from "FCST" to "UFCR"
 *     FORECAST_UPLOAD_SAP_STATUS renamed to FORECAST_FLATFILE_READY_SAP_STATUS
 * 7.  GHA, 16-Sep-2008
 *     New constant: LOCK_ACQUIREMENT_TIMEOUT
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/ProjConst.java#11 $
 */
public final class ProjConst 
{
    public static final String 
        APPL_CONFIG_DIR_PROPERTY = "ApplConfigDir",
        APPL_CONFIG_FILE = "psp3e_impress.properties",
        
        // UDFs trigger and status values for budget
        // (see also ConfParam.BUDGET_STATUS_CODE_NAME)
        BUDGET_STATUS_TRIGGER = "PLBD",
        BUDGET_STATUS_SUCCESS_VALUE = "UPLD",
        BUDGET_DWNL_TRIGGER_VALUE = "BURD",
        BUDGET_DWNL_STATUS_SUCCESS_VALUE = "BUDL",
        BUDGET_DWNL_STATUS_PARTIAL_SUCCESS_VALUE = "BUPT",
        BUDGET_DWNL_STATUS_NO_BUDGET_VALUE = "BUNO",
        
        // UDFs trigger and status values for forecast
        // (see also ConfParam.FORECAST_STATUS_CODE_NAME)
        FORECAST_STATUS_TRIGGER = "FCRU",
        FORECAST_STATUS_SUCCESS_VALUE = "UFCR",
        
        ACT_TRIGGER_VALUE = "ACRY",
        ACT_STATUS_SUCCESS_VALUE = "ACDL",
        ACT_STATUS_PARTIAL_SUCCESS_VALUE = "ACPT",
        ACT_STATUS_NO_ACTUALS_VALUE = "ACNO",
        
        // SAP Z tables:
        SAP_BUDGET_ZTABLE = "ZWM_ED_PV_BDG",
        SAP_ACTUALS_ZTABLE = "ZWM_ED_PV_ACT",
        
        // Indicators for Budget or Forecast
        BUDGET_INDICATOR = "PLAN",
        FORECAST_INDICATOR = "FCST",
        
        // Flat file types (part of flat file name):
        FLATFILE_LABOR = "Labor",
        FLATFILE_NONLABOR = "NonLabor",
        
        // P3e resource type for "Labor", used to identify LOE ResAssignments:
        P3E_LABOR_RES_TYPE = "Labor",
        P3E_NONLABOR_RES_TYPE = "Nonlabor",
        
        // Flat file container fields for Remaining cost/unit values:
        REMAINING_COST = "RemainingCost",
        REMAINING_UNITS = "RemainingUnits",
        
        // UDF types:
        UDF_TEXT = "Text",
        UDF_FINISH_DATE = "Finish Date",
        
        // Primavera entities
        P3E_PROJECT = "Project",
        P3E_ACTIVITY = "Activity",
        
        // SAP upload statuses (depending on budget or forecast upload):
        BUDGET_UPLOAD_SAP_STATUS = "PLBD",
        FORECAST_FLATFILE_READY_SAP_STATUS = "UFCR",
        
        // Synchronization parameter values:
        WS_ACTIVATION_MODE_SYNCPARA = "ActivationMode",
        WS_DEACTIVATE_IF_CLOSED = "DeactivateIfClosed",
        WS_REACTIVATE_IF_NOT_CLOSED = "ReactivateIfNotClosed",
        
        // Email character encoding:
        CODE_PAGE = "iso8859-1",
        
        // SMTP (email) backend connector name:
        SMTP_BACKEND_NAME = "EMail",
        
        // Constants for workspace, queues, workspace groups, mapping rules:
        WS_CONT_FCST_SMRY_KEY = "FcstSmry",
        WS_CONT_PREFIX = "custPrjWs",
        WS_GROUP_ACTIVE = "Active",
        WS_GROUP_INACTIVE = "Inactive",
        WS_QUEUE = "custPrjQueDefault1",
        SAPPSP3E_APPL_MODULE = "SAPPSP3e",
        CREATE_PROJ_MAPPING_RULE = "Create Project, PS->P3e",
        UPDATE_PROJ_MAPPING_RULE = "Update Project, PS->P3e",
        DWNLD_ACTUALS_MAPPING_RULE = "Download Actuals, PS->P3e",
        UPL_FCST_MAPPING_RULE = "Upload Forecast, P3e->PS",
        UPL_BUDGET_MAPPING_RULE = "Upload Original Budget, P3e->PS",
        
        // P3e field names:
        DATA_DATE           = "DataDate",
        COST_ACCOUNT_ID     = "CostAccountId",
        P3E_RES_TYPE = "ResourceType",
        P3E_RES_NAME = "ResourceName",
        P3E_REMAINING_COST = "RemainingCost",
        FINISH_DATE = "FinishDate",
        
        // Any other field keys, and other constants:
        FCST_SMRY_CONT_KEY = "FcstSmry",
        FCST_SMRY_RES = "Resources",
        FCST_SMRY_SAP_PROJ_DEF = "ProjDef",
        FCST_SMRY_P3E_PROJ_NAME = "ProjDefDescr",
        FCST_SMRY_GEN_DATE = "GenerationDate";
        
    // ImpressServer instance IDs:
    public static Long
        DEV_INST = Long.valueOf(3),
        TEST_INST = Long.valueOf(4),
        QA_INST = Long.valueOf(5),
        PROD_INST = Long.valueOf(6);
    
    public static String[] P3ELOAD_PROJ_NON_PDI = {DATA_DATE};
    
    public static String[] P3ELOAD_ACT_NON_PDI = {FINISH_DATE};
    
    public static final String[] P3ELOAD_RA_NON_PDI = {
        COST_ACCOUNT_ID, P3E_REMAINING_COST, P3E_RES_TYPE, P3E_RES_NAME
    };
    
    // Timeout - max time to wait for acquiring a impress.ps.concurrency.Log instance:
    public static final int LOCK_ACQUIREMENT_TIMEOUT = 1000 * 60 * 1; // 1 minute
    

    /**
     * @return absolute path to config file (constant APPL_CONFIG_FILE)
     *         in directory as configured in ois.properties by property
     *         (constant APPL_CONFIG_DIR_PROPERTY)
     */
    public static final String pathToConfigFile()
    {
        String confFileDir = ConfigFileProperties.getStringValue(APPL_CONFIG_DIR_PROPERTY);
        if (confFileDir == null)
        {
            String msg = "Property " + APPL_CONFIG_DIR_PROPERTY + " from ois.properties";
            throw new General.Missing(msg);
        }
        if (!confFileDir.endsWith(File.separator))
            confFileDir += File.separator;
        
        return confFileDir + APPL_CONFIG_FILE;
    }
}
