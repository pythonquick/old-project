package impress.ps.anonomized;


import impress.ois.base.dtrans.IContainer;


/**
 * Class for workspace selection parameter names, accessed in various I.Maps.
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 2007-08-10
 *     Creation
 * 2.  GHA, 2008-06-20
 *     Added general selection parameters BE_SCHEMA and BE_SCHEMA_VIEW
 * 3.  GHA, 2008-09-26
 *     Added selection parameter WSC_WORKSPACE_GROUP
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/WSParam.java#3 $
 */
public final class WSParam
{
    public static final String 
        SRC_SEL_PARA_KEY = "SourceSelectionParameters",
        TRG_SEL_PARA_KEY = "TargetSelectionParameters",
        
        // General selection parameters:
        BE_SCHEMA = "BESchema",
        BE_SCHEMA_VIEW = "BESchemaView",
        
        // SAP selection parameters:
        SAP_COMP_CODE = "SAPCompCode",
        SAP_BUDGET_STATUS = "BudgetStatus",
        SAP_BUDGET_YEAR = "BudgetYear",
        SAP_NETW_CREATION_WBS_DRIVEN = "NetworkCreationWBSDriven",
        SAP_PROJ_DEFINITION = "ProjectDefinition",
        SAP_WBS_SEPARATOR = "WbsElementSeparator",
        
        // P3e selection parameters:
        P3E_EPS_SHORT_NAME = "EpsShortName",
        P3E_INITIAL_EPS_PATH = "InitialEpsPath",
        P3E_TEMPLATE = "P3eTemplate",
        P3E_PROJ_DEFINITION = "ProjectDefinition",
        P3E_PROJ_DESCRIPTION = "ProjectDescription",
        P3E_WBS_SEPARATOR = "WbsElementSeparator",
        
        // WorkspaceCollection selection parameters:
        WSC_WORKSPACE_GROUP = "WorkspaceGroupName";
    
    
    /**
     * @param inCont container with "SourceSelectionParameters" sub-container
     * @param paramName name of source selection parameter
     * @return Object value for paramName in the source parameters
     */
    public static Object sourceSelParam(IContainer inCont, String paramName)
    {
        return selectionParam(inCont, SRC_SEL_PARA_KEY, paramName);
    }
    
    
    /**
     * @param inCont container with "TargetSelectionParameters" sub-container
     * @param paramName name of target selection parameter
     * @return Object value for paramName in the target parameters
     */
    public static Object targetSelParam(IContainer inCont, String paramName)
    {
        return selectionParam(inCont, TRG_SEL_PARA_KEY, paramName);
    }
    
    
    /**
     * @param inCont I.Map "In" container
     * @param selParaCntKey key under which to find either source/target parameters
     * @param paramName name of source/target selection parameter key
     * @return Object value for paramName under source/target selection parameters container
     */
    public static Object selectionParam(IContainer inCont, String selParaCntKey, String paramName)
    {
        IContainer selectionParams = (IContainer)inCont.getElement(selParaCntKey);
        if (selectionParams == null)
            throw new IllegalArgumentException("inCont does not have " + selParaCntKey);
        if (paramName == null)
            throw new IllegalArgumentException("null paramName");
        
        return selectionParams.getElement(paramName);
    }
}
