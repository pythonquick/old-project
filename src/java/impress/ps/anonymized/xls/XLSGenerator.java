package impress.ps.anonomized.xls;

import java.util.regex.Pattern;
import java.util.regex.Matcher;                      
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.dtrans.GenericContainer;
import impress.ps.util.StringUtil;
import impress.ps.util.ContUtil;
import java.util.List;
import java.util.ArrayList;

/**
 * Class to generate text lines for Excel XLS XML lines, based on a template
 * Excel XLS XML lines that contain substitution rules (in XML tag attributes), 
 * and a substitutions values container.
 *
 * Constructor takes a substitution values container. The entries of this container
 * have different structure, depending on the type of substitution rules they apply to:
 * 1) For substitution rules of type "ReplaceToken", the container has following structure:
 *    {subst rule name} = {substitution value}
 * 2) For substitution rules of type "Repeat", the container has following structure:
 *    {subst rule name}.{iteration index}.{nested subst rule name} = {substitution value}         999
 *                                                                                                
 * For example:
 * A) Following are three template lines: A table row with two cells:
 *        &lt;td substrule=Heading substtype=ReplaceToken substpattern=XXX&gt;XXX&lt;/td&gt;
 *        &lt;row substrule="RepeatLabor" substtype=Repeat endTag='row'&gt;
 *        &lt;td class=xl30 x:num substrule=abc substtype=ReplaceToken substpattern=999&gt;999&lt;/td&gt;
 *        &lt;td class=xl30 x:num substrule=def substtype=ReplaceToken substpattern=999&gt;999&lt;/td&gt;
 *        &lt;td class=xl30 x:num substrule=ghi substtype=ReplaceToken substpattern=999 substdefault=XYZ&gt;999&lt;/td&gt;
 *        &lt;/row&gt;"
 *
 * B) Following is the configuration container to produce two rows of cells:
 *        "Heading" = "Summary Report"
 *        "RepeatLabor.0.abc" = "aaa"
 *        "RepeatLabor.0.def" = "ddd"
 *        "RepeatLabor.1.abc" = "bbbbbb"
 *        "RepeatLabor.1.def" = "eeeeee"
 *
 * C) This class will then produce the following, after substituting the cell
 *    values to the template:
 *        &lt;td substrule=Heading substtype=ReplaceToken substpattern=XXX&gt;Summary Report&lt;/td&gt;
 *        &lt;row substrule="RepeatLabor" substtype=Repeat endTag='row'&gt;
 *        &lt;td class=xl30 x:num substrule=abc substtype=ReplaceToken substpattern=999&gt;aaa&lt;/td&gt;
 *        &lt;td class=xl30 x:num substrule=def substtype=ReplaceToken substpattern=999&gt;ddd&lt;/td&gt;
 *        &lt;td class=xl30 x:num substrule=ghi substtype=ReplaceToken substpattern=999 substdefault=XYZ&gt;XYZ&lt;/td&gt;
 *        &lt;/row&gt;"
 *        &lt;row substrule="RepeatLabor" substtype=Repeat endTag='row'&gt;
 *        &lt;td class=xl30 x:num substrule=abc substtype=ReplaceToken substpattern=999&gt;bbbbbb&lt;/td&gt;
 *        &lt;td class=xl30 x:num substrule=def substtype=ReplaceToken substpattern=999&gt;eeeeee&lt;/td&gt;
 *        &lt;td class=xl30 x:num substrule=ghi substtype=ReplaceToken substpattern=999 substdefault=XYZ&gt;XYZ&lt;/td&gt;
 *        &lt;/row&gt;"
 *
 * Restrictions:
 * 1) The template line that specifies a "Repeat" type substitution rule,
 *    cannot have any other substitution rules. 
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 2008-06-16
 *     Creation
 * 2.  GHA, 2008-07-01
 *     when matching end tag ('Repeat') substitution rule type, it does not
 *     matter what case the end tag is.
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/XLSGenerator.java#2 $

 */
public class XLSGenerator
{
    private static final String REPLACE_TOKEN_RULE = "ReplaceToken";
    private static final String REPEAT_RULE = "Repeat";
    private static final String SUBST_RULE_ATTRIB = "substrule";
    private static final String SUBST_TYPE_ATTRIB = "substtype";
    private static final String SUBST_PATTERN_ATTRIB = "substpattern";
    private static final String SUBST_DEFAULT = "substdefault";
    
    
    private IContainer substValues;
    private XLSTemplate xlsTemplate;
    
    
    private XLSGenerator(IContainer substValues, XLSTemplate xlsTemplate)
    {
        this.substValues = substValues;
        this.xlsTemplate = xlsTemplate;
    }
    
    
    /**
     * @param substValues XLS substitution values
     * @param template XLS template
     * @return XLSGenerator instance
     */
    public static XLSGenerator createFromTemplate(IContainer substValues, XLSTemplate template)
    {
        return new XLSGenerator(substValues, template);
    }
    
    
    /**
     * @return List of generated XLS XML lines, based on template 
     *         and the substitution configuration container
     */
    public List generate()
    {
        List outputLines = new ArrayList();
        List templateLines = xlsTemplate.lines();
        IContainer values = substValues;
        generate(templateLines, outputLines, values);
        return outputLines;
    }
    
    
    /**
     * Add lines in the {outputines} list, based on template lines, substitution values,
     * and the substitution rules container
     * @param templateLines template XLS XML lines
     * @param outputLines List of output XLS XML lines, based on template lines, and substitution values
     * @param substValues values to substitute
     */
    private void generate(List templateLines, List outputLines, IContainer substValues)
    {
        int tmplLineIdx = 0;
        while (tmplLineIdx < templateLines.size())
        {
            String line = (String)templateLines.get(tmplLineIdx);
            StringBuilder strBuilder = new StringBuilder(line);
            
            try
            {
                String substRule = readAttrib(line, SUBST_RULE_ATTRIB);
                if (substRule == null || substValues == null)
                {
                    outputLines.add(strBuilder.toString());
                    tmplLineIdx++;
                }
                else
                {
                    String substRuleType = readAttrib(line, SUBST_TYPE_ATTRIB);
                    if (REPLACE_TOKEN_RULE.equals(substRuleType))
                    {
                        String substPattern = readAttrib(line, SUBST_PATTERN_ATTRIB);
                        if (StringUtil.isNullOrEmpty(substPattern))
                        {
                            String msg = "No value for " + REPLACE_TOKEN_RULE 
                                + "-type substitution rule: " + substRule
                                + " attribute " + SUBST_PATTERN_ATTRIB;
                            throw new IllegalStateException(msg);
                        }
                        Object substObject;
                        
                        if (!substValues.containsKey(substRule))
                        {
                            substObject = readAttrib(line, SUBST_DEFAULT);
                            if (null == substObject)
                            {
                                String msg = "No substitution value found for substitution rule: " 
                                    + substRule + ", and no substitution default defined";
                                throw new IllegalStateException(msg);
                            }
                        }
                        else
                        {
                            substObject = substValues.getElement(substRule);
                        }
                        replaceToken(substPattern, strBuilder, substObject); 
                        outputLines.add(strBuilder.toString());
                        tmplLineIdx++;
                    }
                    else if (REPEAT_RULE.equals(substRuleType))
                    {
                        String endTag = readAttrib(line, "endTag");
                        List repeatedTemplateLines = extractRepeatedLineRange(templateLines, tmplLineIdx, endTag);
                        if (substValues != null && substValues.containsKey(substRule))
                        {
                            IContainer repeatRuleValues = (IContainer)substValues.getElement(substRule);
                            repeat(
                                endTag, 
                                repeatedTemplateLines, 
                                outputLines,
                                repeatRuleValues
                            );
                        }
                        else
                        {
                            // No values for repeated lines. Skip these lines.
                        }
                    }
                    else
                    {
                        String msg = "Unknown substitution rule type: " + substRuleType;
                        throw new IllegalStateException(msg);
                    }
                }
            }
            catch (Exception e)
            {
                String msg = "Problem handling following template line: " + line;
                throw new RuntimeException(msg, e);
            }
        }
    }
    
    
    /**
     * @param inputLines list of template lines - from which lines will be removed!
     * @param startIdx starting index (inclusive) of range of lines to be removed from {inputLines}
     * @param endTag XML end-tag that marks the last line in range of lines
     * @return List of lines that were removed from inputLines, starting at line at index {startIdx}
     *         and the last line that contains the XML end-tag specified in {endTag}
     */
    List extractRepeatedLineRange(List inputLines, int startIdx, String endTag)
    {
        List extractedList = new ArrayList();
        boolean lastRepeatLineReached;
        String endTagStrToMatch = "</" + endTag.toUpperCase() + ">";
        do
        {
            String repeatedLine = (String)inputLines.get(startIdx);
            lastRepeatLineReached = repeatedLine.toUpperCase().indexOf(endTagStrToMatch) > -1;
            if (startIdx == inputLines.size()-1)
            {
                // end of inputLines reached.
                // last line better be the last repeat line
                if (!lastRepeatLineReached)
                {
                    String msg = "Could not find end-tag for '" 
                        + REPEAT_RULE + "' substitution rule";
                    throw new IllegalStateException(msg);
                }
            }
            
            extractedList.add(repeatedLine);
            inputLines.remove(startIdx);
        } while (lastRepeatLineReached == false);
        
        return extractedList;
    }

    
    /**
     * @param tokenToReplace String pattern to be replaced by {substObject}
     * @param strBuilder template line
     * @param substObject value to substitute into template {strBuilder} template line
     */
    private void replaceToken(
        String tokenToReplace, 
        StringBuilder strBuilder, 
        Object substObject)
    {
        final int tokenLen = tokenToReplace.length();
        final String substString = StringUtil.toString(substObject);
        final int substStringLen = substString.length();
        
        boolean performedReplace = false;
        int tokenOffset = 0;
        int tokenIdx;
        do 
        {
            tokenIdx = strBuilder.indexOf(tokenToReplace, tokenOffset);
            
            if (tokenIdx > -1)
            {
                int newTokenOffset = skipSubstAttribs(strBuilder, tokenToReplace, tokenIdx, tokenOffset);
                if (newTokenOffset != tokenOffset)
                {
                    tokenOffset = newTokenOffset;
                }
                else
                {
                    strBuilder.replace(tokenIdx, tokenIdx+tokenLen, substString);
                    performedReplace = true;
                    tokenOffset = tokenIdx + substStringLen;
                }
            }
        }
        while (tokenIdx > -1);
        
        if (!performedReplace)
        {
            String msg = "Could not find substitution pattern " + tokenToReplace + " to substitute";
            throw new IllegalStateException(msg);
        }
    }
    
    
    /**
     * Returns new offset value if specified {tokenIdx} matches the {tokenToReplace}
     * that is within one of the tag attribute values: substpattern, substrule, substtyp.
     * If tokenIdx does not match one of these attributes' values, returns original 
     * {tokenOffset} value.
     * @param template line of text based on template
     * @param tokenToReplace
     * @param tokenIdx current index within {line} of occurrence of {tokenToReplace}
     * @param tokenOffset current index offset for token search within line
     * @return int with new token offset if tokenIdx within the attribute tag's value
     *         else returns original (input) tokenOffset value
     */
    private int skipSubstAttribs(
        CharSequence line, 
        String tokenToReplace, 
        int tokenIdx, 
        int tokenOffset)
    {
        int newTokenOffset;
        
        newTokenOffset = skipSubstAttrib(line, SUBST_PATTERN_ATTRIB, tokenToReplace, tokenIdx, tokenOffset);
        if (newTokenOffset != tokenOffset)
            return newTokenOffset;
        
        newTokenOffset = skipSubstAttrib(line, SUBST_RULE_ATTRIB, tokenToReplace, tokenIdx, tokenOffset);
        if (newTokenOffset != tokenOffset)
            return newTokenOffset;
        
        newTokenOffset = skipSubstAttrib(line, SUBST_TYPE_ATTRIB, tokenToReplace, tokenIdx, tokenOffset);
        if (newTokenOffset != tokenOffset)
            return newTokenOffset;
        
        newTokenOffset = skipSubstAttrib(line, SUBST_DEFAULT, tokenToReplace, tokenIdx, tokenOffset);
        if (newTokenOffset != tokenOffset)
            return newTokenOffset;
        
        // No substitution attributes to skip - return original tokenOffset:
        return tokenOffset;
    }
    
    
    /**
     * @param template line of text based on template
     * @param attribute XML tag attribute to skip over
     * @param tokenToReplace
     * @param tokenIdx current index within {line} of occurrence of {tokenToReplace}
     * @param tokenOffset current index offset for token search within line
     * @return int with new token offset if tokenIdx within the attribute tag's value
     *         else returns original (input) tokenOffset value
     */
    private int skipSubstAttrib(
        CharSequence line, 
        String attribute, 
        String tokenToReplace, 
        int tokenIdx, 
        int tokenOffset)
    {
        int newOffset = tokenOffset;
        String escapedTokenToReplace = StringUtil.escapeRegexMetaChars(tokenToReplace);
        Pattern regexPattern = Pattern.compile(attribute + "=('|\")?" + escapedTokenToReplace);
        Matcher regexMatcher = regexPattern.matcher(line);
        if (regexMatcher.find())
        {
            int attribStartIdx = regexMatcher.start();
            int attribEndIdx = regexMatcher.end();
            if (attribStartIdx < tokenIdx && tokenIdx < attribEndIdx)
            {
                // We matched the attrib value itself.
                // skip it, by adjusting the offset:
                newOffset = attribEndIdx;
            }
        }
        
        return newOffset;
    }
    
    
    /**
     * Apply a number of iterations to a list of template lines.
     * The number of iterations is determined by the number of elements in
     * the {repeatRuleValues} container
     * @param endTag XML end-tag that marks the end of the repeated sequence of template lines
     * @param repeatedTemplateLines list of template lines to be repeated
     * @param outputList List of output lines
     * @param repeatRuleValues value container to apply.
     */
    void repeat(
        String endTag, 
        List repeatedTemplateLines, 
        List outputLines,
        IContainer repeatRuleValues)
    {
        String endTagToMatch = "</" + endTag + ">";
        int repeatedTmplLineIdx = 0;
        int numRepeatedLines = repeatedTemplateLines.size();

        String[] keys = ContUtil.keys(repeatRuleValues);
        for (int iterIdx=0; iterIdx<keys.length; iterIdx++)
        {
            IContainer iterationSubstValues = (IContainer)repeatRuleValues.getElement(keys[iterIdx]);
        
            // First take care of first "repeated line".
            // Do this separately from the rest of the lines, because we don't
            // want to pass this to the generate method - that will recurse
            // and call this repeat method again.
            // Note: No substitutions supported on the line that specifies the Repeat rule
            String firstRepeatedLine = (String)repeatedTemplateLines.get(0);
            outputLines.add(firstRepeatedLine);
            
            if (numRepeatedLines > 1)
            {
                List bodyOfRepeatedLines = new ArrayList();
                
                for (int bodyIdx = 1; bodyIdx < numRepeatedLines-1; bodyIdx++)
                {
                    bodyOfRepeatedLines.add(repeatedTemplateLines.get(bodyIdx));
                }
                generate(bodyOfRepeatedLines, outputLines, iterationSubstValues);
                
                String lastRepeatedLine = (String)repeatedTemplateLines.get(numRepeatedLines-1);
                outputLines.add(lastRepeatedLine);
            }
        }
    }
    
    
    /**
     * Returns whether character is a "border character".
     * The end of an attribute value is followed by a border character (e.g. '"')
     * @param aChar
     * @return true if {aChar} is a "border character": '\', '"', ' ' or '>'
     */
    private boolean isBorderChar(char aChar)
    {
        return aChar == '\'' || aChar == '"' || aChar == ' ' || aChar == '>';
    }
    
    
    /**
     * Returns value of attribute in a line of text.
     * An attribute is preceded by a white space, followed by '=' character,
     * followed by a value that is optionally enclosed within quotes.
     * @param line
     * @param attribName
     * @return value of attrib, or null if attrib not within line
     */
    private String readAttrib(String line, String attribName)
    {
        String attribNamePlusEquals = attribName + '=';
        int attribIdx = line.indexOf(attribNamePlusEquals);
        if (attribIdx == -1)
            return null;
        
        StringBuilder attribValue = new StringBuilder();
        int probeIdx = attribIdx + attribNamePlusEquals.length();
        char nextChar = line.charAt(probeIdx);
        if (nextChar == '\'' || nextChar == '"')
        {
            probeIdx++;
        }
        
        nextChar = line.charAt(probeIdx);
        while (!isBorderChar(nextChar))
        {
            attribValue.append(nextChar);
            probeIdx++;
            nextChar = line.charAt(probeIdx);
        }
        
        return attribValue.toString();
    }
    
    
    public static void main(String[] args)
    {
        final String TEMPLATE_FILE_PATH = "c:\\temp\\test.xls";
        XLSTemplate template = XLSTemplate.createFromFile(TEMPLATE_FILE_PATH);
        IContainer substValues = new GenericContainer();
        substValues.setElement("sandratable.0.date", "08/06/12");
        substValues.setElement("sandratable.0.w", "120");
        substValues.setElement("sandratable.0.status", "progress");
        substValues.setElement("sandratable.1.date", "08/06/13");
        substValues.setElement("sandratable.1.w", "119");
        substValues.setElement("sandratable.1.status", "progress");
        substValues.setElement("sandratable.2.date", "08/06/17");
        substValues.setElement("sandratable.2.w", "125");
        substValues.setElement("sandratable.2.status", "bad");
        substValues.setElement("sandratable.3.date", "08/06/19");
        substValues.setElement("sandratable.3.w", "110");
        substValues.setElement("sandratable.3.status", "goooooody");
        XLSGenerator generator = XLSGenerator.createFromTemplate(substValues, template);
        List outputList = generator.generate();
        for (int i=0; i<outputList.size(); i++)
        {
            System.out.println(outputList.get(i));
        }
    }
}
