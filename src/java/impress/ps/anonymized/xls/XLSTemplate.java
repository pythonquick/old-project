package impress.ps.anonomized.xls;


import java.util.List;
import java.util.ArrayList;
import java.io.*;

/**
 * XLS template contains a sequence of XLS XML text lines.
 * These template lines can contain substitution rules. A substitution rule
 * is defined in a XML tag's "subst" attribute.
 * 
 * Following is an example of a couple of lines from a template:
 *
 *        &lt;row subst="RepeatLabor" endTag='row'&gt;
 *        &lt;td class=xl30 x:num subst=abc>999&lt;/td&gt;
 *        &lt;td class=xl30 x:num subst=def>999&lt;/td&gt;
 *        &lt;/row&gt;"
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 2008-06-16
 *     Creation
 * -----------------------------------------------------------------------------
 */
public class XLSTemplate
{
    private List lines;
    
    
    private XLSTemplate(List lines)
    {
        this.lines = lines;
    }
    
    
    /**
     * @param list List of XLS XML text lines for template
     * @return XLSTemplate
     */
    public static XLSTemplate createFromList(List lines)
    {
        List deWrapped = deWrap(lines);
        return new XLSTemplate(deWrapped);
    }
    
    
    /**
     * @param pathToFile absolute file path to tmplate file
     * @return XLSTemplate
     */
    public static XLSTemplate createFromFile(String pathToFile)
    {
        List lines = linesFromFile(pathToFile);
        return createFromList(lines);
    }
    
    
    /**
     * @return number of lines in template
     */
    public int size()
    {
        return lines.size();
    }
    
    
    /**
     * @param idx position for line (first line at idx 0)
     * @return String (line) of template text at specified {idx} line
     */
    public String lineAt(int idx)
    {
        return (String)lines.get(idx);
    }
    
    
    /**
     * @param pathToFile absolute file path to tmplate file
     * @return List of lines (String instances) from the template file
     */
    private static List linesFromFile(String pathToFile)
    {
        List lines = new ArrayList();
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(
                new InputStreamReader(
                    new FileInputStream(pathToFile)
                )
            );
            
            String line;
            while ((line = reader.readLine()) != null)
            {
                lines.add(line);
            }
        }
        catch (IOException e)
        {
            String msg = "Could not read file from " + pathToFile;
            throw new RuntimeException(msg, e);
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e) { /* what to do - ignore it */ }
            }
        }
        
        return lines;
    }
    
   
    /**
     * Unwraps lines where the start and end tags are not on the same line.
     * @param originalLines list of original XLS XML template lines
     * @return List of lines, based on {originalLines} list, but appends
     *         lines together into single lines until the line starts with &lt;
     *         and ends with &gt;
     */ 
    private static List deWrap(List originalLines)
    {
        List deWrappedList = new ArrayList();
        int numOrigLines = originalLines.size();
        if (numOrigLines == 0)
        {
            return deWrappedList;
        }
        
        int origLineIdx = numOrigLines -1;
        
        StringBuffer deWrappedLines = new StringBuffer();
        
        while (origLineIdx >= 0)
        {
            String origLine = ((String)originalLines.get(origLineIdx)).trim();
            if (deWrappedLines.length() > 0)
            {
                deWrappedLines.insert(0, " ");
            }
            deWrappedLines.insert(0, origLine);
            if (deWrappedLines.length() > 0)
            {
                char firstChar = deWrappedLines.charAt(0);
                char lastChar = deWrappedLines.charAt(deWrappedLines.length()-1);
                if (firstChar == '<' && lastChar == '>')
                {
                    deWrappedList.add(0, deWrappedLines.toString());
                    deWrappedLines.delete(0, deWrappedLines.length());
                }
            }
            --origLineIdx;
        }
        
        if (deWrappedLines.length() > 0)
        {
            // Add remaining lines at end:
            deWrappedList.add(0, deWrappedLines.toString());
        }
        
        return deWrappedList;
    }

    
    /**
     * @return copy of XLS template's list of lines
     */
    public List lines()
    {
        return new ArrayList(lines);
    }
    
    
    public static void main(String[] args)
    {
        String pathToFile = args[0];
        List lines = linesFromFile(pathToFile);
        List dewrapped = deWrap(lines);
        for (int i=0; i<dewrapped.size(); i++)
        {
            System.out.println(dewrapped.get(i));
        }
    }
}
