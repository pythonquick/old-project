package impress.ps.anonomized;


import impress.ps.concurrency.Lock;
import impress.ps.err.domain.General;
import impress.ps.anonomized.WorkspaceGroupWriteAccessLock;
import impress.ps.util.ContUtil;
import impress.ps.util.RFCUtil;
import impress.ps.util.StringUtil;
import impress.ois.base.rep.ConfigFileProperties;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.dtrans.GenericContainer;
import impress.ois.base.error.OISException;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.map.imap.rt.BR;
import impress.ois.base.map.imap.rt.XA;
import impress.ois.base.map.imap.AbstractIMap;
import java.io.PrintWriter;


/**
 * Utility methods specific to the Anonomized project
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 30-Jun-2007
 *     Creation
 * 2.  GHA, 05-Jun-2008
 *     New methods genWbsActCodesCnt, lowestWBSElements and loadWBSElements
 * 3.  GHA, 25-Jun-2008
 *     New methods: readCostAccount, setProjectNote
 * 4.  GHA, 15-Sep-2008
 *     New method saveImpressLogsToNotebookTopic
 *     New method addWorkspaceToActiveGroup
 * 5.  GHA, 30-Oct-2008
 *     Removed unused BAPI_ constants (moved to class ProjDefStatusSetter).
 *     ContUtil.createIfNull replaced by ContUtil.getOrCreateCont.
 *     IMapCallFailed constructor now called with BR instance.
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/ProjUtil.java#8 $
 */
public final class ProjUtil 
{
    public static boolean isOnImpressSys()
    {
        // Read some properties from the ois.properties file:
        ConfigFileProperties cfp = new ConfigFileProperties();
        IContainer oisProp = new GenericContainer();
        cfp.loadProperties(oisProp, "ois.properties");
        String reposURL = (String)oisProp.getElement("Repository.IE.Open.URL");
        
        // We are on Impress internal system: repository URL contains PS92U8:
        // or Guenther runs ImpressServer from his laptop
        boolean isOnGLaptop = "Guenther".equals(oisProp.getElement("Laptop"));
        return reposURL.toUpperCase().endsWith("PS92U8") || isOnGLaptop;
    }
    
    
    /**
     * Remove specified workspace from specified workspace group container.
     * @param wsName name of workspace 
     * @param wsGroupCnt workspace group container
     */
    public static void removeWSFromGroup(String wsName, IContainer wsGroupCnt)
    {
        IContainer workspacesCnt = (IContainer)wsGroupCnt.getElement("Workspace");
        String[] keys = ContUtil.keys(workspacesCnt);
        for (int i=0; i<keys.length; i++)
        {
            IContainer oneWorkspaceRow = (IContainer)workspacesCnt.getElement(keys[i]);
            if (wsName.equals(oneWorkspaceRow.getElement("Name")))
            {
                workspacesCnt.removeElement(keys[i]);
            }
        }
    }
    
    
    /**
     * Create new, empty workspace group
     * @param ieSys magical "ie" IESystem instance
     * @param wsGroupName name of workspace group, e.g. "MyGroup"
     * @param queueName name of I4EPM queue e.g. "custPrjQueDefault1"
     * @param custPPCntName container name of application module container
     *        e.g. "custPrjPPInstalledSAPPSP3e"
     * @return IContainer the workspace group container
     */
    private static IContainer createEmptyWSGroup(
        IESystem ieSys,
        String wsGroupName, 
        String queueName, 
        String custPPCntName
    )
    {
        IContainer wsGroupCnt = new GenericContainer();
        wsGroupCnt.setElement("Name", wsGroupName);
        wsGroupCnt.setElement("Comment", null);
        wsGroupCnt.setElement("EMailAddress", null);
        wsGroupCnt.setElement("DefaultQueue", queueName);
        wsGroupCnt.setElement("CreateUser", ieSys.getSession().getUserName());
        wsGroupCnt.setElement("UpdateUser", null);
        wsGroupCnt.setElement("CreateDate", null);
        wsGroupCnt.setElement("UpdateDate", null);
        wsGroupCnt.setElement("WebEnabled", Boolean.TRUE);
        wsGroupCnt.setElement("ProcessCallPackContainer", custPPCntName);
        wsGroupCnt.setElement("Workspace", new GenericContainer());
        
        BR br = ieSys.newCall("iePrjEngCreateWorkspaceGroup");
        
        br.setElement("In.WorkspaceGroup", wsGroupCnt);
        br.process();
        if (!br.wasSuccessful())
        {
            throw new General.IMapCallFailed("iePrjEngSaveContainer", br);
        }
        
        return wsGroupCnt;
    }
    
    
    /**
     * Add specified workspace name to specified workspace group.
     * @param ieSys magical "ie" IESystem instance
     * @param wsGroupName name of workspace group e.g. "MyGroup"
     * @param wsName name of workspace e.g. "X-GH-FE4"
     * @param queueName name of I4EPM queue e.g. "custPrjQueDefault1"
     * @param applModule application module name e.g. "SAPPSP3e"
     */
    public static void addToWorkspaceGroup(
        IESystem ieSys,
        String wsGroupName, 
        String wsName,
        String queueName,
        String applModule)
    {
        Lock wsGrouplock = WorkspaceGroupWriteAccessLock.getInstance();
        try
        {
            if (wsGrouplock.tryLock(ProjConst.LOCK_ACQUIREMENT_TIMEOUT))
            {
                IContainer wsGroup;
                String wsGroupCntName = "custPrjWg" + wsGroupName;
                try 
                {
                    wsGroup = ContUtil.loadContainer(ieSys, wsGroupCntName);
                }
                catch (ContUtil.NotFoundException e)
                {
                    // then create the workspace group
                    String custPPCntName = "custPrjPPInstalled" + applModule;
                    wsGroup = createEmptyWSGroup(ieSys, wsGroupName, queueName, custPPCntName);
                }
                
                // First any existing entries for the workspace name from the
                // "Workspace" container of the workspace group
                ProjUtil.removeWSFromGroup(wsName, wsGroup);
                
                // Add a row to the "Workspace" container of the workspace group:
                IContainer workspaces = ContUtil.getOrCreateCont(wsGroup, "Workspace");
                IContainer wsRow = new GenericContainer();
                wsRow.setElement("Name", wsName);
                ContUtil.addRow(workspaces, wsRow);

                // Save Workspace group container to repository:
                ContUtil.saveContainer(ieSys, wsGroup, wsGroupCntName);
            }
            else
            {
                String msg = "Could not acquire lock for workspace group write access."
                    + " Timed out after waiting " + ProjConst.LOCK_ACQUIREMENT_TIMEOUT + "ms";
                throw new RuntimeException(msg);
            }
        }
        catch (InterruptedException e)
        {
            String msg = "Interrupted while acquiring lock for workspace group write access";
            throw new RuntimeException(msg);
        }
        finally
        {
            // Very important! Release the lock
            if (wsGrouplock.isHeldByCurrentThread())
                wsGrouplock.unlock();
        }
    }
    
    
    /**
     * Write a single row using PrintWriter of specified objects, 
     * separated by TABs
     * @param pw PrintWriter instance
     * @param values Object[], for line, to be separated by tabs
     * @param numCols number of columns
     * @throws IllegalArgumentException if size of 'values' not equal to numCols
     */
    public static void printTabSeparatedLine(PrintWriter pw, Object[] fields, int numCols)
    {
        if (fields.length != numCols)
        {
            String msg = fields.length + " fields in line, expected " + numCols;
            throw new IllegalArgumentException(msg);
        }
        
        for (int i=0; i<fields.length; i++)
        {
            if (fields[i] == null)
                throw new IllegalStateException("empty field in position " + i);
            pw.print(fields[i]);
            boolean isLastField = i == fields.length-1;
            if (!isLastField)
                pw.print("\t");
        }
        pw.println();
    }
    
    
    /**
     * @param messageString message string, typically for update log
     * @param isSimulation true if it is a simulation run
     * @return messageString with "Simulation: " prepended if isSimulationRun
     */
    public static String msg(String messageString, boolean isSimulation)
    {
        if (isSimulation)
            return "Simulation: " + messageString;
        else
            return messageString;
    }
    
    
    /**
     * @param subject Email subject line
     * @param instanceId ImpressServer instance ID
     * @return subject plus String identifying instance:
     *         subject + " - Development" (instanceId 3)
     *         subject + " - Test" (instanceId 4)
     *         subject + " - QA" (instanceId 5)
     *         subject + " - Production (instanceId 6)
     */
    public static String subject(String subject, Long instanceId)
    {
        if (ProjConst.DEV_INST.equals(instanceId))
            return subject + " - Development";
        else if (ProjConst.TEST_INST.equals(instanceId))
            return subject + " - Test";
        else if (ProjConst.QA_INST.equals(instanceId))
            return subject + " - QA";
        else if (ProjConst.PROD_INST.equals(instanceId))
            return subject + " - Production";
        else
            return subject + " - Unknown instance: " + instanceId;
    }
    
    
    
    
    /**
     * @param sapBE name of SAP backend
     * @param projDef PS project definition
     * @return IContainer with WBS Elements belonging to specified project
     */
    public static IContainer loadWBSElements(IESystem ie, String sapBE, String projDef)
    {
        IContainer params = new GenericContainer();
        params.setElement("I_PROJECT_DEFINITION", projDef);
        BR br = RFCUtil.callRFC(ie, sapBE, "BAPI_BUS2054_GETDATA", params, "ET_RETURN", null);
        
        return (IContainer)br.getElement("Out.ET_WBS_ELEMENT");
    }
    
    
    
    
    /**
     * @param wbsElements IContainer with WBS elements
     * @return IContainer with WBS Elements on lowest level
     */
    private static IContainer lowestWBSElements(IContainer wbsElements)
    {
        IContainer lowestWBS = new GenericContainer();
        
        String[] keys = ContUtil.keys(wbsElements);
        for (int i=0; i<keys.length; i++)
        {
            IContainer oneWBS = (IContainer)wbsElements.getElement(keys[i]);
            if (StringUtil.isNullOrEmpty((String)oneWBS.getElement("WBS_DOWN")))
                ContUtil.addRow(lowestWBS, oneWBS);
        }
        return lowestWBS;
    }
    
    
    /**
     * Generate WBS activity code values (from lowest level WBSes, of certain 
     * proj types). Returns these as container. To be created in Save sequence.
     * @param wbsActCodeName name of Activity code to create
     * @param projTypesToMatch String[] of project-types of WBS to use for act code
     * @param lowestWBSElements WBS elements on lowest level (WBS_DOWN empty)
     *        (ET_WBS_ELEMENT IContainer from BAPI_BUS2054_GETDATA)
     * @return IContainer that describes the ActCode (and optionally values)
     *         to be created. (See global type impPsPrjP3eCodesToCreate)
     */
    private static IContainer genWBSActCodesCnt(
        String wbsActCodeName, 
        String[] projTypesToMatch, 
        IContainer lowestWBSElements
    )
    {
        // Collect activity code value/descriptions into container
        // <i>."Value", <i>."Descr":
        IContainer actCodesCnt = new GenericContainer();
        
        String[] keys = ContUtil.keys(lowestWBSElements);
        for (int i=0; i<keys.length; i++)
        {
            IContainer oneWBS = (IContainer)lowestWBSElements.getElement(keys[i]);
            
            String projType = (String)oneWBS.getElement("PROJ_TYPE");
            if (StringUtil.isOneOf(projType, projTypesToMatch))
            {
                IContainer row = new GenericContainer();
                row.setElement("Value", oneWBS.getElement("WBS_ELEMENT")); // must be String
                row.setElement("Descr", oneWBS.getElement("DESCRIPTION")); // must be String
                ContUtil.addRow(actCodesCnt, row);
            }
        }
        
        IContainer projActCodeInfoCnt = new GenericContainer();
        projActCodeInfoCnt.setElement("Name", wbsActCodeName);
        projActCodeInfoCnt.setElement("Values", actCodesCnt);
        
        return projActCodeInfoCnt;
    }
    
    
    /**
     * @param wbsActCodeName name of Activity Code, for WBS ID values
     * @param separatedProjType ";" separated string of project-types of WBS to use for act code
     * @param wbsElements IContainer with each row a WbsElement container
     * @return IContainer that describes the ActCode (and optionally values)
     *         to be created. (See global type impPsPrjP3eCodesToCreate)
     */
    public static IContainer genWbsActCodesCnt(
        String wbsActCodeName, 
        String separatedProjTypes, 
        IContainer wbsElements
    )
    {
        IContainer lowestWBSElements = lowestWBSElements(wbsElements);
        String[] projTypes = StringUtil.split(separatedProjTypes, ";");

        IContainer wbsActCodeCnt = genWBSActCodesCnt(
            wbsActCodeName,
            projTypes,
            lowestWBSElements
        );
        
        return wbsActCodeCnt;
    }
    
    
    /**
     * @param raInfoRow PassThrough.("Labor"|"Nonlabor").{row} container in BF mapping rules
     * @return Cost Account, with leading zeroes removed
     */
    public static String readCostAccount(IContainer raInfoRow)
    {
        String costAccount = (String)raInfoRow.getElement("LSTAR_RFC.KSTAR");
        return StringUtil.removeLeading(costAccount, '0');
    }
    
    
    /**
     * Sets a Project Note to specified 'projNoteCnt' container
     * @param projNoteCont project note container - will add row to this container
     * @param projId Primavera project ID (e.g. "ME-00001")
     * @param nbTopicName name of Notebook Topic
     * @param text raw text String content
     */    
    public static void setProjectNote(
        IContainer projNoteCont,
        String projId,
        String nbTopicName,
        String text)
    {
        IContainer projNoteRow = new GenericContainer();
        
        if (text == null)
            throw new IllegalArgumentException("null text");
        
        projNoteRow.setElement("Note", text);
        projNoteRow.setElement("NotebookTopicName", nbTopicName);
        projNoteRow.setElement("ProjectId", projId);
        
        ContUtil.addRow(projNoteCont, projNoteRow);
    }
    
    
    /**
     * Adds specified workspace to the Active workspace group.
     * @param ieSys the "ie" magical IESystem instance
     * @param wsName name of the workspace (note: not the full container name)
     */
    public static void addWorkspaceToActiveGroup(IESystem ieSys, String wsName)
    {
        addToWorkspaceGroup(
            ieSys,
            ProjConst.WS_GROUP_ACTIVE, 
            wsName, 
            ProjConst.WS_QUEUE, 
            ProjConst.SAPPSP3E_APPL_MODULE
        );
    }
    
    
    /**
     * Call I.Map to set Impress Log Notebook Topic, to include Impress update log
     * messages into certain Primavera Notebook Topics (applies only to certain
     * mapping rules.
     * @param iesys the magical IESystem reference
     * @param inCont I.Map In Container
     * @throws OISException if I.Map call was not successful
     */
    public static void saveImpressLogsToNotebookTopic(IESystem iesys, IContainer inCont)
    {
        BR br = iesys.newCall("impPsPrjAnySetImpressLogNotebookTopic");
        br.setElement("In", inCont);
        br.process();
    }
}
