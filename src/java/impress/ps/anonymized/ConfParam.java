package impress.ps.anonomized;

/**
 * Class for configuration file property names, accessed in various I.Maps.
 * (Configuration file, for example "psp3e_impress.properties")
 *
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 17-Jul-2007
 *     Creation
 * 2.  GHA, 11-Jun-2008
 *     New constant ACTUALS_PERIOD_DATE_UDF_NAME
 * 3.  GHA, 24-Jun-2008
 *     New constant SAP_WBS_STATUS_UDF
 * 4.  GHA, 30-Jun-2008
 *     New constants PROJ_IMPLOG_UPL_FCST_NOTEBOOK_TOPIC, 
 *     PROJ_IMPLOG_DWNL_ACT_NOTEBOOK_TOPIC, PROJ_IMPLOG_UPL_BUDG_NOTEBOOK_TOPIC
 * 5.  GHA, 21-Jul-2008
 *     Constant BUDGET_VERSION renamed to BUDGET_UPLOAD_VERSION
 *     New constant: BUDGET_DOWNLOAD_VERSION
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/ConfParam.java#7 $
 *
 */
public final class ConfParam
{
    public static final String 
        // Properties for project creation log files:
        PCREATE_ERROR_LOG = "ProjCreationErrorLogFile",
        PCREATE_WARNING_LOG = "ProjCreationWarningLogFile",
        PCREATE_SUCCESS_LOG = "ProjCreationSuccessLogFile",
        LEV2_WBS_PROJTYPE = "Lev2WbsProjType",
        
        // Properties for code/udf names:
        COMPCODE_PROJCODE_NAME = "ProjCode.SAPCompCode.Name",
        COMPCODE_PROJCODE_DESCR = "ProjCode.SAPCompCode.Descr",
        TEMPLATE_ID_PROJCODE_NAME = "ProjCode.TemplateID.Name",
        TEMPLATE_ID_PROJCODE_DESCR = "ProjCode.TemplateID.Descr",
        INT_CALC_PROF_PROJCODE_NAME = "ProjCode.IntCalcProf.Name",
        INT_CALC_PROF_PROJCODE_DESCR = "ProjCode.IntCalcProf.Descr",
        INV_REASON_PROJCODE_NAME = "ProjCode.InvReason.Name",
        INV_REASON_PROJCODE_DESCR = "ProjCode.InvReason.Descr",
        BUDGET_STATUS_CODE_NAME = "ProjCode.BudgetStatus.Name",
        FORECAST_STATUS_CODE_NAME = "ProjCode.ForecastStatus.Name",
        ACTUALS_STATUS_CODE_NAME = "ProjCode.ActualsStatus.Name",
        
        SPONSOR_NEED_DATE_UDF_NAME = "ProjUDF.SponsorNeedDate.Name",
        ACTUALS_TRANFER_DATE_UDF_NAME = "ProjUDF.ActualsTransferDate.Name",
        BUDGET_TRANFER_DATE_UDF_NAME = "ProjUDF.BudgetTransferDate.Name",
        FORECAST_TRANFER_DATE_UDF_NAME = "ProjUDF.ForecastTransferDate.Name",
        PROJDEF_LONGTEXT_UDF_NAME = "ProjUDF.ProjLongText.Name",
        SAP_SYS_STATUS_UDF_NAME = "ProjUDF.SAPSysStatus.Name",
        LAST_SYNC_RUN_DATE_NAME ="ProjUDF.LastSyncRunDate.Name",
        ACTUALS_PERIOD_DATE_UDF_NAME = "ProjUDF.ActualsPeriodDate.Name",
        SAP_WBS_STATUS_UDF = "ActUDF.SAP_WBS_STATUS.Name",
        
        SAP_WBS_ACTCODE_NAME = "ActCode.SAP_WBS.Name",
        
        // Property with project type values of leaf WBS
        // elements that should go into the "WBS" activity code:
        WBS_ACTCODE_PROJTYPES = "ActCode.WBS.LeafWBSProjTypes",
        
        // Project Notebook topic for the SAP long text:
        PROJ_LONGTEXT_NOTEBOOK_TOPIC = "ProjNote.SAPLongText.Name",
        
        PROJ_IMPLOG_UPL_FCST_NOTEBOOK_TOPIC = "ProjNote.ImpressLogs-UploadForecast",
        PROJ_IMPLOG_DWNL_ACT_NOTEBOOK_TOPIC = "ProjNote.ImpressLogs-DownloadActuals",
        PROJ_IMPLOG_UPL_BUDG_NOTEBOOK_TOPIC = "ProjNote.ImpressLogs-UploadOriginalBudget",
        
        // Properties for flat file dir, and "copy directory" (shared drive):
        FLATFILE_DIR = "FlatfileDirectory",
        FLATFILE_COPY_DIR = "FlatfileCopyDirectory",
        
        // Properties for Budget/Forecast version number:
        BUDGET_UPLOAD_VERSION = "BudgetVersion",
        BUDGET_DOWNLOAD_VERSION = "BudgetDownloadVersion",
        FORECAST_VERSION = "ForecastVersion",
        
        // Property for Default activity:
        // Activity type is a field in budget/forecast upload files
        // Default value used if no "LSTAR" value returned by RFC
        DEFAULT_ACTTYPE = "DefaultActType",
        
        // Property for number of Closed-Status-Days configuration:
        PROJ_CLOSED_DAYS_THRES = "ProjClosedDaysThreshold",
        
        // Property for error email recipients:
        ERROR_EMAIL_RECIPIENTS = "ErrorEmailRecipients",
        
        // Directory where to generate "Download Actuals" company report files:
        DA_COMP_REPORT_DIR = "CompanyActualsReportDir",
        
        // Directory where to copy the "Download Actuals" company report files:
        DA_COMP_REPORT_COPY_DIR = "CompanyActualsReportCopyDir",
        
        // Path to XLS forecast summary template file
        FCST_SMRY_XLS_TEMPLATE_FILE_PATH = "PathToForecastSummaryReportTemplate";
    
    
    /**
     * @param compCode SAP company code
     * @return configuration file property for specified SAP company code's EPS
     */
    public static String compCodeEPS(String compCode)
    {
        return "CompCode." + compCode + ".EPS";
    }
    
    
    /**
     * @param compCode SAP company code
     * @return configuration file property for specified SAP company code's 
     *         contact email address(es, separated by ';')
     */
    public static String compCodeContactEmail(String compCode)
    {
        return "CompCode." + compCode + ".ContactEmail";
    }
}
