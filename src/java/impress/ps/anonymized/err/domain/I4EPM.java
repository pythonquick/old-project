package impress.ps.err.domain;

import impress.ps.err.AppBaseExc;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.error.OISException;
/**
 * Error domain class for wrapping Exceptions that occur during synchronization 
 * runs on one of the I.Map sequences of Impress for EPM.
 * 
 * <pre>
 * -----------------------------------------------------------------------------
 * Change history:
 * 1. GHA, 22-Mar-2007
 *    Created
 * -----------------------------------------------------------------------------
 * </pre>
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/I4EPM.java#2 $
 *
 * @author     GHA
 */

import impress.ois.base.map.imap.AbstractIMap;

public class I4EPM
{
    
    public static class LoadErr extends AppBaseExc
    {
        public static final int CODE = 82001;
        public static final String MSG = "Problem occurred: {0}"
            + "\n   during Load sequence of system {1}";
        
        public LoadErr(AbstractIMap imap, String msg, Exception e)
        {
            super (new Object[] {msg, imap.in.getElement("SourceSelectionParameters.BESchema")}, e);
        }
    }
    
    
    public static class PreMapErr extends AppBaseExc
    {
        public static final int CODE = 82002;
        public static final String MSG = "Problem occurred: {0}"
            + "\n   during PreMap sequence of mapping rule {1}"
            + "\n   from system: {2}" 
            + "\n   to system  : {3}";
        
        public PreMapErr(AbstractIMap imap, String msg, Exception e)
        {
            super (
                new Object[] {
                    msg,
                    imap.in.getElement("Parameters.ProcessCallName"),
                    imap.in.getElement("Parameters.SourceAccessComponent"),
                    imap.in.getElement("Parameters.TargetAccessComponent")
                }, e
            );
        }
    }
    
    
    public static class MapErr extends AppBaseExc
    {
        public static final int CODE = 82003;
        public static final String MSG = "Problem occurred: {0}"
            + "\n   during Mapping sequence of mapping rule \"{1}\""
            + "\n   for entity {2}"
            + "\n   from system {3}"
            + "\n   to system {4}";
        
        public MapErr(AbstractIMap imap, String msg, Exception e)
        {
            super (
                new Object[] {
                    msg,
                    imap.in.getElement("Parameters.ProcessCallName"),
                    imap.in.getElement("Parameters.EntityTypeName"),
                    imap.in.getElement("Parameters.SourceAccessComponent"),
                    imap.in.getElement("Parameters.TargetAccessComponent")
                }, e
            );
        }
    }
    
    
    public static class PostMapErr extends AppBaseExc
    {
        public static final int CODE = 82004;
        public static final String MSG = "Problem occurred: {0}"
            + "\n   during PostMap sequence of mapping rule \"{1}\" "
            + "\n   from system: {2}" 
            + "\n   to system  : {3}";
        
        public PostMapErr(AbstractIMap imap, String msg, Exception e)
        {
            super (
                new Object[] {
                    msg,
                    imap.in.getElement("Parameters.ProcessCallName"),
                    imap.in.getElement("Parameters.SourceAccessComponent"),
                    imap.in.getElement("Parameters.TargetAccessComponent")
                }, e
            );
        }
    }
    
    
    public static class SaveErr extends AppBaseExc
    {
        public static final int CODE = 82005;
        public static final String MSG = "Problem occurred: {0}"
            + "\n   during Save sequence of system {1}";
        
        public SaveErr(AbstractIMap imap, String msg, Exception e)
        {
            super (new Object[] {msg, imap.in.getElement("TargetSelectionParameters.BESchema")}, e);
        }
    }
}
