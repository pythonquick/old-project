package impress.ps.err.domain;


import impress.ps.err.AppBaseExc;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.error.OISException;
import impress.ois.base.map.imap.rt.BR;


/**
 * Error domain class for general errors
 * Some error classes take parameters to make the error messages more useful.
 * <pre>
 * -----------------------------------------------------------------------------
 * Change history:
 * 1. GHA, 22-Mar-2007
 *    Created
 * 2. GHA, 30-Jul-2008
 *    Newlines added to text of class SAPReturnedErrorOrWorse
 * 3. GHA, 31-Oct-2008
 *    IMapCallFailed now takes BR as constructor param, instead of Exception.
 *    From the BR, the exception can be deduced, and the getBR accessor method
 *    provides more possibilities for the caller to inspect the response.
 * -----------------------------------------------------------------------------
 * </pre>
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/General.java#3 $
 *
 * @author     GHA
 */
public class General
{
    
    public static class MissingContainerField extends AppBaseExc
    {
        public static final int CODE = 80001;
        public static final String MSG = "Expected container field under \"{0}\" {1}";
        
        public MissingContainerField(String fieldName)
        {
            super (new Object[] {fieldName, ""});
        }
        
        public MissingContainerField(String fieldName, String msg)
        {
            super (new Object[] {fieldName, "(" + msg + ")"});
        }
    }
    
    
    public static class IMapCallFailed extends AppBaseExc
    {
        public static final int CODE = 80007;
        public static final String MSG = "Call to I.Map {0} failed. ";
        private final BR br;
        
        public IMapCallFailed(String imapName, BR br)
        {
            super (new Object[] {imapName}, OISException.convertIContainerToException((IContainer)br.getElement("Out.Exception")));
            this.br = br;
        }
        
        
        public BR getBR()
        {
            return br;
        }
    }
    
    
    public static class SAPCallFailed extends AppBaseExc
    {
        public static final int CODE = 80008;
        public static final String MSG = "Call to SAP function module {0} failed. ";
        
        public SAPCallFailed(String imapName, OISException exc)
        {
            super (new Object[] {imapName}, exc);
        }
    }
    
    
    public static class SAPReturnedErrorOrWorse extends AppBaseExc
    {
        public static final int CODE = 80009;
        public static final String MSG = "Call to SAP function module {0} failed. {1} \nDetails: \n{2}";
        
        public SAPReturnedErrorOrWorse(String functionModule, Object details)
        {
            super (new Object[] {functionModule, "", details});
        }
        
        public SAPReturnedErrorOrWorse(String functionModule, String message, Object details)
        {
            super (new Object[] {functionModule, message, details});
        }
    }
    
    
    public static class CouldNotReadFile extends AppBaseExc
    {
        public static final int CODE = 80010;
        public static final String MSG = "Error occurred while reading file {0}.";
        
        public CouldNotReadFile(String fileName, Exception e)
        {
            super(new Object[] {fileName}, e);
        }
    }
    
    
    public static class IllegalFormat extends AppBaseExc
    {
        public static final int CODE = 80011;
        public static final String MSG = "Illegal field format: {0}.";
        
        public IllegalFormat(String descr)
        {
            super(new Object[] {descr});
        }
    }
    
    
    public static class Missing extends AppBaseExc
    {
        public static final int CODE = 80013;
        public static final String MSG = "Required but missing: {0}";
        
        public Missing(String nameOfMissingObject)
        {
            super (new Object[] {nameOfMissingObject});
        }
    }
    
    
    public static class NotFound extends AppBaseExc
    {
        public static final int CODE = 80014;
        public static final String MSG = "Not found: {0}";
        
        public NotFound(String nameOfObjNotFound)
        {
            super (new Object[] {nameOfObjNotFound});
        }
    }
    
    
    /**
     * Generic Warning
     */
    public static class Warning extends AppBaseExc
    {
        public static final int CODE = 80015;
        public static final String MSG = "Warning: {0}";
        
        public Warning(String warningMsg)
        {
            super (new Object[] {warningMsg});
        }
    }
}
