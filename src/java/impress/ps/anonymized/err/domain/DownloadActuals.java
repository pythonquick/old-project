package impress.ps.anonomized.err.domain;

import impress.ps.err.AppBaseExc;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.error.OISException;
/**
 * Error domain class for Primavera specific errors
 * 
 * <pre>
 * -----------------------------------------------------------------------------
 * Change history:
 * 1. GHA, 18-Jun-2007
 *    Created
 * -----------------------------------------------------------------------------
 * </pre>
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/DownloadActuals.java#2 $
 *
 * @author     GHA
 */

import impress.ois.base.map.imap.AbstractIMap;

public class DownloadActuals
{
    public static class AppBaseExcDuringActualsUpd extends AppBaseExc
    {
        public static final int CODE = 91001;
        public static final String MSG = "Download-Actuals specific process error occurred "
                        + "while attempting to update actuals on resource \"{1}\" "
                        + "on activity \"{0}\" "
                        + "with cost account \"{2}\"";
            
        public AppBaseExcDuringActualsUpd(String activityId, String resourceId, String costAccount, Exception cause)
        {
            super(new Object[] {activityId, resourceId, costAccount}, cause);
        }
    }
    
    
    public static class P3eExcDuringActualsUpd extends AppBaseExc
    {
        public static final int CODE = 91002;
        public static final String MSG = "Primavera exception occurred while attempting to update actuals "
                                + "on resource \"{1}\" "
                                + "on activity \"{0}\" "
                                + "with cost account \"{2}\"";
            
        public P3eExcDuringActualsUpd(String activityId, String resourceId, String costAccount, Exception cause)
        {
            super(new Object[] {activityId, resourceId, costAccount}, cause);
        }
    }
    
    
    public static class UnexpectedExcDuringActualsUpd extends AppBaseExc
    {
        public static final int CODE = 91003;
        public static final String MSG = "Unexpected error while setting actuals "
                                + "on resource \"{1}\" on activity \"{0}\"";
            
        public UnexpectedExcDuringActualsUpd(String activityId, String resourceId, Exception cause)
        {
            super(new Object[] {activityId, resourceId}, cause);
        }
    }


    public static class ActivityStarted extends AppBaseExc
    {
        public static final int CODE = 91004;
        public static final String MSGPART1 = "The start flag on activity ";
        public static final String MSGPART2 = " has been set";
        public static final String MSG = MSGPART1 + "{0}" + MSGPART2;
            
        public ActivityStarted(String activityId)
        {
            super(new Object[] {activityId});
        }
    }
}
