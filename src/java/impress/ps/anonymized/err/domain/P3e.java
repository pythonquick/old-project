package impress.ps.anonomized.err.domain;

import impress.ps.err.AppBaseExc;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.error.OISException;
/**
 * Error domain class for Primavera specific errors
 * 
 * <pre>
 * -----------------------------------------------------------------------------
 * Change history:
 * 1. GHA, 18-Jun-2007
 *    Created
 * 2. GHA, 02-Jul-2008
 *    ProjUDFNotFound renamed to UDFNotFound
 * 3. GHA, 03-Jul-2008
 *    New class: NoActiveResourceRateFound
 * 4. GHA, 14-Jul-2008
 *    ZTableLOENotFound, ZTableCostAccountNotFound, ZTableResourceNotFound:
 *    Removed hard-coded "ZWM_ED_PV_ACT" table name, and made it a constructor 
 *    parameter. This allows these classes to be used for the download-budget 
 *    as well as download-actuals process
 * -----------------------------------------------------------------------------
 * </pre>
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/P3e.java#4 $
 *
 * @author     GHA
 */

import impress.ois.base.map.imap.AbstractIMap;

public class P3e
{
    
    public static class TemplateProjNotFound extends AppBaseExc
    {
        public static final int CODE = 90001;
        public static final String MSG = "P3e template project not found: {0}";
        
        public TemplateProjNotFound(String templateProj)
        {
            super (new Object[] {templateProj});
        }
    }
    
    
    public static class EPSNotFound extends AppBaseExc
    {
        public static final int CODE = 90002;
        public static final String MSG = "EPS not found: {0}";
        
        public EPSNotFound(String eps)
        {
            super (new Object[] {eps});
        }
    }

    
    public static class ProjectAlreadyExists extends AppBaseExc
    {
        public static final int CODE = 90003;
        public static final String MSG = "Project already exists: {0}";
        
        public ProjectAlreadyExists(String projName)
        {
            super (new Object[] {projName});
        }
    }
    
    
    public static class P3eAPICallFailed extends AppBaseExc
    {
        public static final int CODE = 90004;
        public static final String MSG = "Call to P3e API call failed. {0}";
        
        public P3eAPICallFailed(String descr, Exception exc)
        {
            super (new Object[] {descr}, exc);
        }
    }
    
    
    public static class ProjectNotFound extends AppBaseExc
    {
        public static final int CODE = 90005;
        public static final String MSG = "Project not found: {0}";
        
        public ProjectNotFound(String projName)
        {
            super (new Object[] {projName});
        }
    }
    
    
    public static class ActivityNotFound extends AppBaseExc
    {
        public static final int CODE = 90006;
        public static final String MSG = "Activity \"{1}\" not found on project \"{0}\"";
        
        public ActivityNotFound(String projId, String activityId)
        {
            super (new Object[] {projId, activityId});
        }
    }
    
    
    public static class RANotFound extends AppBaseExc
    {
        public static final int CODE = 90007;
        public static final String MSG = "Assignment of resource \"{1}\" on Activity \"{0}\" not found";
        
        public RANotFound(String activityId, String resourceId)
        {
            super (new Object[] {activityId, resourceId});
        }
    }
    
    
    public static class ResourceNotFound extends AppBaseExc
    {
        public static final int CODE = 90008;
        public static final String MSG = "Resource \"{0}\" not found";
        
        public ResourceNotFound(String resourceId)
        {
            super (new Object[] {resourceId});
        }
    }
    
    
    public static class CostAccountNotFound extends AppBaseExc
    {
        public static final int CODE = 90009;
        public static final String MSG = "CostAccount \"{0}\" not found";
        
        public CostAccountNotFound(String costAccountId)
        {
            super (new Object[] {costAccountId});
        }
    }
    
    
    public static class ActCodeTypeNotFound extends AppBaseExc
    {
        public static final int CODE = 90010;
        public static final String MSG = "ActivityCodeType \"{0}\" not found";
        
        public ActCodeTypeNotFound(String actCodeTypeName)
        {
            super (new Object[] {actCodeTypeName});
        }
    }
    
    
    public static class CodeTypeNotFound extends AppBaseExc
    {
        public static final int CODE = 90011;
        public static final String MSG = "Code type \"{0}\" not found";
        
        public CodeTypeNotFound(String codeType)
        {
            super (new Object[] {codeType});
        }
    }
    
    
    public static class CodeNotFound extends AppBaseExc
    {
        public static final int CODE = 90012;
        public static final String MSG = "Code value \"{1}\" not found for code type \"{0}\"";
        
        public CodeNotFound(String codeType, String codeValue)
        {
            super (new Object[] {codeType, codeValue});
        }
    }
    
    
    public static class UDFNotFound extends AppBaseExc
    {
        public static final int CODE = 90013;
        public static final String MSG = "UDF not found: \"{0}\"";
        
        public UDFNotFound(String udfField)
        {
            super (new Object[] {udfField});
        }
    }
    
    
    public static class ZTableLOENotFound extends AppBaseExc
    {
        public static final int CODE = 90014;
        public static final String MSG = "Activitiy \"{1}\" specified in table {2}, does not exist in Primavera project \"{0}\"";
            
        public ZTableLOENotFound(String p3eProjId, String p3eActivityId, String zTable)
        {
            super(new Object[] {p3eProjId, p3eActivityId, zTable});
        }
    }
    
    
    public static class ZTableCostAccountNotFound extends AppBaseExc
    {
        public static final int CODE = 90015;
        public static final String MSG = "CostAccount \"{0}\" specified in table {1} does not exist in Primavera";
            
        public ZTableCostAccountNotFound(String costAccountId, String zTable)
        {
            super(new Object[] {costAccountId, zTable});
        }
    }
    
    
    public static class ZTableResourceNotFound extends AppBaseExc
    {
        public static final int CODE = 90016;
        public static final String MSG = "Resource \"{0}\" specified in table {1} does not exist in Primavera";
            
        public ZTableResourceNotFound(String resourceId, String zTable)
        {
            super(new Object[] {resourceId, zTable});
        }
    }
    
    
    public static class NoActiveResourceRateFound extends AppBaseExc
    {
        public static final int CODE = 90017;
        public static final String MSG = "No resource rate found effective on or before {0}";
            
        public NoActiveResourceRateFound(String dateString)
        {
            super(new Object[] {dateString});
        }
    }
}
