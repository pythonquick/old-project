package impress.ps.anonomized.err.domain;

import impress.ps.err.AppBaseExc;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.error.OISException;
/**
 * Error domain class for errors specific to the Download-Budget process.
 * 
 * <pre>
 * -----------------------------------------------------------------------------
 * Change history:
 * 1. GHA, 14-Jul-2008
 *    Created
 * -----------------------------------------------------------------------------
 * </pre>
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/DownloadBudget.java#1 $
 *
 * @author     GHA
 */

import impress.ois.base.map.imap.AbstractIMap;

public class DownloadBudget
{
    public static class AppBaseExcDuringBudgetUpd extends AppBaseExc
    {
        public static final int CODE = 92001;
        public static final String MSG = "Download-Budget specific process error occurred "
                        + "while attempting to update budget on resource \"{1}\" "
                        + "on activity \"{0}\" "
                        + "with cost account \"{2}\"";
            
        public AppBaseExcDuringBudgetUpd(String activityId, String resourceId, String costAccount, Exception cause)
        {
            super(new Object[] {activityId, resourceId, costAccount}, cause);
        }
    }
    
    
    public static class P3eExcDuringBudgetUpd extends AppBaseExc
    {
        public static final int CODE = 92002;
        public static final String MSG = "Primavera exception occurred while attempting to update budget "
                                + "on resource \"{1}\" "
                                + "on activity \"{0}\" "
                                + "with cost account \"{2}\"";
            
        public P3eExcDuringBudgetUpd(String activityId, String resourceId, String costAccount, Exception cause)
        {
            super(new Object[] {activityId, resourceId, costAccount}, cause);
        }
    }
    
    
    public static class UnexpectedExcDuringBudgetUpd extends AppBaseExc
    {
        public static final int CODE = 92003;
        public static final String MSG = "Unexpected error while setting budget "
                                + "on resource \"{1}\" on activity \"{0}\"";
            
        public UnexpectedExcDuringBudgetUpd(String activityId, String resourceId, Exception cause)
        {
            super(new Object[] {activityId, resourceId}, cause);
        }
    }
}
