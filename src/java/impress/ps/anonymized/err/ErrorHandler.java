/**
 *==============================================================================
 * (c) 1998-2005 by IMPRESS SOFTWARE AG, Germany. All rights reserved.
 * IMPRESS Engine 5.3
 * PCE 3.1
 *==============================================================================
 * Author:              VVO
 * Date:                Wed Oct 04 11:50:44 CEST 2006
 * -----------------------------------------------------------------------------
 * Desription:
 *  delivers some static methods for the more simple handling of errors, warnings,
 *  infos, tracings etc.
 * -----------------------------------------------------------------------------
 * Change history:
 * 1. GHA, 22-Mar-2007
 *    Method isDebugOn now takes only one parameter: AbstractIMap
 * 2. GHA, 29-Aug-2008
 *    Synch log "Detailed Errors" renamed to "Detailed Errors/Warnings"
 * 3. GHA, 15-Sep-2008
 *    Moved to impress.ps.anonomized package, for Anonomized-specific 
 *    functionality when flushing logs. Method flushLogs: Logs for some 
 *    mapping rules, need to be added to a Primavera notebook topic first, 
 *    before mapping rule completes
 * -----------------------------------------------------------------------------
 *
 * $Id:  $
 */
package impress.ps.anonomized.err;


import java.util.List;
import impress.ois.base.dtrans.IContainer;
import impress.ois.base.error.OISException;
import impress.ois.base.map.imap.rt.IESystem;
import impress.ois.base.map.imap.AbstractIMap;
import impress.ps.i4epm.logging.*;
import impress.ps.anonomized.ProjUtil;


public class ErrorHandler 
{
    // Update Log types:
    public static final String  TRC_SYNCH_LOG = "Debugging Traces",
                                INF_SYNCH_LOG = "Informations",
                                WRN_SYNCH_LOG = "Warnings",
                                ERR_SYNCH_LOG = "Errors",
                                DET_SYNCH_LOG = "Detailed Errors/Warnings";
    
    // Short hand error levels:
    public static final Integer 
        INFO_LEVEL = new Integer(OISException.INFORMATION),
        WARNING_LEVEL = new Integer(OISException.WARNING),
        ERROR_LEVEL = new Integer(OISException.ERROR),
        FATAL_LEVEL = new Integer(OISException.FATAL_ERROR);
    
    /**
     * Throws the specified fatal exception and logs it to both the error log and to the detail error log.
     * All the gethered logs will be automatically flushed to the repository.
     * @param ie magical ImpressEngine instance
     * @param imap exception causing I.Map. 
     * @param exc exception which has to be handled
     */
    public static void throwFatalErrorAndLog(IESystem ie, AbstractIMap imap, OISException exc) 
    {
        throwFatalErrorAndLog(ie, imap, ERR_SYNCH_LOG, DET_SYNCH_LOG, exc);
    }
    
    
    /**
     * Throws the specified fatal exception and logs it to both the error log and to the detail error log.
     * All the gethered logs will be automatically flushed to the repository.
     * @param ie magical ImpressEngine instance
     * @param imap exception causing I.Map
     * @param logName name of the target log file
     * @param detailsLogName name of the target details log file
     * @param imapIn IContainer the in-container of the I.Map containing the
     *        PassThrough container with the SynchLog entries to be flushed
     * @param exc exception which has to be handled
     */
    public static void throwFatalErrorAndLog(IESystem ie, AbstractIMap imap, String logName, String detailsLogName, OISException exc)
    {
        setLevelOnEntireExceptionChain(exc, FATAL_LEVEL);
        if(logName != null) 
            SynchLog.add(imap, logName, new HighlevelErr(exc));
        if(detailsLogName != null) 
            SynchLog.add(imap, detailsLogName, new FatalError(exc));
        flushLogs(ie, imap);
        ie.addErrorToErr(exc);
    }
    
    
    /**
     * Throws the specified exception and logs it to both the error log and to the detail error log.
     * @param ie magical ImpressEngine instance
     * @param imap exception causing I.Map
     * @param exc exception which has to be handled
     */
    public static void throwErrorAndLog(IESystem ie, AbstractIMap imap, OISException exc) 
    {
        throwErrorAndLog(ie, imap, ERR_SYNCH_LOG, DET_SYNCH_LOG, exc);
        /*
        SynchLog.add(imap, ERR_SYNCH_LOG, new HighlevelErr(exc));
        SynchLog.add(imap, DET_SYNCH_LOG, new LowlevelErr(exc));
        ie.addErrorToErr(exc);*/
    }
    
    
    /**
     * Throws the specified exception and logs it to both the error log and to the detail error log.
     * All the gethered logs will be automatically flushed to the repository.
     * @param ie magical ImpressEngine instance
     * @param imap exception causing I.Map
     * @param logName name of the target log file
     * @param detailsLogName name of the target details log file
     * @param imapIn IContainer the in-container of the I.Map containing the
     *        PassThrough container with the SynchLog entries to be flushed
     * @param exc exception which has to be handled
     */
    public static void throwErrorAndLog(IESystem ie, AbstractIMap imap, String logName, String detailsLogName, OISException exc)
    {
        if(logName != null) 
            SynchLog.add(imap, logName, new HighlevelErr(exc));
        if(detailsLogName != null) 
            SynchLog.add(imap, detailsLogName, new LowlevelErr(exc));
        ie.addErrorToErr(exc);
    }
    
    
    /**
     * Throws the specified warning and logs it to both the warning log and to the detail error log.
     * @param ie magical ImpressEngine instance
     * @param imap warning I.Map
     * @param exc exception which causes the warning
     */
    public static void logAndAddWarning(IESystem ie, AbstractIMap imap, OISException exc) 
    {
        logAndAddWarning(ie, imap, WRN_SYNCH_LOG, DET_SYNCH_LOG, exc); 
    }
    
    
    /**
     * Throws the specified warning and logs it to both the warning log and to the detail error log.
     * @param ie magical ImpressEngine instance
     * @param imap warning I.Map
     * @param logName name of the target log file
     * @param detailsLogName name of the target details log file
     * @param exc exception which causes the warning
     */
    public static void logAndAddWarning(IESystem ie, AbstractIMap imap, String logName, String detailsLogName,  OISException exc) 
    { 
        setLevelOnEntireExceptionChain(exc, WARNING_LEVEL);
        if(logName != null) 
            SynchLog.add(imap, logName, new Warning(exc));
        if(detailsLogName != null) 
            SynchLog.add(imap, detailsLogName, new LowlevelErr(exc));
        ie.addErrorToErr(exc);
    }
    
    
    /**
     * Logs the specified warning into the warning log without throwing it.
     * @param imap warning I.Map
     * @param exc exception which causes the warning
     */
    public static void logWarning(AbstractIMap imap, OISException exc) 
    {
        logWarning(imap, WRN_SYNCH_LOG, exc);
    }
    
    
    /**
     * Logs the specified warning into the warning log without throwing it.
     * @param imap warning I.Map
     * @param logName name of the target log file
     * @param exc exception which causes the warning
     */
    public static void logWarning(AbstractIMap imap, String logName, OISException exc) 
    {
        if(logName != null)
        {
            setLevelOnEntireExceptionChain(exc, WARNING_LEVEL);
            SynchLog.add(imap, logName, new Warning(exc));
        }
    }
    
    
    /**
     * Throws the specified information level "exception" and logs it to both the information log and to the detail error log.
     * @param ie magical ImpressEngine instance
     * @param imap informing I.Map
     * @param exc "exception" which causes the information
     */
    public static void logAndAddInfo(IESystem ie, AbstractIMap imap, OISException exc) 
    {
        setLevelOnEntireExceptionChain(exc, INFO_LEVEL);
        SynchLog.add(imap, INF_SYNCH_LOG, new HighlevelErr(exc));
        SynchLog.add(imap, DET_SYNCH_LOG, new LowlevelErr(exc));
        ie.addErrorToErr((IContainer)exc);
    }
    
    
    /**
     * Logs the specified information into the information log without throwing the exception.
     * @param imap informing I.Map
     * @param exc "exception" which causes the information
     */
    public static void logInfo(AbstractIMap imap, OISException exc) 
    {
        setLevelOnEntireExceptionChain(exc, INFO_LEVEL);
        SynchLog.add(imap, INF_SYNCH_LOG, new InfoEntry(exc));
    }
    
    
    /**
     * Logs the specified information into the information log without throwing the exception.
     * @param imap informing I.Map
     * @param info information message
     */
    public static void logInfo(AbstractIMap imap, String info) 
    {
        logInfo(imap, INF_SYNCH_LOG, info);
    }
    
    
    /**
     * Logs the specified information into the information log without throwing the exception.
     * @param imap informing I.Map
     * @param info information message
     */
    public static void logInfo(AbstractIMap imap, String info, IContainer details) 
    {
        logInfo(imap, INF_SYNCH_LOG, info, details);
    }
    
    
    /**
     * Logs the specified information into the information log without throwing the exception.
     * @param imap informing I.Map
     * @param logName name of the target log file
     * @param info information message
     */
    public static void logInfo(AbstractIMap imap, String logName, String info) 
    {
        SynchLog.add(imap, logName, new InfoEntry(info));
    }
    
    
    /**
     * Logs the specified information into the information log without throwing the exception.
     * @param imap informing I.Map
     * @param logName name of the target log file
     * @param info information message
     * @param details details container
     */
    public static void logInfo(AbstractIMap imap, String logName, String info, IContainer details) 
    {
        SynchLog.add(imap, logName, new InfoEntry(info, details));
    }
    
    
    /**
     * Logs the specified message to the debugging traces log.
     * @param imap source I.Map of the message
     * @param exc message which has to be traced
     */
    public static void traceToLog(AbstractIMap imap, String message) 
    {
        traceToLog(imap, TRC_SYNCH_LOG, message);
    }
    
    
    /**
     * Logs the specified message to a traces log specified by its name.
     * @param imap source I.Map of the message
     * @param logName name of the target log file
     * @param exc message which has to be traced
     */
    public static void traceToLog(AbstractIMap imap, String logName, String message) 
    {
        SynchLog.add(imap, logName, new TextEntry(message));
    }
    
    
    /**
     * Logs all the objects of the specified list into the log of the specified level without throwing the exception.
     * @param imap informing I.Map
     * @param logName name of the target log file
     * @param list list of messages
     * @param title headline of the whole message in the log
     * @param level level of information
     */
    public static void logList(AbstractIMap imap, String logName, List list, String title, Integer level) 
    {
        SynchLog.add(imap, logName, new ListEntry(list, title, level));
    }
    
    
    /**
     * Flush all SynchLog entries to the repository, manually
     * @param ie magical ImpressEngine instance
     * @param imapIn IContainer the in-container of the I.Map containing the
     *        PassThrough container with the SynchLog entries to be flushed
     */
    public static void flushLogs(IESystem ie, AbstractIMap imap)
    {
        IContainer imapIn = imap.in;
        
        // Save Impress logs (PassThrough.SynchLog) to Primavera notebook topic
        // for certain mapping rules:
        ProjUtil.saveImpressLogsToNotebookTopic(ie, imapIn);
        
        // Save logs persistently to repository:
        SynchLog.flushToRepository(ie, imapIn);
    }
    
    
    /**
     * Determines whether debugging mode has been switched on
     * Criteria for yes:
     *   IMap.in"SynchParameters.DebugMode" equals Boolean.TRUE
     *   AND
     *   IMap.in."SynchParameters.DebugIMap" equals IMap name or is null
     *
     * @param imap AbstractIMap instance
     * @return true if debugging mode has been switched on
     */
    public static boolean isDebugOn(AbstractIMap imap)
    {
        boolean ret = false;
        
        IContainer imapIn = imap.in;
        String imapName = nameFor(imap);
        if(imapIn != null)
        {
            Object debMode = null;
            String debugIMap = null;
            try
            {
                debMode = imapIn.getElement("SynchParameters.DebugMode");
                debugIMap = (String)imapIn.getElement("SynchParameters.DebugIMap");
            }
            catch(OISException oise){}
            if(Boolean.TRUE.equals(debMode))
            {
                if(debugIMap == null || debugIMap.length()<1)
                    ret = true;
                else if(debugIMap != null && debugIMap.equals(imapName))
                    ret = true;
            }
        }
        return ret;
    }
    
    
    private static void setLevelOnEntireExceptionChain(OISException exc, Integer levelToSet)
    {
        exc.setLevel(levelToSet);
        Throwable cause = exc.getCause();
        if (cause instanceof OISException)
        {
            setLevelOnEntireExceptionChain((OISException)cause, levelToSet);
        }
    }
    
    
    /**
     * Returns name of specified IMap
     * @param imap AbstractIMap instance
     * @return String name of specified imap
     */
    static String nameFor(AbstractIMap imap)
    {
        return imap.getClass().getName();
    }
    
}
