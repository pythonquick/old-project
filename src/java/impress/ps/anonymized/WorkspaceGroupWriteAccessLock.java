package impress.ps.anonomized;

import impress.ps.concurrency.Lock;

/**
 * Class for locking write access to a workspace group container.
 * -----------------------------------------------------------------------------
 * Change history:
 * 1.  GHA, 13-Jun-2007
 *     Creation
 * -----------------------------------------------------------------------------
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/java-src/WorkspaceGroupWriteAccessLock.java#2 $
 */
public class WorkspaceGroupWriteAccessLock extends Lock
{
    public static Lock instance;
    
    private WorkspaceGroupWriteAccessLock() {}
    
    
    protected long lockTimeout()
    {
        return 1000 * 60 * 5; // 5 minute;
    }
    

    public static Lock getInstance()
    {
        if (instance == null)
            instance = new WorkspaceGroupWriteAccessLock();
        return instance;
    }
}
