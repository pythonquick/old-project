package impress.ps.anonomized;


import impress.ois.base.map.imap.AbstractIMap;
import impress.ois.base.dtrans.IContainer;


/**
 * Convenience class for defining container key constants, from which the
 * container elements can be accessed.
 *
 * An example would be a class for holding PassThrough container key constants
 * that are used in many I.Maps:
 * <code>
 *     public final class PassThrough
 *     {
 *         public static final ContFieldAccessor
 *             PROJ_NAME = forField("ProjectName"),
 *             ACT_INFO = forField("ActInfo"),
 *             FORECAST_FLAG = forField("ForecastFlag");
 *         
 *         
 *         private static ContFieldAccessor forField(String contKey)
 *         {
 *             return ContFieldAccessor.createFromKey("PassThrough." + contKey);
 *         }
 *     }
 * </code>
 *
 * And use as follows in an I.Map to set or get elements from PassThrough:
 * <code>
 *     PassThrough.FORECAST_FLAG.set(Boolean.TRUE);
 *     IContainer activityInfo = PassThrough.ACT_INFO.getCont(in);
 *     String project = PassThrough.PROJ_NAME.getString(in);
 * </code>
 */
public class ContFieldAccessor
{
    private final String contKey;
    
    
    private ContFieldAccessor(String contKey)
    {
        this.contKey = contKey;
    }
    
    
    public String getKey()
    {
        return contKey;
    }
    
    
    public static ContFieldAccessor createFromKey(String contKey)
    {
        return new ContFieldAccessor(contKey);
    }


    public void set(AbstractIMap imap, Object contElem)
    {
        set(imap.out, contElem);
    }
    
    
    public void set(IContainer targetCont, Object contElem)
    {
        targetCont.setElement(contKey, contElem);
    }
    
    
    public Object get(AbstractIMap imap)
    {
        return get(imap.in);
    }
    
    
    public String getString(AbstractIMap imap)
    {
        return getString(imap.in);
    }
    
    
    public IContainer getCont(AbstractIMap imap)
    {
        return getCont(imap.in);
    }
    
    
    public Object get(IContainer sourceContainer)
    {
        return sourceContainer.getElement(contKey);
    }
    
    
    public String getString(IContainer sourceContainer)
    {
        return (String)sourceContainer.getElement(contKey);
    }
    
    
    public IContainer getCont(IContainer sourceContainer)
    {
        return (IContainer)sourceContainer.getElement(contKey);
    }
    
    
    public String toString()
    {
        return "ContFieldAccessor for key " + contKey;
    }
}
