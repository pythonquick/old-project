/**
 *==============================================================================
 * Impress for EPM 3.2
 *==============================================================================
 * Function:    Any "Project" entity based mapping rule PS->P3e
 * Based on:    -
 * 
 * Description:
 * Maps basic Project-level fields, for example: Date and Calendar fields
 *
 *==============================================================================
 * Change history:
 *  1. GHA, 2007-07-26
 *     Creation
 *  2. GHA, 2008-06-05
 *     Do not transfer planned start date at all
 *  3. GHA, 2008-07-01
 *     Remove field ""Data.Description". In the general
 *     ("Gnrl") case we don't want to set the Primavera "Project Name" field,
 *     (to which this field would map if it was set)
 *  4. GHA, 2008-07-14
 *     Moved the setting of WBS code values to separate I.Map
 *  5. GHA, 2008-09-15
 *     Use new class impress.ps.anonomized.err.ErrorHandler (diff package)
 *==============================================================================
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/i.map-src/impPsPrjMapPSP3eGnrlMapProjFields.imap#6 $
 * @description 
 * @flags 
 * @owner guentherh
 * @major_version 1
 * @roles MappingDeveloper 
*/


import impress.ps.err.domain.I4EPM;
import impress.ps.anonomized.err.ErrorHandler;


imap impPsPrjMapPSP3eGnrlMapProjFields
{
    process()
    {
        out = in;
        
        try
        {
            mapFields(in);
        }
        catch (Exception e)
        {
            String msg = "while mapping general project level fields";
            OISException exc = new I4EPM.MapErr(this, msg, e);
            ErrorHandler.throwErrorAndLog(ie, this, exc);
        }
    }
    
    
    /**
     * Field mappings to Primavera project-level codes and UDFs
     * @param inCont I.Map "in" container
     */
    mapFields(IContainer inCont)
    {
        IContainer data = (IContainer)inCont."Data";
        if (data == null)
            return;
        
        data.removeElement("PlannedStartDate");
        data.removeElement("PlannedEndDate");
        data.removeElement("DefaultCalendarId");
        data.removeElement("Description");
    }
}
