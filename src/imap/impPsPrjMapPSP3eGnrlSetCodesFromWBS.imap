/**
 *==============================================================================
 * Impress for EPM 3.2
 *==============================================================================
 * Function:    Create Project or Update Project PS->P3e mapping rules
 * Based on:    -
 * 
 * Description:
 * Sets various code fields, from SAP project's WBS elements.
 *
 *==============================================================================
 * Change history:
 *  1. GHA, 2008-07-14
 *     Creation
 *  2. GHA, 2008-08-27
 *     Method process reads SAP projdef from field "ProjectId" instead of "Name"
 *  3. GHA, 2008-09-15
 *     Use new class impress.ps.anonomized.err.ErrorHandler (diff package)
 *  4. GHA, 2008-10-30
 *     Replace ContUtil.getOrCreateElem by ContUtil.getOrCreateCont
 *==============================================================================
 * $Id: //projects/Anonomized/Anonomized0100212/Development/00-SourceCode/i.map-src/impPsPrjMapPSP3eGnrlSetCodesFromWBS.imap#3 $
 * @description 
 * @flags 
 * @owner guentherh
 * @major_version 1
 * @roles MappingDeveloper 
*/


import impress.ps.err.domain.I4EPM;
import impress.ps.anonomized.err.ErrorHandler;
import impress.ps.util.Config;
import impress.ps.util.ContUtil;
import impress.ps.err.domain.General;
import impress.ps.anonomized.ProjUtil;
import impress.ps.anonomized.ProjConst;
import impress.ps.anonomized.WSParam;
import impress.ps.anonomized.ConfParam;


imap impPsPrjMapPSP3eGnrlSetCodesFromWBS
{
    process()
    {
        out = in;
        
        try
        {
            IContainer data = (IContainer)in."Data";
            if (data == null)
                return;

            String configFilePath = ProjConst.pathToConfigFile();
            Config config = Config.createFromFile(configFilePath);
            String sapProjDef = (String)data."System.PROJECT_DEFINITION";
            setCodesFromWBSElements(in, sapProjDef, config);
        }
        catch (Exception e)
        {
            String msg = "while setting Primavera code values from WBS elements";
            OISException exc = new I4EPM.MapErr(this, msg, e);
            ErrorHandler.throwErrorAndLog(ie, this, exc);
        }
    }
    
    
    /**
     * Set project codes, mapped from values of WBS elements
     * @param inCont I.Map "in" container
     * @param projDef SAP project definition
     * @param config Config instance (configuration file)
     */
    setCodesFromWBSElements(IContainer inCont, String projDef, Config config)
    {
        // Load project's WBS - needed for Project-level activity code values:
        IContainer data = (IContainer)inCont."Data";
        
        String sapBE = (String)WSParam.sourceSelParam(inCont, WSParam.BE_SCHEMA);
        String projType = config.getString(ConfParam.LEV2_WBS_PROJTYPE);
        IContainer wbsElements = ProjUtil.loadWBSElements(ie, sapBE, projDef);
        
        if (ContUtil.isNullOrEmpty(wbsElements))
        {
            OISException exc = new General.Missing("WBS Elements");
            ErrorHandler.throwErrorAndLog(ie, this, exc);
            return;
        }
        
        // Generate WBS activity code values (from lowest level WBSes, of certain proj types). 
        // in a container, to be created in later in Save sequence:
        String wbsActCodeName = config.getString(ConfParam.SAP_WBS_ACTCODE_NAME);
        String separatedProjTypes = config.getString(ConfParam.WBS_ACTCODE_PROJTYPES);
        IContainer wbsActCodeCnt = ProjUtil.genWbsActCodesCnt(wbsActCodeName, separatedProjTypes, wbsElements);
        IContainer projActCodes = ContUtil.getOrCreateCont(inCont, "PassThrough.P3e.Create.ProjActCode");
        ContUtil.addRow(projActCodes, wbsActCodeCnt);
    }
}
